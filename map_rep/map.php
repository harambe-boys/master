<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Lomas De Altamira</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="../images/lomas_altamira.ico" /> 
        <!-- Link css-->
		
        <link rel="stylesheet" type="text/css" href="../css/mred_styles.css"  />
		
        <link rel="stylesheet" type="text/css" href="../components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="../components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="../components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="../components/validationEngine/validationEngine.jquery.css" />
         
        <link rel="stylesheet" type="text/css" href="../components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="../components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="../components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="../components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="../components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="../components/Jcrop/jquery.Jcrop.css"  />
   
        
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->
        
        <script type="text/javascript" src="../js/mred_js.js"></script> 
        <script type="text/javascript" src="../components/ui/mred_ui.js"></script> 
        <!--<script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>-->
        <script type="text/javascript" src="../components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="../components/checkboxes/mred_checkboxes.js"></script>
        <!--<script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>-->
        <script type="text/javascript" src="../components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="../components/datatables/mred_datatables.js"></script>
        <!--<script type="text/javascript" src="components/datatables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="components/datatables/ColVis.js"></script>-->
        <script type="text/javascript" src="../components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="../components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="../components/jscrollpane/mred_jscrollpane.js"></script>
        <!--<script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>-->
        <script type="text/javascript" src="../components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="../components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="../components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="../components/chosen/chosen.js"></script>
        <script type="text/javascript" src="../components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="../components/validationEngine/mred_validationEngine.js" ></script>
        <!--<script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>-->
        <script type="text/javascript" src="../components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="../components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="../components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="../components/flot/mred_flot.js"></script>
        <!--<script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>-->

        <script type="text/javascript" src="../components/uploadify/mred_uploadify.js"></script>
        <!--<script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>        -->
        <!--<script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>-->
        <script type="text/javascript" src="../components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="../components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="../components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="../components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="../components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="../components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="../components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="../components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="../components/smartWizard/jquery.smartWizard.min.js"></script>
        <!--<script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>    
		<script src="js/jquery.form.js" type="text/javascript"></script>-->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
		<script type="text/javascript" src="../js/upload.js"></script>
		<script type="text/javascript">	
			var map;
			var markers 	= [];
			var locaciones 	= [];
			var counter 	= 0;
			var infowindow 	= new google.maps.InfoWindow();
			$(document).ready(function() {
				initialize();
				
				$.post("json.php",{
				},function(data, status){
					if(status =='success'){
						deleteMarkerAll();
						data = data.replace(/^\s*|\s*$/g,"");
						if (data != 'ndata'){
							data = JSON.parse(data);
							data.forEach(function(element){
								var locacion = new google.maps.LatLng(element['lat'],element['lng']);  
								addMarker2(locacion);
							});
							
						}
					}
				});
				$("#guardar").click(function(){
					loading('Procesando',1);
					if(locaciones.length==0){
						showWarning('Ingrese markers al mapa para la promocion seleccionada',3000);
					}else{
						console.log(locaciones);
						
						$.ajax({
							type: 'POST',
							url: 'actionmap.php',
							data: {location:locaciones},
							success: function(data) {
								data = data.replace(/^\s*|\s*$/g,"");
								if(data=='done'){
									msg = "Se ha guardado el punto.";
									setTimeout( "showSuccess(msg,5000);", 1000 ); 
									setTimeout( "unloading()", 3000 );
									locaciones 	= [];
								}else{
									msg = "ERROR. INTENTELO DE NUEVO.";
									setTimeout( "showError(msg,5000);", 1000 );
									setTimeout( "unloading()", 3000 );
								}
							}	
						}); 
					}
				});
			});
			function initialize() {
				var NY = new google.maps.LatLng(13.676572, -89.219841);
				map = new google.maps.Map( 
					document.getElementById('map'), {
					center: NY,
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});

				google.maps.event.addListener(map, 'click', function (event) {
					
					addMarker(event.latLng);
				});
				
				google.maps.event.addListener(infowindow, 'domready', function () {
					$("#deleteButton").click(function() {
						deleteMarker($(this).data('id'));
					});
				});
			}

			function addMarker(location) {
				counter++;
				var clickLat = location.lat();
				var clickLon = location.lng();
				locaciones.push([clickLat,clickLon]);
				var marker = new google.maps.Marker({
					position: location,
					map: map,
					draggable:true,
					id: counter
				});
				
				markers.push(marker);
				var deleteButton = '<button id="deleteButton" data-id="' + counter + '">Delete</button>';
				google.maps.event.addListener(marker, 'rightclick', function () {
					infowindow.setContent(deleteButton);
					infowindow.open(map, marker);
				});
			}
			
			function addMarker2(location) {
				counter++;
				var marker = new google.maps.Marker({
					position: location,
					map: map,
					draggable:true,
					id: counter
				});
				
				markers.push(marker);
				var deleteButton = '<button id="deleteButton" data-id="' + counter + '">Delete</button>';
				google.maps.event.addListener(marker, 'rightclick', function () {
					infowindow.setContent(deleteButton);
					infowindow.open(map, marker);
				});
			}

			function deleteMarker(markerId) {
				for (var i=0; i<markers.length; i++) {
					if (markers[i].id === markerId) {
						markers[i].setMap(null);
						markers.splice(i, 1);
					}
				}
			}
			function deleteMarkerAll() {
				for (var i=0; i<markers.length; i++) {
					markers[i].setMap(null);
				}
				counter 	= 0;
				locaciones 	= [];
				markers 	= [];
			}
		</script>		
      
	</head>        
    <body class="dashborad">
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><span class="ico  gray connect"></span>Puntos</span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div class="load_page">
							<div class="formEl_b" id="msg">	
								<form id="form" name="form"> 
									<fieldset >
										<legend>Seleccione la promocion&oacute;n y coloque los puntos donde necesite</legend>
											<label> Mapa</label>   
											<div> 
												<div style="height:100%; width:100%;">
													 <div id="map"></div> 
												</div>
											</div>
										<div class="section last">
											<div>
												<input type="button" value="GUARDAR" class="uibutton submit_form" name="guardar" id="guardar" />                                  
											</div>
										</div>
									</fieldset>	
								</form> 
							</div>
						</div>	
					</div>                  
				</div>
			</div>
			<?php
				include_once "../footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>