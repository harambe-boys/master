<?php
class Map{
	//Constructor
	function __construct() 
	{
		global $DATA;
		$this->DATA = $DATA;
	}
	
	function getLocaciones() {
		$sql = "SELECT idlocation,latitud,longitud FROM location;";

		$rs = $this->DATA->Execute($sql);
		if ( $rs->RecordCount()) {
			while(!$rs->EOF){
				$id                 = $rs->fields['idlocation'];
				$info[$id]['lat']	= $rs->fields['latitud'];
				$info[$id]['lng']	= $rs->fields['longitud'];
				$rs->MoveNext();
			}
			$rs->Close();
			return $info;
		} else {
			return false;
		}
	}
	function eliminarAllMarker($id) {
		$sql = "DELETE FROM location WHERE idlocation=?;";
		$delete = $this->DATA->Execute($sql, $id);
		if ($delete) {
			return true;
		} else {
			return false;
		}
	}
	function nuevoMarker($params) {
		$sql = "INSERT INTO location (latitud,longitud) VALUES (?,?);";
		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true;
		} else {
			return false;
		}
	}
}
?>