<?php
session_start();
require_once '../data/dataBase.php';
require_once 'cMap.php';

$oP	= new Map();

$result	= $oP->getLocaciones();
$i = 0;
if($result){
	foreach($result  AS $id => $data ){
		$array[$i]['id']	= (int)trim($id);
		$array[$i]['lat']	= trim($data['lat']);
		$array[$i]['lng']	= trim($data['lng']);
		$i++;
	}
	echo json_encode($array);
} else { 
	echo "ndata";
}
?>