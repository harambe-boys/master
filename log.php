<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cLog.php";
	
	$oUsuario 		= new Usuario();
	$oLog 	= new Log();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	 /*if (!$_SESSION['Salazar']['permisos'][1]) {
		header("Location: index.php");
		exit();
	}*/
	
	$url = dirname($_SERVER["PHP_SELF"]); 
	
	$idLog				= "";
	$accion				= "";
	$titulo_push		= "";
	$mensaje_push	= "";
	$fecha				= "";
	$idUsuario			= "";
	$option    			= "";
	
	
		
	if (isset($_GET['opt'])) {
		$option    		= $_GET['opt'];
		$idLog 	= $_GET['idLog'];
	}

	$vLogs 	= $oLog->getLogsAll();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Logs de Notificaciones</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#logs").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				
				
				<?php
					if($option == "mUsuarioWeb"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
						
				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers"
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				<?php
				if ($option == "mUsuarioWeb") {
				?>
				if(form.password.value != form.cpassword.value){
					msg = 'Las Contrase\u00F1as no coinciden';
					is_error = true;
				}
				<?php
				}
				?>
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mUsuarioWeb") {
							echo 'msg = "Correo enviado.";';
						}else{
							echo 'msg = "El nuevo Usuario ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='usuariosWeb.php';",2500);
				}else if(responseText == 'pass') {
					msg = "La verificaci\u00F3n de la contrase\u00F1a no coinciden.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if (responseText == 'error1'){
					msg = "Necesita ingresar la contrase\u00F1a antigua.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 );
					document.getElementById("get").disabled = false;
				}else if (responseText == 'error2'){
					msg = "Contrase\u00F1as antigua incorrecta.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 ); 
					document.getElementById("get").disabled = false;
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
			
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >

					<div class="header"><span ><span class="ico fa fa-exchange  fa-2x"></span> Logs </span> </div>


					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" id="1" >

								<li id="hi1"><a href="#tab1"  id="3"  >  Lista de Logs</a></li>    

							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Usuario Web</th>
														<th>Acci&oacute;n</th>
														<th>T&iacute;tulo Push</th>
														<th>Mensaje Push</th>
														<th>Fecha de Creaci&oacute;n</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if($vLogs){
														foreach ($vLogs AS $id => $arrLogs) {
													?>
														<tr>
															<td ><?=$arrLogs['nombre'];?></td>
															<td><?=$arrLogs['accion'];?></td>		
															<td><?=$arrLogs['titulo_push'];?></td>																	
															<td><?=$arrLogs['mensaje_push'];?></td>
															<td ><?=$arrLogs['fechaCreacion'];?></td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>