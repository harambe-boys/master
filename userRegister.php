<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cUsersWeb.php";
	
	$oUsuario 		= new Usuario();
	$oUsuarioWeb 	= new UsersWeb();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	if (!$_SESSION['Altamira']['permisos'][5]) {
		header("Location: index.php");
		exit();
	}
	
	$url = dirname($_SERVER["PHP_SELF"]); 
	
	$idUsuario		= "";
	$usuario			= "";
	$nombre			= "";
	$cAcceso		= "";
	$cluster			= "";
	//$poligono		= "";
	$casa			= "";
	$fechaC   		= "";
	$estado 		="";
	$option    		= "";
	
	
		
	if (isset($_GET['opt'])) {
		$option    		= $_GET['opt'];
		$idUsuario 	= $_GET['idUsuario'];
	}
	
	if ($option == "mUsuarioRegister") {
		$vUsuario = $oUsuarioWeb->getUsuarioOne($idUsuario);
		foreach ($vUsuario AS $id => $info){  
			$idUsuario		= $id;
			$usuario		= $info["correo"];
			$nombre		= $info["nombre"];
			$cAcceso		= $info["codigoAcceso"];
			$cluster			= $info["cluster"];
			//$poligono		= $info["poligono"];
			$casa			= $info["casa"];
			$fechaC		= $info["fechaCreacion"];
			$estado 		= $info["estado"];
			$threeC  = substr(sha1($fechaC.$usuario),0,7);
			
		}
	}
	$vUsuariosE 	= $oUsuarioWeb->getUserAllenable();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Usuarios</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#userRegister").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				
				
				<?php
					if($option == "mUsuarioRegister"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
						
				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers"
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				<?php
				if ($option == "mUsuarioRegister") {
				?>
				if(form.password.value != form.cpassword.value){
					msg = 'Las Contrase\u00F1as no coinciden';
					is_error = true;
				}
				<?php
				}
				?>
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mUsuarioWeb") {
							echo 'msg = "El Usuario ha sido Modificado.";';
						}else{
							echo 'msg = "El nuevo Usuario ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='userRegister.php';",2500);
				}else if(responseText == 'pass') {
					msg = "La verificaci\u00F3n de la contrase\u00F1a no coinciden.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if (responseText == 'error1'){
					msg = "Necesita ingresar la contrase\u00F1a antigua.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 );
					document.getElementById("get").disabled = false;
				}else if (responseText == 'error2'){
					msg = "Contrase\u00F1as antigua incorrecta.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 ); 
					document.getElementById("get").disabled = false;
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
			
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >

					<div class="header"><span ><span class="ico fa fa-check fa-2x"></span> Usuarios Registrados  </span> </div>


					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >

								<li id="hi1"><a href="#tab1"  id="3"  >  Lista Usuarios Registrados</a></li>  
								<!--<li id="hi2"><a href="#tab2"  id="2"  >Modificar Usuario </a></li>-->   

							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Correo</th>
														<th>Nombre</th>
														<!--<th>Codigo de Acceso</th>-->
														<th>Calle</th>
														<th>Casa</th>
														<th>Fecha de Creaci&oacute;n</th>														
														<th>Estado</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vUsuariosE){
														foreach ($vUsuariosE AS $id => $arrUsuario) {
													?>
														<tr>
															<td><?=$arrUsuario['correo'];?></td>		
															<td><?=$arrUsuario['nombre'];?></td>
															<td ><?=$arrUsuario['cluster'];?></td>
															<td ><?=$arrUsuario['casa'];?></td>
															<td ><?=$arrUsuario['fechaCreacion'];?></td>
															<td ><?php if($arrUsuario['estado']==1){echo "activo";}else{echo "Inactivo";}?></td>
															<td><span class="tip" >
																	<a title="Detalle" href="detalle_usuario.php?id=<?=$id;?>">
																		<img src="images/icon/color_18/checkmark2.png" >
																	</a>
																	<?php 
																		if($arrUsuario['estado']==1){
																	?>
																			<a id="opt=bUsuario&idUsuario=<?=$id?>" class="actiones2" data-location="userRegister.php" data-action="actionWebUser.php" data-tittle="Bloquear" data-msg="&iquest;Quiere bloquear al usuario?" name="<?=$arrUsuario['correo']?>" title="Bloquear">
																				<img src="images/icon/color_18/delete.png" >
																			</a>
																	<?php
																		}
																	?>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>