<?php

include_once '../data/dataBase.php';
// Classes
include_once "../classes/cPushService.php";

//Abriendo Sesiones
session_start();

// Params    
$oPush 		= new PushService();


try{
	$msg 			= $_POST['m'];
	$idM 			= $_SESSION['mred']['municipio'];
	$vPushAndroid 	= $oPush->getAndroidPushAlerta2($idM);
	$vPushIOS 		= $oPush->getIOSPushAlerta2($idM);
	if($vPushAndroid){
		try{
			$oPush->PushAndroid($vPushAndroid,$msg,1,'',false);
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
	if($vPushIOS){
		try{
			$i=0;
			foreach($vPushIOS AS $id){
				$oPush->PushIOS2($id,$msg,1,$i,'');
				$i++;
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
	echo "done";
}catch(Exception $e){
	echo "error";
}

?>