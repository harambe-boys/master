 <div id="header" >
	<div id="account_info"> 
		<div class="setting" title="Perfil"><b>  bienvenido,</b> <b class="red"><?php echo( $_SESSION['Altamira']['user']);?></b><img src="images/gear.png" class="gear" ></a></div>
		<div class="logout" title="Desconectar"><b >Desconectar</b> <img src="images/connect.png" name="connect" class="disconnect" alt="disconnect" ></div> 
	</div>
</div> <!--//  header -->
<div id="shadowhead"></div>
<div id="hide_panel"></div>           
		   
<div id="left_menu">
	<ul id="main_menu" class="main_menu">
		<!-- Enter http://fontawesome.io/icons/ & search similar icon ex. fa-home --> 
		<?php 
		if($_SESSION['Altamira']['permisos'][14]){
		?>
		<li class="limenu" id="1m"><a href="index.php"><span class="fa fa-home fa-2x" ></span><b>Home</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][1]){
		?>
		<li class="limenu" id="Usuario"><a href="usuario.php"><span class="fa fa-user-plus fa-2x" ></span><b>Usuario Web</b></a></li>
		<?php
		}
		if($_SESSION['Altamira']['permisos'][8]){
		?>
		<li class="limenu" id="Rol"><a href="roles.php"><span class="fa fa-list-ul fa-2x" ></span><b>Roles</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][2]){
		?>
		<li class="limenu" id="userRegister"><a href="userRegister.php"><span class="fa fa-check fa-2x" ></span><b>Usuarios Registrados</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][3]){
		?>
		<li class="limenu" id="UserWeb"><a href="usuariosWeb.php"><span class="fa fa-user-times fa-2x" ></span><b>Verificaci&oacute;n de Usuarios</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][4]){
		?>
		<li class="limenu" id="Proveedor"><a href="proveedores.php"><span class="fa fa-user fa-2x" ></span><b>Proveedores</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][5]){
		?>
		<li class="limenu" id="Tipop"><a href="tipoProveedor.php"><span class="fa fa-list-alt fa-2x" ></span><b>Tipo de Proveedores</b></a></li>
		<?php 
		}
		if($_SESSION['Altamira']['permisos'][6]){
		?>
		<li class="limenu" id="atencionU"><a href="atencionUsuario.php"><span class="fa fa-users fa-2x" ></span><b>Atenci&oacute;n Usuario</b></a></li>
		<?php 
		}
		
		if($_SESSION['Altamira']['permisos'][10]){
		?>
		<li class="limenu" id="Cluster"><a href="calles.php"><span class="fa fa-bars fa-2x" ></span><b>Calles</b></a></li>
		<?php
		}
		if($_SESSION['Altamira']['permisos'][11]){
		
		?>
			<li class="limenu" id="Log"><a href="log.php"><span class="fa fa-exchange fa-2x" ></span><b>Log de Notificaciones</b></a></li>
		<?php
		}
		if($_SESSION['Altamira']['permisos'][12]){
		
		?>
		<li class="limenu" id="AlertasE">
			<a href="#">
				<span class="fa fa-map-marker fa-2x" ></span>
				<b>Alertas Espec&iacute;ficas</b>
			</a>
			<ul class="nav nav-third-level">
				<li>
					<a href="mapa_alerta_especifica.php">Mapa</a>
				</li>
				<li>
					<a href="detalle_alerta.php">Detalle</a>
				</li>
			</ul>
		</li>
		<?php
		}
		if($_SESSION['Altamira']['permisos'][13]){
		?>
		<li class="limenu" id="AlertasR"><a href="mapa_alertas.php"><span class="fa fa-map-marker fa-2x" ></span><b>Alertas R&aacute;pidas</b></a></li>
		<?php
		}
		if($_SESSION['Altamira']['permisos'][16]){
		?>
		<li class="limenu" id="Visitas"><a href="visitas.php"><span class="fa fa-users fa-2x" ></span><b>Visitas</b></a></li>
		<?php
		}
		?>
	</ul>
</div>