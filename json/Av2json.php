<?php
session_start();
require_once '../data/dataBase.php';
require_once '../classes/cMapaAlerta.php';

$oA	= new MapaAlerta();

if(isset($_SESSION['mred']['municipio'])){
	$e 	= 0;
	$ta = 0;
	if(isset($_POST['es'])){
		$e = $_POST['es'];
	}
	if(isset($_POST['ta'])){
		$ta = $_POST['ta'];
	}
	$municipio 	= $_SESSION['mred']['municipio'];
	$f1		= date('Y-m-d H:i:s');
	$f2 	= strtotime ( '-24 hours' , strtotime ( $f1 ) ) ;
	$f2 	= date ( 'Y-m-d H:i:s' , $f2 );
	$result	= $oA->getAM2($municipio,$f1,$f2,$e,$ta);
	$i = 0;
	if($result){
		foreach($result  AS $id => $data ){
			$array[$i]['id']	= (int)trim($id);
			$array[$i]['idAM']	= (int)trim($data['idAM']);
			$array[$i]['idM']	= (int)trim($data['idM']);
			$array[$i]['p']		= trim($data['p']);
			$array[$i]['time']	= date('Y/m/d H:i:s',strtotime($data['time']));
			$array[$i]['lon']	= trim($data['lon']);
			$array[$i]['lat']	= trim($data['lat']);
			$array[$i]['t']		= trim($data['t']);
			$array[$i]['tiempo']= utf8_encode(trim($data['tiempo']));
			$array[$i]['idE']	= (int)trim($data['idE']);
			$i++;
		}
		echo json_encode($array);
	} else { 
		echo "ndata";
	}
}
?>