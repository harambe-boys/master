<?php
session_start();
require_once '../data/dataBase.php';
require_once '../classes/cAlertasR.php';

$oA	= new AlertasR();

	$e 	= 0;
	$ta = 0;
	if(isset($_REQUEST['ide'])){
		$e = $_REQUEST['ide'];
	}
	if(isset($_REQUEST['idtpa'])){
		$ta = $_REQUEST['idtpa'];
	}
	$f1		= date('Y-m-d');
	$tma	= '-200 hours';
	/*if($_SESSION['mred']['webconfig']['tma'] != false){
		if($_SESSION['mred']['webconfig']['tma'] == '1d'){
			$tma	= '-24 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '2d'){
			$tma	= '-48 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '3d'){
			$tma	= '-72 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '4d'){
			$tma	= '-96 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '5d'){
			$tma	= '-120 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '6d'){
			$tma	= '-144 hours';
		}elseif($_SESSION['mred']['webconfig']['tma'] == '7d'){
			$tma	= '-168 hours';
		}
	}*/
	$f2 	= strtotime ( '-72 hours' , strtotime ( $f1 ) ) ;
	$f2 	= date ( 'Y-m-d' , $f2 );
	$result	= $oA->getAlertasR($e,$ta,$f1,$f2); 
	$i = 0;
	if($result){
		foreach($result  AS $id => $data ){
			$array[$i]['id']			= (int)trim($id);
			$array[$i]['latitud']		= trim($data['latitud']);
			$array[$i]['longitud']		= trim($data['longitud']);
			$array[$i]['time']			= date('Y/m/d H:i:s',strtotime($data['fechaCreacion']));
			$array[$i]['idusuario']		= (int)trim($data['idusuario']);
			$array[$i]['idTipoalerta']	= (int)trim($data['idTipoalerta']);
			$array[$i]['tipoAlerta']	= utf8_encode(trim($data['tipoAlerta']));
			$array[$i]['nombre']		= utf8_encode(trim($data['nombre']));
			$array[$i]['correo']		= trim($data['correo']);
			$array[$i]['cluster']		= trim($data['cluster']);
			$array[$i]['poligono']		= trim($data['poligono']);
			$array[$i]['casa']			= trim($data['casa']);
			$array[$i]['estado']		= trim($data['estado']);
			$array[$i]['tiempo']		= utf8_encode(trim($data['tiempo']));
			$i++;
		}
		echo json_encode($array); 
	} else { 
		echo "ndata";
	}
?>