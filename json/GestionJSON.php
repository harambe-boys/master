<?php
session_start();
require_once '../data/dataBase.php';
require_once '../classes/cMapaGestion.php';

$oG	= new MapaGestionM();

if(isset($_SESSION['mred']['municipio'])){
	$e 	= 0;
	$tg = 0;
	if(isset($_POST['es'])){
		$e = $_POST['es'];
	}
	if(isset($_POST['tg'])){
		$tg = $_POST['tg'];
	}
		
	$municipio 	= $_SESSION['mred']['municipio'];
	$f1		= date('Y-m-d H:i:s');
	$tmg	= '-24 hours';
	if($_SESSION['mred']['webconfig']['tmg'] != false){
		if($_SESSION['mred']['webconfig']['tmg'] == '1d'){
			$tmg	= '-24 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '2d'){
			$tmg	= '-48 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '3d'){
			$tmg	= '-72 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '4d'){
			$tmg	= '-96 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '5d'){
			$tmg	= '-120 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '6d'){
			$tmg	= '-144 hours';
		}elseif($_SESSION['mred']['webconfig']['tmg'] == '7d'){
			$tmg	= '-168 hours';
		}
	}
	$f2 	= strtotime ( $tmg , strtotime ( $f1 ) ) ;
	$f2 	= date ( 'Y-m-d H:i:s' , $f2 );
	$result	= $oG->getGM($municipio,$f1,$f2,$e,$tg);
	$i = 0;
	if($result){
		foreach($result  AS $id => $data ){
			$array[$i]['id']	= (int)trim($id);
			$array[$i]['idGM']	= (int)trim($data['idGM']);
			$array[$i]['idM']	= (int)trim($data['idM']);
			$array[$i]['p']		= trim($data['p']);
			$array[$i]['time']	= date('Y/m/d H:i:s',strtotime($data['time']));
			$array[$i]['lon']	= trim($data['lon']);
			$array[$i]['lat']	= trim($data['lat']);
			$array[$i]['t']		= trim($data['t']);
			$array[$i]['title']	= trim($data['title']);
			$array[$i]['img']	= trim($data['img']);
			$array[$i]['msg']	= trim($data['msg']);
			$array[$i]['tiempo']= utf8_encode(trim($data['tiempo']));
			$array[$i]['idE']	= (int)trim($data['idE']);
			$i++;
		}
		echo json_encode($array);
	} else { 
		echo "ndata";
	}
}
?>