<?php
session_start();
require_once '../data/dataBase.php';
require_once '../classes/cMapaGestion.php';

$oGestion	= new MapaGestionM();


//Filter tipo alertas
if(isset($_GET['idGM'])){
	if(isset($_SESSION['mred']['municipio'])){
		$municipio 	= $_SESSION['mred']['municipio'];
		$t 		= $_GET['idGM'];
		$e		= $_GET['idE'];
		$f1		= date('Y-m-d');
		$tmg	= '-24 hours';
		if($_SESSION['mred']['webconfig']['tmg'] != false){
			if($_SESSION['mred']['webconfig']['tmg'] == '1d'){
				$tmg	= '-24 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '2d'){
				$tmg	= '-48 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '3d'){
				$tmg	= '-72 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '4d'){
				$tmg	= '-96 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '5d'){
				$tmg	= '-120 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '6d'){
				$tmg	= '-144 hours';
			}elseif($_SESSION['mred']['webconfig']['tmg'] == '7d'){
				$tmg	= '-168 hours';
			}
		}
		$f2 	= strtotime ( '-24 hours' , strtotime ( $f1 ) ) ;
		$f2 	= date ( 'Y-m-d' , $f2 );
		$result	= $oGestion->getTipoGM($e,$t,$municipio,$f1,$f2);
		$i = 0;
		if($result){
			foreach($result  AS $id => $data ){
				$array[$i]['id']		= (int)trim($id);
				$array[$i]['idGM']		= (int)trim($data['idGM']);
				$array[$i]['idM']		= (int)trim($data['idM']);
				$array[$i]['time']		= trim($data['time']);
				$array[$i]['lon']		= trim($data['lon']);
				$array[$i]['lat']		= trim($data['lat']);
				$array[$i]['t']			= trim($data['t']);
				$array[$i]['pt']		= trim($data['idP']);
				$array[$i]['tiempo']	= utf8_encode(trim($data['tiempo']));
				$array[$i]['idE']		= (int)trim($data['idE']);
				if(trim($data['msg'])==null){
					$array[$i]['msg']		= null;
				}else{
					$array[$i]['msg']		= utf8_encode(trim($data['msg']));
				}
				if(trim($data['img'])==null){
					$array[$i]['img']		= null;
				}else{
					$array[$i]['img']		= trim($data['img']);
				}
				if(trim($data['ti'])==null){
					$array[$i]['ti']		= null;
				}else{
					$array[$i]['ti']		= utf8_encode(trim($data['ti']));
				}
				
				$i++;
			}
			echo json_encode($array);
		} else { 
			echo 'error';
		}
	}
} 



?>