<?php
session_start();
include_once '../data/dataBase.php';
include_once '../classes/cVisitas.php';

$uVisitas  = new Visitas();

try{
	if(isset($_REQUEST['fecIniciosearch'])){
		$myDateTime = DateTime::createFromFormat('d/m/Y', $_REQUEST['fecIniciosearch']);
		$f1 = $myDateTime->format('Y-m-d');
	}else{
		echo "fdata";
		exit();
	}
	
	if(isset($_REQUEST['fecFinsearch'])){
		$myDateTime = DateTime::createFromFormat('d/m/Y', $_REQUEST['fecFinsearch']);
		$f2 = $myDateTime->format('Y-m-d');
	}else{
		echo "fdata";
		exit();
	}
	if(isset($_REQUEST['estado'])){
		$estado = $_REQUEST['estado'];
	}else{
		echo "fdata";
		exit();
	}
	
	$rs = $uVisitas->getReporteVisitas($estado,$f1,$f2);
	$arr = array();
	$i=0;
	if($rs){
		foreach ($rs AS $id => $array) {
			$arr[$i]["id"] 					= $id;
			$arr[$i]["nombre_visitante"] 	= ($array["nombre_visitante"]);
			$arr[$i]["placa_vehiculo"] 		= ($array["placa_vehiculo"]);
			$arr[$i]["notas"] 				= ($array["notas"]);
			$arr[$i]["residente"] 			= ($array["residente"]);
			$arr[$i]["cluster"] 			= ($array["cluster"]);
			$arr[$i]["poligono"] 			= ($array["poligono"]);
			$arr[$i]["casa"] 				= ($array["casa"]);
			$arr[$i]["fecha"] 				= ($array["fecha"]);
			$arr[$i]["hora"]	 			= ($array["hora"]);
			$arr[$i]["hora_confirmacion"]	= ($array["hora_confirmacion"]);
			$arr[$i]["estado"]				= ($array["estado"]);
			$i++;
		}
		$_SESSION['Altamira']['reporte_excel'] = $arr;
		echo json_encode($arr);
	}else{
		echo "ndata";
	}
}catch(Exception $e){
	echo $e;
}
?>