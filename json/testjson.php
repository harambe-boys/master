<?php
session_start();
require_once '../data/dataBase.php';
require_once '../classes/cMapaGestion.php';

$oGestion	= new MapaGestionM();

if(isset($_SESSION['mred']['municipio'])){
	
	$municipio 	= $_SESSION['mred']['municipio'];
	$f1		= date('Y-m-d');
	$f2 	= strtotime ( '-3 day' , strtotime ( $f1 ) ) ;
	$f2 	= date ( 'Y-m-d' , $f2 );
	$result	= $oGestion->getGMtest($municipio,$f1,$f2);
	$i = 0;
	if($result){
		foreach($result  AS $id => $data ){
			$array[$i]['id']	= (int)trim($id);
			$array[$i]['idGM']	= (int)trim($data['idGM']);
			$array[$i]['idM']	= (int)trim($data['idM']);
			$array[$i]['p']		= trim($data['p']);
			$array[$i]['time']	= trim($data['time']);
			$array[$i]['lon']	= trim($data['lon']);
			$array[$i]['lat']	= trim($data['lat']);
			$array[$i]['t']		= trim($data['t']);
			$array[$i]['tiempo']= utf8_encode(trim($data['tiempo']));
			$array[$i]['idE']	= (int)trim($data['idE']);
			$i++;
		}
		echo json_encode($array);
	} else { 
		echo "ndata";
	}
}
?>