<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cAlertasR.php";
	
	$oAlertas   = new AlertasR();
	$idE 		= $_SESSION['Salazar']['e'];
	$idTGM  	= $_SESSION['Salazar']['g'];
	$F1 		= $_SESSION['Salazar']['f1'];
	$F2 		= $_SESSION['Salazar']['f2'];
	
	
	if(isset($_REQUEST['g'])){
		$idTGM 	= $_REQUEST['g'];
		$_SESSION['Salazar']['g'] = $idTGM;
	}
	if(isset($_REQUEST['e'])){
		$idE 	= $_REQUEST['e'];
		$_SESSION['Salazar']['e'] = $idE;
	}
	if(isset($_REQUEST['fechaI'])){
		$F1 = $_REQUEST['fechaI'];
		$F1 = explode('-',$F1,3);
		$F1 = $F1[2].'-'. $F1[1].'-'.$F1[0]. ' '. '00:00:01';
		$_SESSION['Salazar']['f1'] = $F1;
	}
	if(isset($_REQUEST['fechaF'])){
		$F2 = $_REQUEST['fechaF'];
		$F2 = explode('-',$F2,3);
		$F2 = $F2[2].'-'. $F2[1].'-'.$F2[0]. ' '. '23:59:59';
		$_SESSION['Salazar']['f2'] = $F2;
	}
		
	$elementos = array();
	$vAlertas = $oAlertas->getAlertasE2($idE,$idTGM,$F1,$F2);
	if($vAlertas){
		$i = 0;
		foreach($vAlertas AS $id => $array){
			$elementos[$i]['id']			= (int)trim($id);
			$elementos[$i]['latitud']		= trim($array['latitud']);
			$elementos[$i]['longitud']		= trim($array['longitud']);
			$elementos[$i]['time']			= date('Y/m/d H:i:s',strtotime($array['fechaCreacion']));
			$elementos[$i]['idusuario']		= (int)trim($array['idusuario']);
			$elementos[$i]['idTipoalerta']	= (int)trim($array['idTipoalerta']);
			$elementos[$i]['tipoAlerta']	= utf8_encode(trim($array['tipoAlerta']));
			$elementos[$i]['nombre']		= utf8_encode(trim($array['nombre']));
			$elementos[$i]['correo']		= trim($array['correo']);
			$elementos[$i]['cluster']		= trim($array['cluster']);
			$elementos[$i]['poligono']		= trim($array['poligono']);
			$elementos[$i]['casa']			= trim($array['casa']);
			$elementos[$i]['estado']		= trim($array['estado']);
			$elementos[$i]['tiempo']		= utf8_encode(trim($array['tiempo']));
			$i++;
		}
		echo json_encode($elementos);
	}else{
		echo "ndata";
	}
?>
