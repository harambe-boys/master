﻿<?php
//LLAMAMOS AL FRAMEWORK DE SLIM
	require 'Slim/Slim.php';
	require '../classes/PHPmailer/PHPMailerAutoload.php';
	
//INSTANCIAMOS EL API 
	$app = new Slim();
	
//DEFINIMOS LOS TIPOS DE SERVICIOS QUE NECESITAMOS, LAS URLS Y LAS FUNCIONES QUE EJECUTARAN
	//S01
	$app->post('/RegistroUsuario','setRegistro');
	//S02
	$app->post('/setLogin','setLogin');
	//S03
	$app->post('/AtencionUsuario','setAtencionusuario');
	//S04
	$app->post('/Referidos','setReferidos'); 
	//S05
	$app->post('/Alertas','setAlerta');
	//S06
	$app->get('/getTiposProveedores/:idusuario/:tokenSesion','getTipoproveedores');
	//S07
	$app->put('/setModificacionPush', 'setModificarPush');
	//S08
	$app->get('/getProveedor/:id/:iduser/:tokenSesion', 'getProveedores');
	//S09
	$app->put('/setEstadonotificacion', 'setestadoNotificacion');
	//S10
	$app->post('/setReporteRobo','SetReporterobo'); 
	//S11
	$app->post('/setPuntuacionproveedores','setLikeproveedores');
	//S12
	$app->delete('/eliminarLike/:idusuario/:idProveedor/:estadoLike/:tokenSesion', 'deleteLike');
	//S13
	$app->put('/setPuntuacionproveedores', 'setPuntuacionproveedores');
	//S13
	$app->put('/setModificarContrasenia', 'setModificarPassword');
	//S14
	$app->put('/setModificarContrasenia', 'setModificarPassword');
	//S15
	$app->put('/setVerificacionCorreov2', 'setVerificacionCorreov2');
	//S16
	$app->put('/setResetpassword', 'setResetContrasenia');
	//S17
	$app->get('/getNotificaciones/:idusuario/:tokenSesion','getNotificacionesPush');
	//S18
	$app->put('/getDeleteNotificacion','setUpdateNotificacion');

	
	//S19
	$app->put('/setCerrarSesion', 'setCerrarSesion');
	
	$app->post('/visitas', 'visitas');

	//SERVICIOS NUEVOS -- 31-05-2016---BY GABO
	//S20
	$app->get('/getVisita/:tokenSesion/:idusuario', 'getVisita');
	#Catalogo de calles
	$app->get('/getCalle', 'getCalle');
	//S21
	$app->post('/eliminarVisita', 'eliminarVisita');
	//S22
	$app->post('/setModificarVisita', 'setModificarVisita');
//DECLARAMOS LA RESPUESTA COMO JSON Y QUE SEA UTF 8
	$app->response()->header('Content-Type','application/json','charset=utf-8');
	
//CORREMOS EL API
	$app->run();
	
//CREAMOS LA CONEXION A LA DB
	function getConexion(){
		$dbhost="localhost";
		$dbuser="root";
		$dbpass='Pa$$w0rd';
		$dbname="Altamira";
		$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbh;
	}
		
//FUNCIONES GLOBALES EJECUTADAS EN LOS WS
//BY GABO--------31-05-2016:InTheMorning

function setModificarVisita(){
	$request  	= Slim::getInstance()->request();
	$data 		= json_decode($request->getBody());
	$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
	if($comprobaciontoken==true){

		try {
			$db = getConexion();

			$visits = $data->visits;

			foreach ($visits as $key) {
				$sql   =  "UPDATE visita SET nombre=:nombre, placa_vehiculo=:placa_vehiculo, fecha=:fecha, hora=:hora, notas=:notas, dui=:dui WHERE
							idvisita=:idVisita";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("idVisita", $key->idVisita);
				$stmt->bindParam("nombre", $key->nombre);
				$stmt->bindParam("placa_vehiculo", $key->placa_vehiculo);
				$stmt->bindParam("fecha", $key->fecha);
				$stmt->bindParam("hora", $key->hora);
				$stmt->bindParam("notas", $key->notas);
				$stmt->bindParam("dui", $key->dui);
				$stmt->execute();
			}

			echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado correctamente",
									 "msg": ""
								}
							}';

			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();
			echo $e;
			/*$tipoerror 		= "WebServices SP12";
            $dates			= date("Y-m-d H:i:s");
            seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
			echo '{"response":{
						"errorCode": 3,
						"errorMessage":"Error en ejecucion de servicio web",
						"msg":""
					}}';
		}

	}else{
		echo  	'{"response": {
							"errorCode": 7,
							"errorMessage": "Token de sesion no valido.",
							"msg":""
						}
					}';
	}
}
/*function setModificarPassword(){
	$request  = Slim::getInstance()->request();
	$data = json_decode($request->getBody());

	$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
	if($comprobaciontoken==true){
		try {
			$db = getConexion();
			$sql   = "UPDATE usuario SET password=:password, primerRegistro=0 WHERE idusuario=:idusuario;";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idusuario", $data->idusuario);
			$stmt->bindParam("password", $data->password);
			$stmt->execute();
			echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Servicio ejecutado con exito",
								"msg": ""
							}
						}';
			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();

			echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"
							}
						}';
		}
	}else{
		echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""
					}
				}';
	}
}*/

function eliminarVisita(){
	$request = Slim::getInstance()->request();
	$data = json_decode($request->getBody());

	$sql = "DELETE FROM visita WHERE idvisita =:idvisita";
	$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);

	if ( $comprobaciontoken == true ){
		try{
			$db = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idvisita", $data->idvisita);
			$stmt->execute();
			if( $stmt->rowCount() > 0 ){
				echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": "Visita Eliminada"							
								}
							}';
			}
		}catch (PDOException $e){
			echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
		}
	}else{
		echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';
	}
}

#getCalle
function getCalle(){
	try{
		$sql = "SELECT * FROM cluster WHERE estado = 1;";
		$i = 0;
		$db   = getConexion();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		if( $stmt->rowCount() > 0 ) {
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$array[$i]['idcluster']   				= trim($data['idcluster']);
				$array[$i]['cluster']   				= trim($data['cluster']);
				$i++;
			}
			echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($array).'							
								}
							}';
		}else{
			echo 	'{"response": {
									"errorCode": 1,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": "No hay datos"							
								}
							}';
		}
	}catch (Exception $e){
		echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
	}
}


function getVisita($tokenSesion, $idusuario){
	$sql = "SELECT idvisita, nombre, placa_vehiculo, fecha, hora, notas, dui FROM visita WHERE idusuario = $idusuario;";
	$comprobaciontoken = getverificaciontoken($tokenSesion,$idusuario);
	if ( $comprobaciontoken == true ){
		try{
			$i = 0;
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->execute();
			if( $stmt->rowCount() > 0 ){
				while( $data = $stmt->fetch(PDO::FETCH_ASSOC) ){
					$array[$i]['idvisita']   				= trim($data['idvisita']);
					$array[$i]['nombre']   					= trim($data['nombre']);
					$array[$i]['placa_vehiculo']   			= trim($data['placa_vehiculo']);
					$array[$i]['fecha']   					= trim($data['fecha']);
					$array[$i]['hora']   					= trim($data['hora']);
					$array[$i]['notas']   					= trim($data['notas']);
					$array[$i]['dui']   				 	= trim($data['dui']);
					$i++;
				}
				echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($array).'							
								}
							}';
			}else{
				echo 	'{"response": {
									"errorCode": 1,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": "No hay datos"							
								}
							}';
			}

		}catch (PDOException $e){
			$e = $e->getMessage();

			echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
		}
	}else{
		echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';
	}
}
//S09 - PUT - ESTADO NOTIFICACION
function setestadoNotificacion(){
	$request  = Slim::getInstance()->request();
	$data = json_decode($request->getBody());
	$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);

	if($comprobaciontoken==true){
		try {
			$db = getConexion();
			$sql   = "UPDATE usuario SET estadoNotificacion=:estadoNotificacion WHERE idusuario=:idusuario;";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idusuario", $data->idusuario);
			$stmt->bindParam("estadoNotificacion", $data->estadoNotificacion);
			$stmt->execute();
			echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Servicio ejecutado con exito",
								"msg": ""
							}
						}';
			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();
			
			echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}';
		}
	}else{
		echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';
	}
}
	
	/*function seterrorlogwebservices($tipoerror,$e,$dates,$dispositivo){ 
		$sql    = "INSERT INTO logerrorws (ws,descripcion,datetime,iddispositivo) VALUES (:tipoerror,:e,:datetime,:dispositivo);";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("tipoerror", $tipoerror);
			$stmt->bindParam("e", $e);
			$stmt->bindParam("dispositivo", $dispositivo);
			$datetime 	= date("Y-m-d H:i:s");
			$stmt->bindParam("datetime", $datetime);
			$stmt->execute();
		} catch (PDOException $e) {
			echo $e;
		} 
	}*/
	
	function getZoneLomasAltamira($lat,$long){
		$db 	= getConexion();
		$sql = "SELECT idlocation,TRUNCATE((6371 * ACOS( COS( RADIANS($lat) ) 
						 * COS(RADIANS(latitud)) 
						 * COS(RADIANS(longitud) 
						 - RADIANS($long) ) 
						 + SIN( RADIANS($lat) ) 
						 * SIN(RADIANS(latitud)) 
						) * 1000),0
						   ) AS distance  
					FROM location ORDER BY distance ASC LIMIT 0,1;"; 
		$stmt	= $db->query($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($e = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idlocation	= trim($e['idlocation']);
				$distance	= ($e['distance']);
			}
		}
		$db = null;
		if($distance<1000){
			return $idlocation;
		}else{
			return false;
		}
		
	}
	
	/*funcion que verifica si el correo ya esta registrado en la base de datos*/
	function getverificacioncorreo($correo){
		$sql = "SELECT idusuario FROM usuario WHERE correo =:correo;";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("correo",$correo);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				return true;
			}else{
				return false;
			}
			$db = null;
		}catch(PDOException $e){
			return $e;
		}
	}
	
	
	
	//FUNCION QUE ENVIA CORREO CON PING PARA RESETEAR PASSWORD
	function send_ping($correo,$ping1){
	//echo($correo);
	//echo($password);
		$mensaje = file_get_contents("../template/ping.php");
		$mensaje = str_replace("{{CORREO}}", utf8_decode($correo), $mensaje);
		$mensaje = str_replace("{{PING}}", utf8_decode($ping1), $mensaje);
		$mensaje = str_replace("{{YEAR}}", date('Y'), $mensaje);
		
		$para = $correo;
		$asunto = 'Pin para modificar clave de acceso a la App'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		//$mailPHP->SMTPDebug = 2; 
		//Debo de hacer autenticaci�n SMTP
		$mailPHP->SMTPAuth = true;
		$mailPHP->SMTPSecure = "ssl";
		//indico el servidor de Gmail para SMTP
		$mailPHP->Host = "smtp.gmail.com";
		//indico el puerto que usa Gmail
		$mailPHP->Port = 465;
		//indico un usuario / clave de un usuario de gmail
		$mailPHP->Username = "rauleduardoguardado1991@gmail.com";
		$mailPHP->Password = 'vecchiasignora';
		$mailPHP->SetFrom('rauleduardoguardado1991@gmai', 'Lomas de Altamira'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$address = $para;
		$mailPHP->AddAddress($address, $correo);
		$mailPHP->Send();
	}
	
	
	//FUNCION QUE ENVIA CORREO AL EJECUTIVO DE VENTAS DE LOS PARENTESCOS DE LOS USUARIOS REGISTRADOS
	function send_parentesco($nombre,$telefono,$parentesco,$correo,$correoU){
	//echo($correo);
	//echo($password);
		$mensaje = file_get_contents("../template/parentesco.php");
		$mensaje = str_replace("{{NOMBREU}}", utf8_decode($correoU), $mensaje);
		$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nombre), $mensaje);
		$mensaje = str_replace("{{TELEFONO}}", utf8_decode($telefono), $mensaje);
		$mensaje = str_replace("{{PARENTESCO}}", utf8_decode($parentesco), $mensaje);
		$mensaje = str_replace("{{CORREO}}", utf8_decode($correo), $mensaje);
		$mensaje = str_replace("{{YEAR}}", date('Y'), $mensaje);
		
		$para = 'raulguardado100891@gmail.com';
		$asunto = 'Verifiaci�n de parentesco de usuarios de Lomas de Altamira '; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		//$mailPHP->SMTPDebug = 2; 
		//Debo de hacer autenticaci�n SMTP
		$mailPHP->SMTPAuth = true;
		$mailPHP->SMTPSecure = "ssl";
		//indico el servidor de Gmail para SMTP
		$mailPHP->Host = "smtp.gmail.com";
		//indico el puerto que usa Gmail
		$mailPHP->Port = 465;
		//indico un usuario / clave de un usuario de gmail
		$mailPHP->Username = "rauleduardoguardado1991@gmail.com";
		$mailPHP->Password = 'vecchiasignora';
		$mailPHP->SetFrom('rauleduardoguardado1991@gmai', 'Lomas de Altamira'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$address = $para;
		$mailPHP->AddAddress($address, $correo);
		$mailPHP->Send();
	}
	
		function send_reporteRobo($correo){
	//echo($correo);
	//echo($password);
		$mensaje = file_get_contents("../template/reporte_robo.php");
		$mensaje = str_replace("{{CORREO}}", utf8_decode($correo), $mensaje);
		$mensaje = str_replace("{{YEAR}}", date('Y'), $mensaje);
		
		$para = 'patricia2014avela@hotmail.com';
		$asunto = 'Reporte de Robo o extravío de Código de Acceso a Lomas de Altamira '; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		//$mailPHP->SMTPDebug = 2; 
		//Debo de hacer autenticaci�n SMTP
		$mailPHP->SMTPAuth = true;
		$mailPHP->SMTPSecure = "ssl";
		//indico el servidor de Gmail para SMTP
		$mailPHP->Host = "smtp.gmail.com";
		//indico el puerto que usa Gmail
		$mailPHP->Port = 465;
		//indico un usuario / clave de un usuario de gmail
		$mailPHP->Username = "rauleduardoguardado1991@gmail.com";
		$mailPHP->Password = 'vecchiasignora';
		$mailPHP->SetFrom('rauleduardoguardado1991@gmai', 'Lomas de Altamira'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$address = $para;
		$mailPHP->AddAddress($address, $correo);
		$mailPHP->Send();
	}
	
	
	/*funcion que nos permite comprobar si el token del usuario es correcto para desplegar la informacion de la consulta de 
		cualquier servicio web o retornar error y evitar mostrar datos.*/	
	function getverificaciontoken($token,$idusuario){
		return true;
		$sql = "SELECT tokenSesion FROM usuario WHERE idusuario =:id";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id",$idusuario);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				while ($tokens  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$tokenS    = trim($tokens['tokenSesion']);
				}
				if($token ==$tokenS){
					return true;
				}
			}else{
				return false;
			}
			$db = null;
		}catch(PDOException $e){
			return $e;
		}
	}
	
	function getverificacionping($ping){
		$sql = "SELECT ping FROM usuario WHERE ping =:ping";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("ping",$ping);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				while ($pings  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$pingS    = trim($pings['ping']);
				}
				if($ping ==$pingS){
					return true;
				}
			}else{
				return false;
			}
			$db = null;
		}catch(PDOException $e){
			return $e;
		}
	}
	
	
	function getUserProveedor($idusuario,$idProveedor){
		$sql = "SELECT idusuario, idProveedor FROM usuario_has_proveedores WHERE idusuario=:idusuario AND idProveedor =:idProveedor;";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idusuario",$idusuario);
			$stmt->bindParam("idProveedor",$idProveedor);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				while ($users  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$idusuario    	= trim($users['idusuario']);
						$idProveedor    = trim($users['idProveedor']);
				}
				if($sql){
					return true;
				}
			}else{
				return false;
			}
			$db = null;
		}catch(PDOException $e){
			return $e;
		}
	}
	
	function getCorreo($idusuario){ 
		$sql = "SELECT idusuario, correo FROM usuario WHERE idusuario=:idusuario;";
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idusuario",$idusuario);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				while ($users  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$idusuario    	= trim($users['idusuario']);
						$correo    = trim($users['correo']);
				}
				if($sql){
					return $correo;
				}
			}else{
				return false;
			}
			$db = null;
		}catch(PDOException $e){
			return $e;
		}
	}
	

	/*function interval_date($init,$finish){
		//formateamos las fechas a segundos tipo 1374998435
		$diferencia = strtotime($finish) - strtotime($init);
	 
		//comprobamos el tiempo que ha pasado en segundos entre las dos fechas
		//floor devuelve el n?mero entero anterior, si es 5.7 devuelve 5
		if($diferencia < 60){
			$tiempo = 'Hace ' . floor($diferencia) . ' segundos';
		}else if($diferencia > 60 && $diferencia < 3600){
			$tiempo = 'Hace ' . floor($diferencia/60) . ' minutos';
		}else if($diferencia > 3600 && $diferencia < 86400){
			$tiempo = 'Hace ' . floor($diferencia/3600) . ' horas';
		}else if($diferencia > 86400 && $diferencia < 2592000){
			$tiempo = 'Hace ' . floor($diferencia/86400) . ' d�as';
		}else if($diferencia > 2592000 && $diferencia < 31104000){
			$tiempo = 'Hace ' . floor($diferencia/2592000) . ' meses';
		}else if($diferencia > 31104000){
			$tiempo = 'Hace ' . floor($diferencia/31104000) . ' a�os';
		}else{
			$tiempo = 'Error';
		}
		return $tiempo;
	}*/
	
	/*function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}*/

	/*function secondsToTime($seconds){
		// extract hours
		$hours = floor($seconds / (60 * 60));
	 
		// extract minutes
		$divisor_for_minutes = $seconds % (60 * 60);
		$minutes = floor($divisor_for_minutes / 60);
	 
		// extract the remaining seconds
		$divisor_for_seconds = $divisor_for_minutes % 60;
		$seconds = ceil($divisor_for_seconds);
	 
		// return the final array
		$obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		); 
		return $obj;
	}*/
//FUNCIONES QUE SE MANDAN A LLAMAR EN LOS WS ----- POR ESTANDAR ESTOS SERAN MR--
	
//S01 - POST - GUARDAR NUEVO REGISTRO

	function setRegistro(){ 
		$sql    = "INSERT INTO usuario (tokenPush,nombre,codigoAcceso,cluster,casa,correo,idDipositivo,estadoNotificacion,primerRegistro) VALUES (:tokenPush,:nombre,:codigoAcceso,:cluster,:casa,:correo,:idDipositivo,:estadoNotificacion,:primerRegistro);";
		try{
			$p 		= Slim::getInstance()->request();
			$data 	= json_decode($p->getBody());
			$vCorreo = getverificacioncorreo($data->correo);
			if(!$vCorreo){
				$db 	= getConexion();
				$stmt = $db->prepare($sql);
				$data->estadoNotificacion = "1";
				$data->primerRegistro = "1";
				$stmt->bindParam("tokenPush", $data->tokenPush);
				$stmt->bindParam("nombre", $data->nombre);
				$stmt->bindParam("codigoAcceso", $data->codigoAcceso);
				$stmt->bindParam("cluster", $data->cluster);
				//$stmt->bindParam("poligono", $data->poligono);
				$stmt->bindParam("casa", $data->casa);
				$stmt->bindParam("correo", $data->correo);
				$stmt->bindParam("idDipositivo", $data->idDipositivo);
				$stmt->bindParam("estadoNotificacion", $data->estadoNotificacion);
				$stmt->bindParam("primerRegistro", $data->primerRegistro);
				$stmt->execute();
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Registro Almacenado",
									"token": "'.$data->tokenPush.'"
								}
							}';
			}else {
				echo 	'{"response": {
									"errorCode": 1,
									"errorMessage": "Correo ya registrado",
									"token": "0",
									"idusuario":"0"
								}
							}';	
			}
			
			
			$db = null;
		} catch (PDOException $e) {
			echo $e;
		} 
	}	
	
	
//S02 - POST-  LOGIN USUARIO 
	function setLogin(){ 
		$sql    = "SELECT idusuario,correo,tokenPush,nombre,codigoAcceso,cluster,casa,tokenSesion,idDipositivo,estadoNotificacion,primerRegistro FROM usuario WHERE correo=:correo AND password =:password AND estado = 1;";
		
		$p 		= Slim::getInstance()->request();
		$data 	= json_decode($p->getBody());
		try{
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("correo",$data->correo);
			$stmt->bindParam("password",$data->password);
			$stmt->execute();
			$fecha = date("Y-m-d H:i:s");
			$tokenSesion = sha1($fecha.$data->correo); 
			if ( $stmt->rowCount() > 0 ) {
				while ($login  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$arrUser['idusuario']   				= trim($login['idusuario']);
						$idUsuario			  					= trim($login['idusuario']);
						$arrUser['correo'] 					= trim($login['correo']);
						$arrUser['nombre'] 					= trim($login['nombre']);
						$arrUser['codigoAcceso']			= trim($login['codigoAcceso']);
						$arrUser['tokenPush'] 					= trim($login['tokenPush']);
						$arrUser['cluster'] 					= trim($login['cluster']);
						$arrUser['casa'] 						= trim($login['casa']);
						$arrUser['tokenSesion']  			= $tokenSesion;
						$arrUser['estadoNotificacion']		= trim($login['estadoNotificacion']);
						$arrUser['primerRegistro']		= trim($login['primerRegistro']);
						
				}
				$sql2 = "UPDATE usuario SET tokenSesion=:tokenSesion, idDipositivo=:idDipositivo WHERE idusuario=:id ";
				try{
					$stmt = $db->prepare($sql2);
					$stmt->bindParam("id",$idUsuario);
					$stmt->bindParam("tokenSesion",$tokenSesion);
					$stmt->bindParam("idDipositivo",$data->idDispositivo);
					$stmt->execute();
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($arrUser).'  
								}
							}';
							
							
				}catch(PDOException $e){
					$db = null;
					/*$e = $e->getMessage();
					$tipoerror 		= "WebServices SP02";
					$dates			= date("Y-m-d H:i:s");
					seterrorlogwebservices($tipoerror,$e,$dates,$idDispositivo);
					echo 	'{"response": {
									"errorCode": 4,
									"errorMessage": "No se pudo almacenar el token",
									"msg": "0"  
								}
							}';*/
							
				}
			}else{
				echo 	'{"response": {
									"errorCode": 1,
									"errorMessage": "Datos incorrectos de password o usuario",
									"msg": "0"  
								}
							}';
			}
			$db = null;
		} catch(PDOException $e){
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP02";
			$dates			= date("Y-m-d H:i:s");
			$dispositivo	= "5";
			seterrorlogwebservices($tipoerror,$e,$dates,$iddispositivo);*/
			echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
						
		}
	}	
	
//S03 - POST - ATENCI�N USUARIO
	function setAtencionusuario(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
	
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){
			try {
				$db = getConexion();
				$sql   = "INSERT INTO atencionusuario (asunto,descripcion,imagen,idusuario) VALUES (:asunto,:descripcion,:imagen,:idusuario);";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("asunto", $data->asunto);
				$stmt->bindParam("descripcion", $data->descripcion);
				$stmt->bindParam("imagen", $data->imagen);
				$stmt->bindParam("idusuario", $data->idusuario);
				$stmt->execute();
				$idusuario = $data->idusuario;
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Registro Almacenado",
								"idusuario": "'.$idusuario.'"
							}
						}';
				
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP12";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S04 - POST - REFERIDOS
	function setReferidos(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		$db = getConexion();

		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		$correoUsuario = getCorreo($data->idusuario);
			
		if($comprobaciontoken==true){
			try {
				
				$sql   = "INSERT INTO planreferido (nombreReferido,telefonoReferido,parentescoReferido,correo,idusuario) VALUES (:nombreReferido,:telefonoReferido,:parentescoReferido,:correo,:idusuario);";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("nombreReferido", $data->nombreReferido);
				$stmt->bindParam("telefonoReferido", $data->telefonoReferido);
				$stmt->bindParam("parentescoReferido", $data->parentescoReferido);
				$stmt->bindParam("correo", $data->correo);
				$stmt->bindParam("idusuario", $data->idusuario);
				$stmt->execute();
				
				
				
				if(isset($data->nombreReferido) && isset($data->telefonoReferido) && isset($data->parentescoReferido) && isset($data->correo) && isset($correoUsuario)){
					send_parentesco($data->nombreReferido,$data->telefonoReferido,$data->parentescoReferido,$data->correo,$correoUsuario);
				}
				
				$idusuario = $data->idusuario;
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Registro Almacenado",
								"idusuario": "'.$idusuario.'"
							}
						}';
				
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP12";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S05 - POST - DENUNCIA DEL USUARIO EN ALERTA 
	function setAlerta(){
			$request 		= Slim::getInstance()->request();
			$data 	= json_decode($request->getBody()); 
			$db 	= getConexion();
			
			$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
			
			if($comprobaciontoken == true){
				try{
					$sql	= "INSERT INTO alertas (latitud,longitud,asunto,descripcion,imagen,idusuario,idTipoalerta) VALUES (:latitud,:longitud,:asunto,:descripcion,:imagen,:idusuario,:idTipoalerta);";
					$stmt	= $db->prepare($sql);
					$zona = getZoneLomasAltamira($data->latitud,$data->longitud);  
					if(!$zona){
						echo '{"response":{
									"errorCode": 6,
									"errorMessage":"Fuera del area permitida",
									"msg":""
								}}';
					}else{
						if(($data->asunto) == '' or ($data->descripcion) == '' or ($data->imagen) == ''){
							//echo('pasa en el if');
							$data->asunto = "N/A";
							$data->imagen = "N/A";
							$data->descripcion = "N/A";
							$stmt->bindParam("asunto", $data->asunto);
							$stmt->bindParam("descripcion",  $data->descripcion);
							$stmt->bindParam("imagen",  $data->imagen);
							
						}else{
							$stmt->bindParam("asunto", $data->asunto);
							$stmt->bindParam("descripcion", $data->descripcion);
							$stmt->bindParam("imagen", $data->imagen);
						}
							$stmt->bindParam("idusuario", $data->idusuario);
							$stmt->bindParam("idTipoalerta", $data->idTipoalerta);
							$stmt->bindParam('longitud',$data->longitud);
							$stmt->bindParam('latitud',$data->latitud);
							$stmt->execute();
							$idusuario = $data->idusuario;
					//}
					
						echo '{"response":{
									"errorCode": 0,
									"errorMessage":"Registro Almacenado",
									"idusuario": "'.$idusuario.'"
								}}';
							$db = null;	
					}		
				
				} catch (PDOException $e){
					$db = null;
					$e = $e->getMessage();
						echo '{"response":{
							"errorCode": 2,
							"errorMessage":"Error en ejecucion de servicio web",  
							"msg":""
						}}';
					}
			}else{
				echo '{"response":{
							"errorCode": 3,
							"errorMessage":"Token Incorrecto",
							"msg":""
						}}';
			}
	}
	
//S06 - GET - TIPO DE PROVEEDORES
	function getTipoproveedores($tokenSesion,$iduser){ 
		$path = 'http://altamira.buzzcoapp.com/IMG/';
		$sql    = "SELECT idProveedorcategoria, nombreCategoria, imagenCategoria, estado FROM proveedorCategoria WHERE idProveedorcategoria IN (SELECT idProveedorcategoria FROM proveedor WHERE estado = 1) ";
		$comprobaciontoken = getverificaciontoken($tokenSesion,$iduser);
		
		if($comprobaciontoken==true){
		
		try{
			$i = 0; 
			$db   = getConexion();
			$stmt = $db->query($sql);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
					while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$array[$i]['idProveedorcategoria']   = trim($data['idProveedorcategoria']);
						$array[$i]['nombreCategoria'] 		= trim($data['nombreCategoria']);
						$array[$i]['imagenCategoria'] 		= trim($path.$data['imagenCategoria']);
						$i++; 
					} 
			
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($array).'							
								}
							}';
			}else{
				echo 	'{"response": {
									"errorCode": 4,
									"errorMessage": "No hay Datos",
									"msg": ""						
								}
							}';
			}
		} catch(PDOException $e){
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP02";
			$dates			= date("Y-m-d H:i:s");
			$dispositivo	= "5";
			seterrorlogwebservices($tipoerror,$e,$dates,$iddispositivo);*/
			echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
						
		}
		
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S07 - PUT - MODIFICAR TOKEN PUSH
	function setModificarPush(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		if(!isset($data->tokenSesion) or !isset($data->idusuario) or !isset($data->tokenPush)){
			echo 	'{"response": {
									"errorCode": 6,
									"errorMessage": "Falta Datos",
									"msg": ""
								}
							}';
		} else{
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);	
			if($comprobaciontoken==true){	
				try {
					$db = getConexion();
					$sql   = "UPDATE usuario SET tokenPush=:tokenPush WHERE idusuario=:idusuario;";
					$stmt = $db->prepare($sql);
					$stmt->bindParam("idusuario", $data->idusuario);
					$stmt->bindParam("tokenPush", $data->tokenPush);
					$stmt->execute(); 
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": ""
								}
							}';
					$db = null;
				} catch (PDOException $e){
					$db = null;
					$e = $e->getMessage();
					/*$tipoerror 		= "WebServices SP27";
					$dates			= date("Y-m-d H:i:s");
					seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
					echo  	'{"response": {
									"errorCode": 2,
									"errorMessage": "Error al ejecutar el servicio web",
									"token": "0",
									"idusuario":"0"  
								}
							}'; 
				}
			}else{
				echo 	'{"response": {
							"errorCode": 3,
							"errorMessage": "Token Incorrecto",
							"msg": ""  
						}
					}';	
			}
		}
	}
	
	
//S08 - GET - PROVEEDORES
	function getProveedores($idProveedorcategoria,$iduser,$tokenSesion){ 
		$path = 'http://altamira.buzzcoapp.com/IMG/';
		$sql    = "SELECT p.idProveedor, p.nombre, p.descripcion, p.imagen, p.telefono, p.idProveedorcategoria, p.puntuacion FROM proveedor  AS p
					 WHERE p.idProveedorcategoria =:idProveedorcategoria AND p.estado = 1;";
		$varId = 0;
		
		$comprobaciontoken = getverificaciontoken($tokenSesion,$iduser);
		
		if($comprobaciontoken==true){
		
		try{
			$i = 0;		
			$db   = getConexion();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idProveedorcategoria",$idProveedorcategoria);
			$stmt->execute();
			
			if ( $stmt->rowCount() > 0 ) {
			
					while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$array[$i]['idProveedor']   				= trim($data['idProveedor']);
						$array[$i]['nombre'] 						= trim($data['nombre']);
						$array[$i]['descripcion'] 				= trim($data['descripcion']);
						$array[$i]['imagen'] 						= trim($path.$data['imagen']);
						$array[$i]['telefono'] 					= trim($data['telefono']);
						$array[$i]['idProveedorcategoria'] 	= trim($data['idProveedorcategoria']);
						$array[$i]['puntuacion'] 				= trim($data['puntuacion']);
						
						$varId = trim($data['idProveedor']);
						
						$sql2   = "SELECT idusuario FROM usuario_has_proveedores WHERE idusuario =:iduser AND idProveedor = '".$varId."';";
						$stmt1 = $db->prepare($sql2);
						$stmt1->bindParam("iduser",$iduser);
						$stmt1->execute();
						
							if( $stmt1->rowCount() > 0 ){
								while ($data  = $stmt1->fetch(PDO::FETCH_ASSOC)){
									$array[$i]['estadoLike'] = "1";
								}
							}else{
									$array[$i]['estadoLike'] = "0";
							}
						
						$i++; 
					}  
			
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($array).'							
								}
							}';
			}
		} catch(PDOException $e){
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP02";
			$dates			= date("Y-m-d H:i:s");
			$dispositivo	= "5";
			seterrorlogwebservices($tipoerror,$e,$dates,$iddispositivo);*/
			echo	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
						
		}
		
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S09 - PUT - ESTADO NOTIFICACION
	/*function setestadoNotificacion(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){	
			try {
				$db = getConexion();
				$sql   = "UPDATE usuario SET estadoNotificacion=:estadoNotificacion WHERE idusuario=:idusuario;";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("idusuario", $data->idusuario);
				$stmt->bindParam("estadoNotificacion", $data->estadoNotificacion);
				$stmt->execute();  
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Servicio ejecutado con exito",
								"msg": ""
							}
						}';
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}*/
	
	
//S10- POST - REPORTE DE ROBO
	function SetReporterobo(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		$vCorreo = getverificacioncorreo($data->email);
		
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		if($vCorreo){
			if($comprobaciontoken==true){
				try {
					$db = getConexion();
					$sql   = "INSERT INTO configuracion (email,idusuario) VALUES (:email,:idusuario);";
					$stmt = $db->prepare($sql);
					$stmt->bindParam("email", $data->email);
					$stmt->bindParam("idusuario", $data->idusuario);
					$stmt->execute();
					$idusuario = $data->idusuario;
					
				if(isset($data->email)){
					send_reporteRobo($data->email);
				}
				
				
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Registro Almacenado",
									"idusuario": "'.$idusuario.'"
								}
							}';
					
					$db = null;
				} catch (PDOException $e){
					$db = null;
					$e = $e->getMessage();
					/*$tipoerror 		= "WebServices SP12";
					$dates			= date("Y-m-d H:i:s");
					seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
					echo  	'{"response": {
									"errorCode": 2,
									"errorMessage": "Error al ejecutar el servicio web",
									"token": "0",
									"idusuario":"0"  
								}
							}'; 
				}
			}else{
				echo 	'{"response": {
							"errorCode": 3,
							"errorMessage": "Token Incorrecto",
							"msg": ""  
						}
					}';	
			}
			
		}else{
			echo 	'{"response": {
										"errorCode": 1,
										"errorMessage": "Correo No registrado",
										"token": "0",
										"idusuario":"0"
									}
								}';
		}
	}
	
//S11 - POST - LILE PROVEEDORES
	function setLikeproveedores(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		$db = getConexion();

		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){
			try {
				
				$sql   = "INSERT INTO usuario_has_proveedores (idusuario,idProveedor,estadoLike) VALUES (:idusuario,:idProveedor,:estadoLike);";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("idusuario", $data->idusuario);
				$stmt->bindParam("idProveedor", $data->idProveedor);
				$stmt->bindParam("estadoLike", $data->estadoLike);
				$stmt->execute();
				$idusuario = $data->idusuario;
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Registro Almacenado",
								"idusuario": "'.$idusuario.'"
							}
						}';
				
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP12";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S12 - DELETE - ELIMINAR LIKE PROVEEDOR
	function deleteLike($idusuario,$idProveedor,$estadoLike,$tokenSesion){
		$comprobaciontoken = getverificaciontoken($tokenSesion,$idusuario);
		if($comprobaciontoken==true){
			$sql   = "UPDATE usuario_has_proveedores SET estadoLike = 0 WHERE idusuario=:idusuario AND idProveedor=:idProveedor AND estadoLike=:estadoLike;";
			try {
				$db = getConexion();
				$stmt   = $db->prepare($sql);
				$stmt->bindParam("idusuario",$idusuario);
				$stmt->bindParam("idProveedor",$idProveedor);
				$stmt->bindParam("estadoLike",$estadoLike);
				//$stmt->bindParam("fecha",date('Y-m-d H:i:s'));
				$stmt->execute();
				$db = null;
				echo 	'{"response": {
								"errorCode": 3,
								"errorMessage": "Servicio ejecutado con exito",
								"msg": "" 
							}
						}';
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP15";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
							"errorCode": 3,
							"errorMessage": "Token Incorrecto",
							"estado": ""  
						}
					}';
		}
	}
	
	
//S13 - PUT - PUNTUACI�N PROVEEDOR
	function setPuntuacionproveedores(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
	
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){

			$verificacionuser = getUserProveedor($data->idusuario,$data->idProveedor);
			try {
			
			$sql0 = "SELECT puntuacion FROM proveedor WHERE idProveedor =:idProveedor;";
				$db   = getConexion();
				$stmt = $db->prepare($sql0);
				$stmt->bindParam("idProveedor",$data->idProveedor);
				$stmt->execute();
				if ( $stmt->rowCount() > 0 ) {
					while ($point  = $stmt->fetch(PDO::FETCH_ASSOC)){
							$puntuacion    	= trim($point['puntuacion']);
							
					}
				}
			
				if($verificacionuser==false){
					$sql  = "INSERT INTO usuario_has_proveedores (idusuario,idProveedor) VALUES (:idusuario,:idProveedor);";
						$db   = getConexion();
						$stmt = $db->prepare($sql);
						$stmt->bindParam("idusuario", $data->idusuario);
						$stmt->bindParam("idProveedor",$data->idProveedor);
						$stmt->execute(); 
						
						$puntuacion ++;
						
						$sql1   = "UPDATE proveedor SET puntuacion= '".$puntuacion."' WHERE idProveedor=:idProveedor;";
						$db   = getConexion();
						$stmt = $db->prepare($sql1);
						$stmt->bindParam("idProveedor", $data->idProveedor);
						$stmt->execute(); 
						
				}else{
				
					$sql2 = "DELETE FROM usuario_has_proveedores WHERE idusuario=:idusuario AND idProveedor=:idProveedor";
					$db   = getConexion();
					$stmt = $db->prepare($sql2);
					$stmt->bindParam("idusuario", $data->idusuario);
					$stmt->bindParam("idProveedor", $data->idProveedor);
					$stmt->execute(); 
					
					$puntuacion --;
					$sql3   = "UPDATE proveedor SET puntuacion= '".$puntuacion."' WHERE idProveedor=:idProveedor;";
					$db   = getConexion();
					$stmt = $db->prepare($sql3);
					$stmt->bindParam("idProveedor", $data->idProveedor);
					$stmt->execute(); 
						 
				}
				echo 	'{"response": {
										"errorCode": 0,
										"errorMessage": "Servicio ejecutado con exito",
										"msg": "'.$puntuacion  .'"
									}
								}';
						$db = null;
						
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP27";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
	
//S14 - PUT - MODIFICAR PASSWORD
	function setModificarPassword(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		if($comprobaciontoken==true){	
			try {
				$db = getConexion();
				$sql   = "UPDATE usuario SET password=:password, primerRegistro=0 WHERE idusuario=:idusuario;";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("idusuario", $data->idusuario);
				$stmt->bindParam("password", $data->password);
				$stmt->execute(); 
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Servicio ejecutado con exito",
								"msg": ""
							}
						}';
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP27";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								"token": "0",
								"idusuario":"0"  
							}
						}'; 
			}
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S15 - PUT - VERIFICACION CORREO V2
	function setVerificacionCorreov2(){ 
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		$vCorreo = getverificacioncorreo($data->correo);
		
		if($vCorreo){
			try {
				$db = getConexion();
				$sql   = "UPDATE usuario SET ping=:ping WHERE correo=:correo;";
				$stmt = $db->prepare($sql);
				$ping = sha1(time().$data->correo);
				$ping = substr($ping, 0, 7);
				$stmt->bindParam("correo", $data->correo);
				$stmt->bindParam("ping", $ping);
				$stmt->execute(); 
				
				if(isset($data->correo) && isset($ping)){
					send_ping($data->correo,$ping);
				}
				
				echo 	'{"response": {
								"errorCode": 0,
								"errorMessage": "Pin Enviado exitosamente",
								"msg": ""
							}
						}';
				
				$db = null;
				
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				/*$tipoerror 		= "WebServices SP27";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo  	'{"response": {
								"errorCode": 2,
								"errorMessage": "Error al ejecutar el servicio web",
								 "msg": ""
							}
						}'; 
			}
		}else {
				echo 	'{"response": {
									"errorCode": 1,
									"errorMessage": "Correo No registrado",
									"token": "0",
									"idusuario":"0"
								}
							}';	
			}
			$db = null;
	}

//S16 - PUT - CAMBIAR CONTRASE�A DESPUES QUE SE VERIFICA QUE EL CORREO EXISTE
	function setResetContrasenia(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		$getveficigetverificacionping =getverificacionping($data->ping);
		
		if($getveficigetverificacionping==true){
		try {
			$db = getConexion();
			$sql   =  "UPDATE usuario SET password=:password WHERE ping=:ping;";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("password", $data->password);
			$stmt->bindParam("ping", $data->ping);
			$stmt->execute();
			echo 	'{"response": {
							"errorCode": 0,
							"errorMessage": "Cambio Realizado",
							 "msg": ""
						}
					}';
			
			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP12";
			$dates			= date("Y-m-d H:i:s");
			seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
			echo  	'{"response": {
							"errorCode": 2,
							"errorMessage": "Error al ejecutar el servicio web",
							"token": "0",
							"idusuario":"0"  
						}
					}'; 
		}
		
		}else{
			echo  	'{"response": {
							"errorCode": 4,
							"errorMessage": "Ping no valido",
							"Ping":"0"
						}
					}'; 
		}
	}
	
//S17 - GET - MSJ NOTIFICACIONES
	function getNotificacionesPush($tokenSesion,$idusuario){ 
	
		$sql    = "SELECT idNotificacion, titulo, descripcion, datetimeEnvio, estadoPush, estado FROM notificacion  WHERE estadoPush =1 ORDER BY datetimeEnvio DESC LIMIT 10; ";
	
		$sql2="SELECT n.idNotificacion, un.id_notificacion, n.titulo, n.descripcion, n.datetimeEnvio, n.estadoPush, 
				un.id_usuario_web AS idusuario, n.estado FROM notificacion as n
				INNER JOIN notificacion_usuario_atencion as un ON n.idNotificacion = un.id_notificacion
				WHERE un.id_usuario_web =  ".$idusuario."
				UNION
				SELECT n.idNotificacion, nu.idnotificacion, n.titulo, n.descripcion, n.datetimeEnvio, n.estadoPush, 
				nu.idusuario AS idusuario, n.estado FROM notificacion as n
				INNER JOIN notificacion_has_usuario as nu ON n.idNotificacion = nu.idnotificacion
				WHERE nu.idusuario =  ".$idusuario."
				UNION
				SELECT n.idNotificacion, h.idNotificacion, n.titulo, n.descripcion, n.datetimeEnvio, n.estadoPush, 
				h.idUsuario  AS idusuario, n.estado FROM notificacion as n
				INNER JOIN usuario_has_notificacion as h ON n.idNotificacion = h.idNotificacion 
				WHERE h.idUsuario =  ".$idusuario." ORDER BY datetimeEnvio DESC LIMIT 10;";
					
		
		
		$comprobaciontoken = getverificaciontoken($tokenSesion,$idusuario);
		if($comprobaciontoken==true){

		try{
			$i = 0; 
			$db   = getConexion();
			$stmt = $db->query($sql);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
					while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$array[$i]['idNotificacion']  = trim($data['idNotificacion']);
						$array[$i]['titulo'] 			  = trim($data['titulo']);
						$array[$i]['descripcion'] 	  = trim($data['descripcion']);
						$format							  = trim($data['datetimeEnvio']);
						$fechaFormat					  = date('d-m-Y H:i', strtotime($format));	
						$array[$i]['datetimeEnvio'] = $fechaFormat;	
						$array[$i]['estadoPush'] = trim($data['estadoPush']);
						$i++;
					}
				$stmt = $db->query($sql2);
					$stmt->execute();
					if ( $stmt->rowCount() > 0 ) {
							while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
								$array[$i]['idNotificacion']  = trim($data['idNotificacion']);
								$array[$i]['titulo'] 			  = trim($data['titulo']);
								$array[$i]['descripcion'] 	  = trim($data['descripcion']);
								$format							  = trim($data['datetimeEnvio']);
								$fechaFormat					  = date('d-m-Y H:i', strtotime($format));	
								$array[$i]['datetimeEnvio'] = $fechaFormat;	
								$array[$i]['estadoPush'] = trim($data['estadoPush']);
								$array[$i]['idusuario'] = trim($data['idusuario']);
								$i++; 
							}
					}
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado con exito",
									"msg": '.json_encode($array).'							
								}
							}';
			}else{
				echo 	'{"response": {
									"errorCode": 4,
									"errorMessage": "No hay Datos",
									"msg": ""						
								}
							}';
			}
			
		} catch(PDOException $e){
			$e = $e->getMessage();
			echo($e);
			/*$tipoerror 		= "WebServices SP02";
			$dates			= date("Y-m-d H:i:s");
			$dispositivo	= "5";
			seterrorlogwebservices($tipoerror,$e,$dates,$iddispositivo);*/
			echo	'{"response": {
							"errorCode": 1,
							"errorMessage": "Error al ejecutar el servicio web",
							"msg": "0"  
						}
					}';
						
		}
		
		}else{
			echo 	'{"response": {
						"errorCode": 3,
						"errorMessage": "Token Incorrecto",
						"msg": ""  
					}
				}';	
		}
	}
	
//S18 - PUT - CAMBIAR ESTADO DE LA NOTIFICACION
	function setUpdateNotificacion(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){
		try {
			$db = getConexion();
			$sql   =  "UPDATE notificacion SET estado=0 WHERE idNotificacion=:idNotificacion;";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idNotificacion", $data->idNotificacion);
			$stmt->execute();
			echo 	'{"response": {
							"errorCode": 0,
							"errorMessage": "Cambio Realizado",
							 "msg": ""
						}
					}';
			
			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP12";
			$dates			= date("Y-m-d H:i:s");
			seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
			echo  	'{"response": {
							"errorCode": 1,
							"errorMessage": "Error al ejecutar el servicio web",
							"token": "0",
							"idusuario":"0"  
						}
					}'; 
		}
		
		}else{
			echo  	'{"response": {
							"errorCode": 3,
							"errorMessage": "Token invalido",
							"Ping":"0"
						}
					}'; 
		}
	}
	
//S19 - PUT - CERRAR SESI�N
	function setCerrarSesion(){
		$request  = Slim::getInstance()->request();
		$data = json_decode($request->getBody());
		
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		
		if($comprobaciontoken==true){
		try {
			$db = getConexion();
			$sql   =  "UPDATE usuario SET tokenPush=0 WHERE idusuario=:idusuario;";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idusuario", $data->idusuario);
			$stmt->execute();
			echo 	'{"response": {
							"errorCode": 0,
							"errorMessage": "Cambio Realizado",
							 "msg": ""
						}
					}';
			
			$db = null;
		} catch (PDOException $e){
			$db = null;
			$e = $e->getMessage();
			/*$tipoerror 		= "WebServices SP12";
			$dates			= date("Y-m-d H:i:s");
			seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
			echo  	'{"response": {
							"errorCode": 1,
							"errorMessage": "Error al ejecutar el servicio web",
							"token": "0",
							"idusuario":"0"  
						}
					}'; 
		}
		
		}else{
			echo  	'{"response": {
							"errorCode": 3,
							"errorMessage": "Token invalido",
							"Ping":"0"
						}
					}'; 
		}
	}
	
	function visitas(){
		$request  	= Slim::getInstance()->request();
		$data 		= json_decode($request->getBody());
		$comprobaciontoken = getverificaciontoken($data->tokenSesion,$data->idusuario);
		if($comprobaciontoken==true){
				
			try {
				$db = getConexion();
				
				$visits = $data->visits;
				
				foreach ($visits as $key) {
					$sql   =  "INSERT INTO visita (nombre,placa_vehiculo,fecha,hora,notas,dui,idusuario) 
								VALUES (:nombre,:placa_vehiculo,:fecha,:hora,:notas,:dui,:idusuario);";
					$stmt = $db->prepare($sql);
					$stmt->bindParam("nombre", $key->nombre);
					$stmt->bindParam("placa_vehiculo", $key->placa_vehiculo);
					$stmt->bindParam("fecha", $key->fecha);
					$stmt->bindParam("hora", $key->hora);
					$stmt->bindParam("notas", $key->notas);
					$stmt->bindParam("dui", $key->dui);
					$stmt->bindParam("idusuario",$data->idusuario);
					$stmt->execute();
				}
					
					echo 	'{"response": {
									"errorCode": 0,
									"errorMessage": "Servicio ejecutado correctamente",
									 "msg": ""
								}
							}';
				
				$db = null;
			} catch (PDOException $e){
				$db = null;
				$e = $e->getMessage();
				echo $e;
				/*$tipoerror 		= "WebServices SP12";
				$dates			= date("Y-m-d H:i:s");
				seterrorlogwebservices($tipoerror,$e,$dates,$register->iddispositivo);*/
				echo '{"response":{
						"errorCode": 3,
						"errorMessage":"Error en ejecucion de servicio web",
						"msg":""
					}}';
			}
			
		}else{
				echo  	'{"response": {
							"errorCode": 7,
							"errorMessage": "Token de sesion no valido.",
							"msg":""
						}
					}'; 
			}
	}	
?>