<?php
try{
	if(isset($_GET['lat'])){
		$lat = $_GET['lat'];
	}
	if(isset($_GET['lon'])){
		$lon = $_GET['lon'];
	}
	if(isset($_GET['t'])){
		$t = $_GET['t'];
	}
	if(isset($_GET['e'])){
		$e = $_GET['e'];
	}
?>
<style>
	#mapcontainer {width: 700px; height: 420px;} 
	#map_canvas {width: 700px; height: 420px;}
</style>
<div id="mapcontainer" ><div id="map_canvas" style=""></div></div>
<script>	
$(document).ready(function() {
	var t = 0;
	setInterval(function(){ 
		if(t==0){
			initialize();
			t=1;
		} 
	}, 500);
});
function init(){
	initialize();
}
function initialize(){
	var map;
	map = new google.maps.Map(
		document.getElementById("map_canvas"), {
			zoom: 15,
			center: new google.maps.LatLng(<?=$lat?>,<?=$lon?>)
		}   
	);
	var position = new google.maps.LatLng(<?=$lat?>,<?=$lon?>);
	var icon = '';
	if(<?=$t?>==1){
		if(<?=$e?>==3){
			icon =  'images/mred/Alertas/1.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/Alertas/1_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/Alertas/1_3.png';
		}
	}else if(<?=$t?>==2){
		if(<?=$e?>==3){
			icon =  'images/mred/Alertas/2.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/Alertas/2_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/Alertas/2_3.png';
		}
	}else if(<?=$t?>==3){
		if(<?=$e?>==3){
			icon =  'images/mred/Alertas/3.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/Alertas/3_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/Alertas/3_3.png';
		}
	}else if(<?=$t?>==4){
		if(<?=$e?>==3){
			icon =  'images/mred/Alertas/4.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/Alertas/4_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/Alertas/4_3.png';
		}
	}
	map.setCenter(position);
	var marker = new google.maps.Marker({
		position: position,
		map: map,
		animation: google.maps.Animation.DROP,
		icon: icon
	});
}
</script>
<?php
}catch(Exception $e){
	echo 'Problema al imprimir el mapa';
}
?>