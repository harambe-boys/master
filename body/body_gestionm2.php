<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cMapaGestion.php";
	
	$oGestion = new MapaGestionM();
	$idE 	= $_SESSION['mred']['e'];
	$idTGM 	= $_SESSION['mred']['g'];
	$idM 	= $_SESSION['mred']['municipio'];
	$F1 	= $_SESSION['mred']['f1'];
	$F2 	= $_SESSION['mred']['f2'];
	if(isset($_GET['ta'])){
		$idTGM 	= $_GET['ta'];
		$_SESSION['mred']['a'] = $idTGM;
	}
	if(isset($_GET['te'])){
		$idE 	= $_GET['te'];
		$_SESSION['mred']['e'] = $idE;
	}
	if(isset($_GET['f1'])){
		$F1 = $_GET['f1'];
		$F1 = explode('-',$F1,3);
		$F1 = $F1[2].'-'. $F1[1].'-'.$F1[0]. ' '. '00:00:01';
		$_SESSION['f1'] = $F1;
	}
	if(isset($_GET['f2'])){
		$F2 = $_GET['f2'];
		$F2 = explode('-',$F2,3);
		$F2 = $F2[2].'-'. $F2[1].'-'.$F2[0]. ' '. '23:59:59';
		$_SESSION['mred']['f2'] = $F2;
	}
	$elementos = array();	
	$vGestion = $oGestion->getGM2($idM,$F1,$F2,$idE,$idTGM);
	if($vGestion){
		$i = 0;
		foreach($vGestion AS $id => $array){
			if($i==0){
				$elementos[$i]['orden'] 		= $o;
				$elementos[$i]['id'] 			= ($element['id']);
				$elementos[$i]['encabezado'] 	= ($element['encabezado']);
				$elementos[$i]['texto'] 		= ($element['texto']);
				$elementos[$i]['tipoalerta'] 	= ($element['tipoalerta']);
				$elementos[$i]['municipio']		= ($element['municipio']);
				$elementos[$i]['text']			= ($element['text']);
				$elementos[$i]['fecha']			= ($element['fecha']);
				$elementos[$i]['hora']			= ($element['hora']);
				$elementos[$i]['longitud']		= ($element['longitud']);
				$elementos[$i]['latitud']		= ($element['latitud']);
				$elementos[$i]['date']			= ($element['date']);
				$i++;
				$o++;
			}else{
				$ban = false;
				foreach($elementos AS $el2 => $element2){
					if(($element2['tipoalerta'] == $element['tipoalerta']) && distance($element['latitud'],$element['longitud'],$element2['latitud'],$element2['longitud'],'K')< 0.02){
						$ts1 = strtotime($element['date']);
						$ts2 = strtotime($element2['date']);
						$seconds_diff = $ts2 - $ts1. '';
						$result = secondsToTime($seconds_diff);
						if( $result['h']<1 && $result['m']<20){
							$ban = true;
						}
					}
				}
				if(!$ban){
					$elementos[$i]['orden'] 		= $o;
					$elementos[$i]['id'] 			= ($element['id']);
					$elementos[$i]['encabezado'] 	= ($element['encabezado']);
					$elementos[$i]['texto'] 		= ($element['texto']);
					$elementos[$i]['tipoalerta'] 	= ($element['tipoalerta']);
					$elementos[$i]['municipio']		= ($element['municipio']);
					$elementos[$i]['text']			= ($element['text']);
					$elementos[$i]['fecha']			= ($element['fecha']);
					$elementos[$i]['hora']			= ($element['hora']);
					$elementos[$i]['longitud']		= ($element['longitud']);
					$elementos[$i]['latitud']		= ($element['latitud']);
					$elementos[$i]['date']			= ($element['date']);
					$i++;
					$o++;
				}
			}
		}
	}
	
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}	

	function secondsToTime($seconds){
		// extract hours
		$hours = floor($seconds / (60 * 60));
	 
		// extract minutes
		$divisor_for_minutes = $seconds % (60 * 60);
		$minutes = floor($divisor_for_minutes / 60);
	 
		// extract the remaining seconds
		$divisor_for_seconds = $divisor_for_minutes % 60;
		$seconds = ceil($divisor_for_seconds);
	 
		// return the final array
		$obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		);
		return $obj;
	}
?>
<div class="load_page">
	<form class="tableName toolbar">
		<table class="display data_table2" id="data_table">
			<thead>
				<tr>
					<th>Tipo de Gestion</th>
					<th>Imagen</th>
					<th>Titulo</th>
					<th>Mensaje</th>
					<th>Fecha de Reporte</th>
					<th>Estado</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($vGestion){
					foreach ($vGestion AS $id => $array) {
				?>
					<tr>
						<td align="left"width='150px'><?=$array['t'];?></td>
						<td width='250px' ><?php if($array['img']){ ?><img src="data:image/gif;base64,<?=$array['img'];?>" width="50%" /><?php }else{ echo "N/A"; }?></td>						
						<td width='200px'><?php  if($array['ti']){ echo $array['ti']; }else{ echo 'N/A'; }?></td>
						<td width='200px'><?php  if($array['msg']){ echo $array['msg']; }else{ echo 'N/A'; }?></td>
						<td ><?=$array['time'];?></td>
						<td ><?php if($array['idE']==3){echo "Pendiente";}elseif($array['idE']==4){echo "Aprobada";}elseif($array['idE']==5){echo "Rechazada";}elseif($array['idE']==6){echo " En proceso";}elseif($array['idE']==7){echo "Terminada";}?></td>
						<td><span class="tip" >
								<!--<a id="opt=bUsuario&idUsuario=<?=$id?>" class="actiones" data-action="actionWebUser.php" data-tittle="Disable" data-msg="Want to Disable" name="" title="Disable" >
									<span class="fa fa-edit fa-2x edit"></span>
								</a>-->
								<?php
								if($array['idE']==3){
								?>
									<a id="opt=GV&i=<?=$id?>&idGM=<?=$array['idGM']?>" class="actiones" data-action="actionGestion.php" data-tittle="Aprobar" data-msg="Desea aprobar esta alerta" name="" title="Aprobar" >
										<span class="fa fa-check-square-o fa-2x ok"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-check-square-o fa-2x ok disableicon"></span>
								<?php
								}
								if($array['idE']==3){
								?>
									<a id="opt=GR&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Rechazar" data-msg="Desea rechazar esta alerta" name="" title="Rechazar" >
										<span class="fa fa-remove fa-2x reject"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-remove fa-2x reject disableicon"></span>
								<?php
								}
								if($array['idE']==4){
								?>
									<a id="opt=GP&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Proceso" data-msg="Desea poner en proceso esta alerta" name="" title="En proceso" >
										<span class="fa fa-clock-o fa-2x process"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-clock-o fa-2x process disableicon"></span>
								<?php
								}
								if($array['idE']==6){
								?>
									<a id="opt=GT&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Terminar" data-msg="Desea poner como terminada esta alerta" name="" title="Terminada" >
										<span class="fa fa-power-off fa-2x off"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-power-off fa-2x off disableicon"></span>
								<?php
								}
								?>
							</span> 
						</td>
					</tr>
				<?php
					}
				}	
				?>
			</tbody>
		</table>
	</form>
</div>	