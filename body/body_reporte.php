<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cReporte.php";
	
	$oReport  	= new Reporte();
	$idm 		= $_SESSION['mred']['municipio'];
	$tg 		= 0;
	$ta 		= 0;
	$e 			= 0;
	$d 			= 0;
	$f1 		= date('Y-m-d');
	$f2 		= date('Y-m-d');
	$table 		= 0;
	if(isset($_GET['r'])){
		if(isset($_GET['f1'])){
			$f1 = $_GET['f1'];
			$f1 = explode('-',$f1,3);
			$f1 = $f1[2].'-'. $f1[1].'-'.$f1[0];
		}
		if(isset($_GET['f2'])){
			$f2 = $_GET['f2'];
			$f2 = explode('-',$f2,3);
			$f2 = $f2[2].'-'. $f2[1].'-'.$f2[0];
		}
		if(isset($_GET['e'])){
			$e 	= $_GET['e'];
		}
		if(isset($_GET['d'])){
			$d 	= $_GET['d'];
		}
		
		if($_GET['r']==1){
			if(isset($_GET['ta'])){
				$ta 	= $_GET['ta'];
			}
			$vReport = $oReport->getReporteAlerta($ta,$e,$d,$f1,$f2,$idm);
			$_SESSION['mred']['objRA'] = $vReport;
			$table 	= 1;
		}elseif($_GET['r']==2){
			if(isset($_GET['tg'])){
				$tg 	= $_GET['tg'];
			}
			$vReport = $oReport->getReporteGestion($tg,$e,$d,$f1,$f2,$idm);
			$_SESSION['mred']['objRG'] = $vReport;
			$table 	= 2;
		}
	}else{
		$vReport = false;
		$table 	= 0;
	}
	
?>

<?php
if($table == 1){
?>
	<div class="load_page">
		<form class="tableName toolbar">
			<table class="display data_table2" id="data_table">
				<thead>
					<tr>
						<th>Tipo de Alerta</th>
						<th>Fecha reporte</th>
						<th>Identificador</th>
						<th>OS movil</th>
						<th>Estado</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($vReport){
						foreach ($vReport AS $id => $array) {
					?>
						<tr>
							<td ><?= $array['t'];?></td>
							<td ><?= $array['f'];?></td>
							<td ><?= $array['idp'];?></td>
							<td ><?php if($array['idd']==1){echo "<span class='fa fa-android fa-2x'></span>";}elseif($array['idd']==2){echo "<span class='fa fa-apple fa-2x'></span>";}?></td>
							<td ><?php if($array['ide']==3){echo "Pendiente";}elseif($array['ide']==4){echo "Aprobada";}elseif($array['ide']==5){echo "Rechazada";}?></td>
							<td>
								<span class="tip" >
									<a class="inline" href="body/map.php?lat=<?=$array['lat']?>&lon=<?=$array['lon']?>&t=<?=$array['ida']?>&e=<?=$array['ide']?>" title="Ver en mapa" >
										<span class="fa fa-map-marker fa-2x"></span>
									</a>
								</span> 
							</td>
						</tr>
					<?php
						}
					}	
					?>
				</tbody>
			</table>
		</form>
		<a href="reporte/reportepdfA.php" target="_blank"><input type="button" value="Exportar PDF" class="uibutton submit_form" name="PDF"/></a>
		<a href="reporte/reporteexcelA.php" target="_blank"><input type="button" value="Exportar excel" class="uibutton submit_form" name="excel"/></a>
	</div>	
<?php
}elseif($table == 2){
?>
	<div class="load_page">
		<form class="tableName toolbar">
			<table class="display data_table2" id="data_table">
				<thead>
					<tr>
						<th>Tipo de gestion</th>
						<th>Fecha reporte</th>
						<th>Identificador</th>
						<th>OS movil</th>
						<th>Estado</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($vReport){
						foreach ($vReport AS $id => $array) {
					?>
						<tr>
							<td ><?= $array['t'] ?></td>
							<td ><?= $array['f'];?></td>
							<td ><?= $array['idp'];?></td>
							<td ><?php if($array['idd']==1){echo "<span class='fa fa-android fa-2x'></span>";}elseif($array['idd']==2){echo "<span class='fa fa-apple fa-2x'></span>";}?></td>
							<td ><?php if($array['ide']==3){echo "Pendiente";}elseif($array['ide']==4){echo "Aprobada";}elseif($array['ide']==5){echo "Rechazada";}elseif($array['ide']==6){echo "En proceso";}elseif($array['ide']==7){echo "Terminada";}?></td>
							<td>
								<span class="tip" >
									<a  class="inline" href="body/map2.php?lat=<?=$array['lat']?>&lon=<?=$array['lon']?>&t=<?=$array['idg']?>&e=<?=$array['ide']?>"  title="Ver en mapa" >
										<span class="fa fa-map-marker fa-2x"></span>
									</a>
								</span> 
							</td>
						</tr>
					<?php
						}
					}	
					?>
				</tbody>
			</table>
		</form>
		<a href="reporte/reportepdfG.php"target="_blank"><input type="button" value="Exportar PDF" class="uibutton submit_form" name="PDF"/></a>
		<a href="reporte/reporteexcelG.php"target="_blank"><input type="button" value="Exportar excel" class="uibutton submit_form" name="excel"/></a>
	</div>
<?php
}
?>
<script>
	$("a.inline").fancybox({
		'hideOnContentClick': false, // so you can handle the map
		'overlayColor'      : '#000000',
		'overlayOpacity'    : 0.8,
		'autoDimensions': true // fixes inline bug
	});
</script>