<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cUsuarioApp.php";
	
	$oUApp  = new UsuarioApp();
	$idm 	= $_SESSION['mred']['municipio'];
	$idd 	= $_SESSION['mred']['idd'];
	$f1 	= $_SESSION['mred']['f1u'];
	$f2 	= $_SESSION['mred']['f2u'];
	if(isset($_GET['ta'])){
		$idd 	= $_GET['ta'];
		$_SESSION['mred']['idd'] = $idd;
	}
	if(isset($_GET['f1'])){
		$f1 = $_GET['f1'];
		$f1 = explode('-',$f1,3);
		$f1 = $f1[2].'-'. $f1[1].'-'.$f1[0];
		$_SESSION['mred']['f1u'] = $f1;
	}
	if(isset($_GET['f2'])){
		$f2 = $_GET['f2'];
		$f2 = explode('-',$f2,3);
		$f2 = $f2[2].'-'. $f2[1].'-'.$f2[0];
		$_SESSION['mred']['f2u'] = $f2;
	}
	
	$vUser = $oUApp->getUsuarioApp($idm,$idd,$f1,$f2);
	
?>
<div class="load_page">
	<form class="tableName toolbar">
		<table class="display data_table2" id="data_table">
			<thead>
				<tr>
					<th>Identificador</th>
					<th>Fecha de Registro</th>
					<th>OS movil</th>
					<th>N&ordm; denuncias</th>
					<th>Estado</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($vUser){
					foreach ($vUser AS $id => $array) {
				?>
					<tr>
						<td ><?= $id ?></td>
						<td ><?=$array['f'];?></td>
						<td ><?php if($array['idd']==1){echo "<span class='fa fa-android fa-2x'></span>";}elseif($array['idd']==2){echo "<span class='fa fa-apple fa-2x'></span>";}?></td>
						<td ><?=$array['cantidad'];?></td>
						<td ><?php if($array['ide']==1){echo "Activo";}elseif($array['ide']==2){echo "Bloqueado";}?></td>
						<td><span class="tip" >
								<?php
								if($array['ide']==2){
								?>
									<a id="opt=UH&i=<?=$id?>" class="actiones" data-action="actionUsuarioApp.php" data-tittle="Habilitar" data-msg="Desea habilitar este usuario" name="" title="Aprobar" >
										<span class="fa fa-check-square-o fa-2x ok"></span>
									</a>
								<?php
								}
								if($array['ide']==1){
								?>
									<a id="opt=UB&i=<?=$id?>" class="actiones" data-action="actionUsuarioApp.php" data-tittle="Bloquear" data-msg="Desea bloquear esta usuario" name="" title="Bloquear" >
										<span class="fa fa-remove fa-2x reject"></span>
									</a>
								<?php
								}
								?>
							</span> 
						</td>
					</tr>
				<?php
					}
				}	
				?>
			</tbody>
		</table>
	</form>
</div>	