<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cMapaGestion.php";
	
	$oGestion = new MapaGestionM();
	$idE 	= $_SESSION['mred']['e'];
	$idTGM 	= $_SESSION['mred']['g'];
	$idM 	= $_SESSION['mred']['municipio'];
	$F1 	= $_SESSION['mred']['f1'];
	$F2 	= $_SESSION['mred']['f2'];
	if(isset($_GET['ta'])){
		$idTGM 	= $_GET['ta'];
		$_SESSION['mred']['a'] = $idTGM;
	}
	if(isset($_GET['te'])){
		$idE 	= $_GET['te'];
		$_SESSION['mred']['e'] = $idE;
	}
	if(isset($_GET['f1'])){
		$F1 = $_GET['f1'];
		$F1 = explode('-',$F1,3);
		$F1 = $F1[2].'-'. $F1[1].'-'.$F1[0]. ' '. '00:00:00';
		$_SESSION['f1'] = $F1;
	}
	if(isset($_GET['f2'])){
		$F2 = $_GET['f2'];
		$F2 = explode('-',$F2,3);
		$F2 = $F2[2].'-'. $F2[1].'-'.$F2[0]. ' '. '23:59:59';
		$_SESSION['mred']['f2'] = $F2;
	}
		
	
	$vGestion = $oGestion->getTipoGM2($idE,$idTGM,$idM,$F1,$F2);
?>
<div class="load_page">
	<form class="tableName toolbar">
		<table class="display data_table2" id="data_table">
			<thead>
				<tr>
					<th>Tipo de Gestion</th>
					<th>Imagen</th>
					<th>Titulo</th>
					<th>Mensaje</th>
					<th>Fecha de Reporte</th>
					<th>Estado</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($vGestion){
					foreach ($vGestion AS $id => $array) {
				?>
					<tr>
						<td align="left"width='150px'><?=$array['t'];?></td>
						<td width='250px' ><?php if($array['img']){ ?><img src="data:image/gif;base64,<?=$array['img'];?>" width="50%" /><?php }else{ echo "N/A"; }?></td>						
						<td width='200px'><?php  if($array['ti']){ echo $array['ti']; }else{ echo 'N/A'; }?></td>
						<td width='200px'><?php  if($array['msg']){ echo $array['msg']; }else{ echo 'N/A'; }?></td>
						<td ><?=$array['time'];?></td>
						<td ><?php if($array['idE']==3){echo "Pendiente";}elseif($array['idE']==4){echo "Aprobada";}elseif($array['idE']==5){echo "Rechazada";}elseif($array['idE']==6){echo " En proceso";}elseif($array['idE']==7){echo "Terminada";}?></td>
						<td><span class="tip" >
								<!--<a id="opt=bUsuario&idUsuario=<?=$id?>" class="actiones" data-action="actionWebUser.php" data-tittle="Disable" data-msg="Want to Disable" name="" title="Disable" >
									<span class="fa fa-edit fa-2x edit"></span>
								</a>-->
								<a id="" class="actiones" href="detalle_gestionm_data_selected.php" data-tittle="Abrir" data-msg="Pasos para gestionar una alerta" name="" title="Abrir" >
									<span class="fa fa-external-link fa-2x"></span>
								</a>
								
								<?php
								if($array['idE']==3){
								?>
									<a id="opt=GV&i=<?=$id?>&idGM=<?=$array['idGM']?>" class="actiones" data-action="actionGestion.php" data-tittle="Aprobar" data-msg="Desea aprobar esta alerta" name="" title="Aprobar" >
										<span class="fa fa-check-square-o fa-2x ok"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-check-square-o fa-2x ok disableicon"></span>
								<?php
								}
								if($array['idE']==3){
								?>
									<a id="opt=GR&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Rechazar" data-msg="Desea rechazar esta alerta" name="" title="Rechazar" >
										<span class="fa fa-remove fa-2x reject"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-remove fa-2x reject disableicon"></span>
								<?php
								}
								if($array['idE']==4){
								?>
									<a id="opt=GP&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Proceso" data-msg="Desea poner en proceso esta alerta" name="" title="En proceso" >
										<span class="fa fa-clock-o fa-2x process"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-clock-o fa-2x process disableicon"></span>
								<?php
								}
								if($array['idE']==6){
								?>
									<a id="opt=GT&i=<?=$id?>" class="actiones" data-action="actionGestion.php" data-tittle="Terminar" data-msg="Desea poner como terminada esta alerta" name="" title="Terminada" >
										<span class="fa fa-power-off fa-2x off"></span>
									</a>
								<?php
								}else{
								?>
									<span class="fa fa-power-off fa-2x off disableicon"></span>
								<?php
								}
								?>
							</span> 
						</td>
					</tr>
				<?php
					}
				}	
				?>
			</tbody>
		</table>
	</form>
</div>	