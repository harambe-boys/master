<?php 
	session_start();
//Data
	include_once "../data/dataBase.php";
	include_once "../classes/cReporte.php";
	
	$oReport  	= new Reporte();
	$idm 		= $_SESSION['mred']['municipio'];
	$f1 		= date('Y-m-d');
	$f2 		= date('Y-m-d');
	
	if(isset($_GET['f1'])){
		$f1 = $_GET['f1'];
		$f1 = explode('-',$f1,3);
		$f1 = $f1[2].'-'. $f1[1].'-'.$f1[0];
	}
	if(isset($_GET['f2'])){
		$f2 = $_GET['f2'];
		$f2 = explode('-',$f2,3);
		$f2 = $f2[2].'-'. $f2[1].'-'.$f2[0];
	}
	
	$vReport = $oReport->getReporteLogGestion($f1,$f2,$idm);
	//$_SESSION['objRG'] = $vReport;
	
?>
	<div class="load_page">
		<form class="tableName toolbar">
			<table class="display data_table2" id="data_table">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>Tipo de gesti&oacute;n</th>
						<th>Identificador</th>
						<th>Cambio de estado</th>
						<th>Fecha de cambio</th>
						<th>Mapa</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($vReport){
						foreach ($vReport AS $id => $array) {
					?>
						<tr>
							<td ><?= $array['user'] ?></td>
							<td ><?= $array['g'] ?></td>
							<td ><?= $array['idp'];?></td>
							<td ><?= $array['e'] ?></td>
							<td ><?= date('d/m/Y H:i:s',strtotime($array['f']));?></td>
							<td>
								<span class="tip" >
									<a class="inline" href="body/map2.php?lat=<?=$array['lat']?>&lon=<?=$array['lng']?>&t=<?=$array['idg']?>&e=<?=$array['ide']?>" title="Ver en mapa" >
										<span class="fa fa-map-marker fa-2x"></span>
									</a>
								</span> 
							</td>
						</tr>
					<?php
						}
					}	
					?>
				</tbody>
			</table>
		</form>
	</div>	

<script>
	$("a.inline").fancybox({
		'hideOnContentClick': false, // so you can handle the map
		'overlayColor'      : '#000000',
		'overlayOpacity'    : 0.8,
		'autoDimensions': true // fixes inline bug
	});
</script>