<?php
try{
	if(isset($_GET['lat'])){
		$lat = $_GET['lat'];
	}
	if(isset($_GET['lon'])){
		$lon = $_GET['lon'];
	}
	if(isset($_GET['t'])){
		$t = $_GET['t'];
	}
	if(isset($_GET['e'])){
		$e = $_GET['e'];
	}
?>
<style>
	#mapcontainer {width: 700px; height: 420px;} 
	#map_canvas {width: 700px; height: 420px;}
</style>
<div id="mapcontainer" ><div id="map_canvas" style=""></div></div>
<script>	
$(document).ready(function() {
	var t = 0;
	setInterval(function(){ 
		if(t==0){
			initialize();
			t=1;
		} 
	}, 500);
});
function init(){
	initialize();
}
function initialize(){
	var map;
	map = new google.maps.Map(
		document.getElementById("map_canvas"), {
			zoom: 15,
			center: new google.maps.LatLng(<?=$lat?>,<?=$lon?>)
		}   
	);
	var position = new google.maps.LatLng(<?=$lat?>,<?=$lon?>);
	var icon = '';
	if(<?=$t?>==1){
	//basura
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/1.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/1_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/1_3.png';
		}
	}else if(<?=$t?>==2){
	//alumbrado
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/2.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/2_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/2_3.png';
		}
	}else if(<?=$t?>==3){
	//inundaciones
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/3.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/3_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/3_3.png';
		}
	}else if(<?=$t?>==4){
	//ornato
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/4.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/4_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/4_3.png';
		}
	}else if(<?=$t?>==5){
	//ruido
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/5.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/5_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/5_3.png';
		}
	}else if(<?=$t?>==6){
	//ventas
		if(<?=$e?>==7){
			icon =  'images/mred/GM_2/6.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/6_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/6_3.png';
		}
	}else if(<?=$t?>==7){
	//prostitucion
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/7.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/7_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/7_3.png';
		}
	}else if(<?=$t?>==8){
	//desorden
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/8.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/8_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/8_3.png';
		}
	}else if(<?=$t?>==9){
	//bacheo
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/9.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/9_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/9_3.png';
		}
	}else if(<?=$t?>==10){
	//fuga agua
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/10.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/10_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/10_3.png';
		}
	}else if(<?=$t?>==11){
	//emergencia
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/11.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/11_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/11_3.png';
		}
	}else if(<?=$t?>==12){
	//otros
		if(<?=$e?>==3){
			icon =  'images/mred/GM_2/12.png';
		}else if(<?=$e?>==4){
			icon =  'images/mred/GM_2/12_2.png';
		}else if(<?=$e?>==5){
			icon =  'images/mred/GM_2/12_3.png';
		}
	}
	map.setCenter(position);
	var marker = new google.maps.Marker({
		position: position,
		map: map,
		animation: google.maps.Animation.DROP,
		icon: icon
	});
}
</script>
<?php
}catch(Exception $e){
	echo 'Problema al imprimir el mapa';
}
?>