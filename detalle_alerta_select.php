<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cAlertasR.php";
	
	$oUsuario 		= new Usuario();
	$Object 		= new AlertasR();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	$nombreCurrent = $_SESSION['Altamira']['user'];
	$url = dirname($_SERVER["PHP_SELF"]); 
	
	$id				= '';
	$estado 		= '';
	$idTipoalerta 	= '';
	$tipoAlerta 	= '';
	$lat 			= '';
	$lon 			= '';
	$time 			= '';
	$nombre 		= '';
	$correo 		= '';
	$cluster 		= '';
	$poligono 		= '';
	$casa 			= '';
	$markerimg		= '';
	
	$titulo			= '';
	$descripcion	= '';
	$imagen			= '';
	
	if(isset($_REQUEST['id'])){
		$id 		= $_REQUEST['id'];
	}
	if(isset($_REQUEST['estado'])){
		$estado = $_REQUEST['estado'];
	}
	if(isset($_REQUEST['tipoAlerta'])){
		$tipoAlerta 	= $_REQUEST['tipoAlerta'];
	}
	if(isset($_REQUEST['idTipoalerta'])){
		$idTipoalerta = $_REQUEST['idTipoalerta'];
	}
	if(isset($_REQUEST['lat'])){
		$lat = $_REQUEST['lat'];
	}
	if(isset($_REQUEST['lon'])){
		$lon = $_REQUEST['lon'];
	}
	if(isset($_REQUEST['time'])){
		$time = $_REQUEST['time'];
	}
	if(isset($_REQUEST['nombre'])){
		$nombre = $_REQUEST['nombre'];
	}
	if(isset($_REQUEST['correo'])){
		$correo = $_REQUEST['correo'];
	}
	if(isset($_REQUEST['cluster'])){
		$cluster = $_REQUEST['cluster'];
	}
	if(isset($_REQUEST['poligono'])){
		$poligono = $_REQUEST['poligono'];
	}
	if(isset($_REQUEST['casa'])){
		$casa = $_REQUEST['casa'];
	}
	if($idTipoalerta==4){
		$markerimg = 'images/pickers/personas-a.png';
	}elseif($idTipoalerta==5){
		$markerimg = 'images/pickers/alumbrado-a.png';
	}elseif($idTipoalerta==6){
		$markerimg = 'images/pickers/fuga-a.png';
	}elseif($idTipoalerta==7){
		$markerimg = 'images/pickers/basura-a.png';
	}elseif($idTipoalerta==8){
		$markerimg = 'images/pickers/otros-a.png';
	}
	$vdetalle = $Object->getAlertasDetalle($id);
	if($vdetalle){
		$titulo			= $vdetalle['asunto'];
		$descripcion	= $vdetalle['descripcion'];
		$imagen			= $vdetalle['imagen'];
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Alertas espec&iacute;ficas</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
		<script type="text/javascript">
			$(document).ready(function() {
					initialize();
					$("#AlertasE").addClass("select");
			});
			function initialize() {
				var location = new google.maps.LatLng(<?=$lat?>, <?=$lon?>);
				map = new google.maps.Map(
					document.getElementById('map'), {
					center: location,
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				var marker = new google.maps.Marker({
					position: location,
					map: map,
					draggable:false,
					icon: '<?=$markerimg;?>'
				});
				$(".opt").live('click',function() { 
					  var title    = $(this).attr("data-tittle");
					  var msg    = $(this).attr("data-msg");
					  var opt    = $(this).attr("data-opt");
					  option(title,msg,opt);
				});
			}	
			function option(title,msg,opt){
				var mensaje = document.getElementById('m').value;
				if($("#checkbox").is(':checked')) {  
					var checkbox = 1;
				} else {  
					var checkbox = 0;
				}
				if( checkbox==1 && mensaje==''){
					showWarning('<font color=black>Ingrese el mensaje para validar la alerta</font>',3000);	
				}else{
					$.confirm({
						'title': title,
						'message': "<strong>&iquest;"+msg+"?</strong>",
						'buttons': {
							'Si': {
								'class': 'special',
								'action': function(){
									loading('Cargando',1);
										$.post("actions/actionAlertaE.php",{
											id: <?=$id?>,
											opt: opt,
											m: mensaje,
											checkbox:checkbox
										},function(data, status){
											if(status =='success'){
												procesandodata(data);
											}else{
												showError('No se pudo realizar la acci\u00F3n',3000);
												setTimeout(function(){unloading();},3000);
											}
										});						
										return false;	 
								}
							},
							'No': {
								'class'	: ''
								}
						}
					});
				}
			}
			function procesandodata(data){
				if(data == 'done1'){
					showSuccess('Alerta validada',3000);
					validargestion();
					setTimeout(function(){unloading();},3000);
				}else if(data == 'done2'){
					showSuccess('Alerta en proceso',3000);
					procesargestion();
					setTimeout(function(){unloading();},3000);
				}else if(data == 'done3'){
					showSuccess('Alerta terminada',3000);
					terminargestion();
					setTimeout(function(){unloading();},3000);
				}else if(data == 'done4'){
					showSuccess('Alerta rechazada',3000);
					rechazargestion();
					setTimeout(function(){unloading();},3000);
				}else{
					showError('Error intente de nuevo',3000);
					setTimeout(function(){unloading();},3000);
				}
			}
			function validargestion(){
				$('#validar').css('width','25%');
				$("#btnvalidar").addClass("disabled");
				$("#btnrechazar").addClass("disabled");
				$("#btnproceso").addClass("opt");
				$("#btnvalidar").removeClass("opt");
				$("#btnproceso").removeClass("disabled");
			}
			function procesargestion(){
				$('#precesar').css('width','25%');
				$("#btnproceso").addClass("disabled");
				$("#btnterminar").addClass("opt");
				$("#btnterminar").removeClass("disabled");
				$("#btnproceso").removeClass("opt");
			}
			function terminargestion(){
				$('#terminar').css('width','25%');
				$("#btnterminar").addClass("disabled");
				$("#btnproceso").addClass("disabled");
				$("#btnterminar").removeClass("opt");
			}
			function rechazargestion(){
				$('#rechazar').css('width','100%');
				$("#btnrechazar").addClass("disabled");
				$("#btnvalidar").addClass("disabled");
				$("#btnrechazar").removeClass("opt");
			}
		</script>
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><i class="fa fa-university fa-1x"></i> Alertas espec&iacute;ficas </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
							
						<div class="column_left">
							<h1>Alerta de <?=$tipoAlerta;?></h1>
							<label>Titulo:</label><p class="text-justify"><?=$titulo;?></p>
							<label>Descripcion:</label><p class="text-justify"><?=$descripcion;?></p>
							<label>Usuario:</label><p class="text-justify"><?=$nombre;?></p>
							<label>Correo:</label><p class="text-justify"><?=$correo;?></p>
							<label>Cl&uacute;ster:</label><p class="text-justify"><?=$cluster;?></p>
							<label>Poligono:</label><p class="text-justify"><?=$poligono;?></p>
							<label>Casa:</label><p class="text-justify"><?=$casa;?></p>
							<div style="height:100%; width:100%;">
								 <div id="map"></div> 
							</div>
						</div>
						
						<div class="column_right">
						<?php
							if($imagen!=''){
						?>
							<img  class="picHolder images" src="data:image/gif;base64,<?=$imagen;?>" width="50%" />
						<?php
							}
						?>
						</div>
						<div class="clear"></div>
						<div class="load_page">
							<div class="formEl_b">
								<form onsubmit="return false"> 
									<fieldset>
										<legend>Env&iacute;o de Notificaciones Push</legend>
										<div class="section last">
											<label> Notificaci&oacute;n <small>Ingresa el mensaje que quiera mandar a todas las personas que realizaron el reporte</small></label>   
											<div> 
												<input type="text"  name="m" id="m" maxlength="100" class="validate[required]  large"/>
												<label><small>M&aacute;ximo de 100 caracteres</small></label>
												<label>Quiere enviar notificaci&oacute;n al realizar la acci&oacute;n<input type="checkbox" name="checkbox" id="checkbox" /></label>
												
											</div>
										</div>
									</fieldset>
							</div>
						</div>
						<br><br>
						<h1>Acciones de <?=$tipoAlerta;?></h1>
						<br>
						<a id="btnvalidar" class="action color-black <?php if($estado!=1){ echo "disabled"; }else{ echo "opt"; }?>" data-tittle="Validar" data-msg="Desea validar esta alerta" data-opt="AV" >
							<div class="column_4 text-center divOk"> 
								<p class="text-title"><b>Aprobar</b><br>
									<span class="text-desc">Acci&oacute;n para aprobar gesti&oacute;n del usuario.<span>
								</p>
								<span class="fa fa-check-square-o fa-2x ok"></span>
							</div>
						</a>
						
						<a id="btnproceso" class="action  color-black <?php if($estado!=2){ echo "disabled"; }else{ echo "opt"; }?>"   data-tittle="En proceso" data-msg="Desea poner en proceso esta alerta" data-opt="AP" >
							<div class="column_4 text-center divProcess"> 
								<p class="text-title"><b>En Proceso</b><br>
									<span class="text-desc">Acci&oacute;n para activar proceso en ejecuci&oacute;n.<span>
								</p>
								<span class="fa fa-clock-o fa-2x process"></span>
							</div>
						</a>
						
						<a id="btnterminar"  class="action  color-black <?php if($estado!=4){ echo "disabled"; }else{ echo "opt";} ?>"  data-tittle="Terminar" data-msg="Desea terminar esta alerta" data-opt="AT" >
							<div class="column_4 text-center divOff"> 
								<p class="text-title"><b>Terminar</b><br>
									<span class="text-desc">Acci&oacute;n para indicar que la gesti&oacute;n ha finalizado.<span>
								</p>
								<span class="fa fa-power-off fa-2x off"></span>
							</div>
						</a>
						
						<a  id="btnrechazar" class="action color-black <?php if($estado!=1){ echo "disabled"; }else{ echo "opt";} ?>"  data-tittle="Rechazar" data-msg="Desea rechazar esta alerta" data-opt="AR" >
							<div class="column_4 text-center divCancel"> 
								<p class="text-title"><b>Rechazar</b><br>
									<span class="text-desc">Acci&oacute;n para rechazar la gesti&oacute;n recibida.<span>
								</p>
								<span class="fa fa-remove fa-2x reject"></span>
							</div>
						</a>
						
						<div class="clear"></div>
						<br>
						<div class="progress">
							<?php
								$v1 = 25;
								if($estado==1 || $estado==3){
									$v1 = 0;
								}
							?>
							<div id="validar" class="progress-bar progress-bar-success" style="width: <?=$v1?>%">
								<span class="sr-only">35% Complete (success)</span>
							</div>
							<?php
								$v1 = 25;
								if($estado==1 || $estado==3){
									$v1 = 0;
								}
							?>
							<div id="precesar" class="progress-bar progress-bar-process progress-bar-striped" style="width: <?=$v1?>%">
								<span class="sr-only">20% Complete (warning)</span> 
							</div>
							<?php
								$v1 = 25;
								if($estado==1 || $estado==3 || $estado==2|| $estado==4){
									$v1 = 0;
								}
							?>
							<div id="terminar" class="progress-bar progress-bar-danger" style="width: <?=$v1?>%">
								<span class="sr-only">10% Complete (danger)</span>
							</div>
							<?php
								$v1 = 0;
								if($estado==2){
									$v1 = 100;
								}
							?>
							<div id="rechazar" class="progress-bar progress-bar-warning progress-bar-striped" style="width: <?=$v1?>%">
								<span class="sr-only">35% Complete (success)</span>
							</div>
						</div>
						
						<div class="clear"></div>
					</div> 
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
	
</html>