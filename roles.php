<?php
	//Data
	include_once "data/dataBase.php";
	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cRoles.php";
	
	$oUsuario 		= new Usuario();
	$oRol 	= new Rol();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	if (!$_SESSION['Altamira']['permisos'][8]) {
		header("Location: index.php");
		exit();
	}
	
	$url = dirname($_SERVER["PHP_SELF"]); 
	$idR	= "";
	$nombre 	= "";
	$vPermisosP = "";
	$estado 	= "";
	$option 	= "";
	
	if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "mRol"){
		$option = $_REQUEST['opt'];
		$idR	= $_REQUEST['idRol'];
	}

	if ($option == "mRol") {
		$vRol = $oRol->getOne($idR);
		if($vRol){
			foreach ($vRol AS $id => $info){  
				$idRol 			= $id;
				$nombre		= $info["nombre"];
				$estado		= $info["estado"];
			}
			$vPermisosP = $oRol->getRolPermisos($idRol);
		}else{
			header("Location: roles.php");
			exit();
		}
	}
  $vRoles 		= $oRol->getAll();
  $vPermisos 	= $oRol->getAllPermisos();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Roles</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#Rol").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				<?php
					if($option == "mRol"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers",
					"aaSorting": []
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				if (!form.nombre.value) {
					msg = 'Ingrese nombre del Rol';
					is_error = true;
				}
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mRol") {
							echo 'msg = "El Rol ha sido Modificado.";';
						}else{
							echo 'msg = "El nuevo Rol ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='roles.php';",2500);
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
		</script>		
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><span class="ico  fa fa-list-ul fa-2x"></span>Roles </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >
								<li id="hi1"><a href="#tab1"  id="3"  >  Lista de Roles</a></li>  
								<li id="hi2"><a href="#tab2"  id="2"  ><?php if ($option == "mRol") { echo "Modificar"; }else{ echo "Crear"; }?> Roles</a></li>   
							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Nombre</th>
														<th>Fecha de Creaci&oacute;n</th>
														<th>Estado</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vRoles){
														foreach ($vRoles AS $id => $arrRol) {
													?>
														<tr>
															<td><?=$arrRol['nombre'];?></td>		
															<td ><?=$arrRol['fechaCreacion'];?></td>
															<td ><?php if($arrRol['estado']==1){echo "activo";}else{echo "Inactivo";}?></td>
															<td><span class="tip" >
																	<a href="roles.php?opt=mRol&idRol=<?=$id;?>" title="Modificar" >
																		<img src="images/icon/icon_edit.png" > 
																	</a>
																	<?php 
																		if($arrRol['estado']==1){
																	?>
																			<a id="opt=bRol&idRol=<?=$id?>" class="actiones2"  data-action="actionRoles.php" data-location="roles.php" data-tittle="Bloquear" data-msg="&iquest;Quiere bloquear al Rol?" name="<?=$arrRol['nombre']?>" title="Bloquear">
																				<img src="images/icon/color_18/delete.png" >
																			</a>
																	<?php
																		}elseif($arrRol['estado']==0){
																	?>
																			<a id="opt=hRol&idRol=<?=$id?>" class="actiones2"  data-action="actionRoles.php" data-location="roles.php" data-tittle="Habilitar" data-msg="&iquest;Quiere habilitar al Rol?" name="<?=$arrRol['nombre']?>" title="Habilitar">
																				<img src="images/icon/color_18/checkmark2.png" >
																			</a> 
																	<?php
																	}
																	?>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
								<div id="tab2" class="tab_content"> 
									<div class="load_page">
										<div class="formEl_b" id="msg">	
											<form id="form" action="actions/actionRoles.php" method="post" name="form"> 
												<?php
													if ($option == "mRol") {
														echo '<input type="hidden" name="idRol" value="'.$idR.' "/>';
														echo '<input type="hidden" name="opt" value="mRol" />';
													}else{
														echo '<input type="hidden" name="opt" value="nRol" />';
													}
												?>
												<fieldset >
													<legend>Por favor introducir toda la informaci&oacute;n.</span></legend>
													
														
													<div class="section ">
														<label> Nombre de Rol</label>   
														<div> 
															<input type="text" class="validate[required] large" name="nombre" id="nombre" value="<?=$nombre;?>" />
														</div>
													</div>
													<div class="section">
														<label> Permisos</label>   
														<div> 
															<table cellspacing="0" cellpadding="0" width="100%" rules="none" border="1" style="border: white;">
																<?php
																$itd 	= 0;
																$itr	= 0;
																if($vPermisos){
																	foreach($vPermisos AS $id => $array){
																		$selected="";
																		if($vPermisosP){
																			foreach($vPermisosP as $idPP => $arr){
																				if ($arr['idpermiso'] == $id){ $selected = "checked='checked'";}
																			}
																		}
																		if($itr==0){
																?>
																			<tr align="left" valign="top">
																		<?php
																		}
																		?>
																				<td><input class="chkbo" type="checkbox" name="permisos[]" id="chk<?php echo $id; ?>" value="<?php echo $id; ?>" <?=$selected?> /><?php echo $array['nombre']; ?></td>
																		<?php
																			$itr=1;
																			$itd++;
																		if($itd==4){
																			$itr=0;
																			$itd=0;
																		?>
																			</tr>
																		<?php
																		}
																		
																	}
																}
																?>
															</table>
														</div>
													</div>				
													<div class="section">
														<label> Estado</label>   
														<div> 
															<select name="estado" id="estado" class="chzn-select">
																<option value="0" <?php if($estado==0){echo"selected='selected'";}?>>Inactivo</option>
																<option value="1" <?php if($estado==1){echo"selected='selected'";}?>>Activo</option>
															</select>
														</div>
													</div>											
													<div class="section last">
														<div>
														<?php
															if ($option == "mRol") {
																echo '<input type="submit" value="MODIFICAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}else{
																echo '<input type="submit" value="GUARDAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}
														?>    
														<a class="btn btn-danger" style="padding: 2px 12px 2px 12px; border-radius: 0px;border: 1px solid #000000;" href="http://52.23.92.102/Altamira/roles.php" role="button">Cancelar</a>
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>	
								</div><!--tab2-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>