<?php
//Data
	include_once "data/dataBase.php";
	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cAtencionU.php";
	
	$oUsuario 		= new Usuario();
	$oAtencion 	= new Atencion();

	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Lomas de Altamira</title>
        <?php
			include_once "cssyjscomun.php";
		?>
        <script type="text/javascript">	
			$(document).ready(function() {
				initialize();
				ta = $('#talerta').val();
				ea = $('#ealerta').val();
				setInterval(function(){					
					setMarkerInterval(ta,ea,2);		
					animatemarker();
				},10000);			
			});

			function play(){
				document.getElementById("mp3").play();
			}
		</script>	   
    </head>        
    <!--<body class="dashborad" onload="drop()">        -->
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

		<div id="content">		
			<div class="inner">
				<div class="content-filter" id="content-filter">
						<div class="section-filter">
							<label> Tipo de alertas</label>   
							<div> 
								<select name="talerta" id="talerta" >
									<option value="0"  >Todos</option>
									<option value="1"  >Panico</option>
									<option value="2"  >Desordenes</option>
									<option value="4"  >Personas Sospechosas</option>
								</select>
							</div>
						</div>
						
						<div class="section-filter">
							<label> Estado de alertas</label>   
							<div> 
								<select name="ealerta" id="ealerta" >
									<option value="0"  >Todos</option>
									<option value="1"  >Pendientes</option>
									<option value="2"  >Aprobados</option>
									<option value="3"  >Rechazados</option>
								</select>
							</div>
						</div>
						
						<div class="section-filter last">
							<div> 
								<a href="#" class="uibutton submit_form" name="filtro" id="filtro"><span class="fa fa-filter"></span> Filtrar</a>
							</div>
						</div> 
						<div class="section-filter last">
							<div> 
								<span id='msg_box_nd' style="color: red"></span>  
							</div>
						</div>
					<div class="content-button-filter">
						<div class="button-filter">
							<a href="#" id="filtro-link"><span class="fa fa-filter fa-1x"></span> Filtros</a>
						</div>
					</div>
				</div>
				<div id="mapa" class="">				
				</div>
				<div class="clear"></div>

				<?php
					include_once "footer.php";
				?>
			</div> <!--// End inner -->
		</div> <!--// End content --> 
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
		<script type="text/javascript" src="js/map_alert.js"></script>
		<audio src = "sonido/alerta03.mp3" id = "mp3" precarga = "auto"> </audio>
		
	</body>
</html>