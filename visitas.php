<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	
	$oUsuario 		= new Usuario();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	if (!$_SESSION['Altamira']['permisos'][5]) {
		header("Location: index.php");
		exit();
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Visitantes</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#Visitas").addClass("select");

				tabla = $('#data_table').dataTable({
					sPaginationType:"full_numbers",
					bFilter: false
				});
				$('#fecIniciosearch').datepicker({
					dateFormat: 'dd/mm/yy',
					maxDate: 0,
					onClose: function( selectedDate ) {
						$( "#fecFinsearch" ).datepicker( "option", "minDate", selectedDate );
					}		
				});
				$('#fecFinsearch').datepicker({
					dateFormat: 'dd/mm/yy',
					maxDate: 0,
					onClose: function( selectedDate ) {
						$( "#fecIniciosearch" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
			function getelement(){
				var error 			= false;
				var estado 			= document.getElementById("estado").value;
				var fecIniciosearch = document.getElementById("fecIniciosearch").value;
				var fecFinsearch 	= document.getElementById("fecFinsearch").value;
				if(fecIniciosearch==''){
					msg = "Seleccione fecha inicial para el rango.";
					showWarning(msg,5000);
					error = true;
				}
				if(fecFinsearch==''){
					msg = "Seleccione fecha final para el rango.";
					showWarning(msg,5000);
					error = true;
				}
				
				if(!error){
					loading('Cargando',1);
					tabla.fnClearTable();
					$.ajax({
						url:'json/visitantes.php',
						type:'POST',
						data: { 
								estado: estado, 
								fecIniciosearch: fecIniciosearch, 
								fecFinsearch: fecFinsearch 
							}
					}).done(function( data ){
						var estado='';
						if(data=='ndata'){
							setTimeout('unloading();',3000);
							$('#reporte_excel').css('display','none');
						}else{
							data = JSON.parse(data);
							if(data != ''){
								for(var i in data){
									if(data[i].estado == 1){
										estado = 'Procesado';
									}
									else {
										estado = 'No Procesado';
									}
									tabla.fnAddData([
										 data[i].nombre_visitante,
										 data[i].placa_vehiculo,
										 data[i].fecha,
										 data[i].hora,
										 data[i].residente,
										 data[i].notas,
										 estado,
										 data[i].hora_confirmacion
									]);
								}
								setTimeout('unloading();',3000);
								$('#reporte_excel').css('display','block');
							}else{
								setTimeout('unloading();',3000);
								$('#reporte_excel').css('display','none');
							}
						}
					});
				}
			}
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >

					<div class="header"><span ><span class="ico fa fa-check fa-2x"></span> Registro de solicitudes de visitante  </span> </div>


					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >

								<li id="hi1"><a href="#tab1"  id="3"  >  Lista solicitudes de visitante</a></li>  

							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" >
									<form id="form2" onsubmit="return false;" >
										<div class="section">
											<label class="control-label">Estado:</label>
											<div  class="controls"> 
												<select name="estado" id="estado">
													<option value="1" selected="selected">Todas</option>
													<option value="2" >No procesadas</option>
													<option value="3" >Procesadas</option>
												</select>
											</div>
										</div>
										
										<div class="section" id="rango_f">
											<label class="control-label"> Rango de Fechas:</label>
											<div  class="controls"> 
												<small> Fecha de inicio:</small>
												<input type="text" readonly="readonly" name="fecIniciosearch" id="fecIniciosearch" value=""  />
												<small> Fecha de Finalizaci&oacute;n:</small>
												<input type="text" readonly="readonly" name="fecFinsearch" id="fecFinsearch" value=""  />
											</div>
										</div>
										<div class="section last">
											<div  class="controls"> 
												<input type="button" onclick="getelement();" value="Buscar" class="uibutton submit_form" id="boton_b"/>
											</div>
										</div>
									</form>
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th width="190">Nombre visitante</th>
														<th width="50">Placa</th>
														<th width="75">Fecha</th>
														<th width="45">Hora</th>
														<th width="190">Residente</th>
														<th width="300">Notas</th>
														<th width="50">Estado</th>														
														<th width="150">Hora de confirmaci&oacute;n</th>
														 
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</form>
										<br />
										<div id="reporte_excel" style="display:none">
											<a href="reporte/reporteVisitantes.php" target="_blank">
												<button type="button" data-style="expand-center" style="float: center;"class="btn btn-primary ladda-button"/>
													<span class="ladda-label">Exportar excel</span>
												</button>
											</a>
										</div>
										<br />
									</div>	
								</div>
								<!--tab1-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>