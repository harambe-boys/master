<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cUsersWeb.php";
	
	$oUsuario 		= new Usuario();
	$oUsuarioWeb 	= new UsersWeb();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	 if (!$_SESSION['Altamira']['permisos'][3]) {
		header("Location: index.php");
		exit();
	}
	
	$url = dirname($_SERVER["PHP_SELF"]); 
	
	$idUsuario		= "";
	$usuario		= "";
	$nombre		= "";
	$cAcceso		= "";
	$cluster			= "";
	//$poligono		= "";
	$casa			= "";
	$fechaC   		= "";
	$estado 		="";
	$idCluster 	="";
	$idPoligono 	="";
	$option    		= "";
	
	
		
	if (isset($_GET['opt'])) {
		$option    		= $_GET['opt'];
		$idUsuario 	= $_GET['idUsuario'];
	}
	
	if ($option == "mUsuarioWeb") {
		$vUsuario = $oUsuarioWeb->getUsuarioOne($idUsuario);
		foreach ($vUsuario AS $id => $info){  
			$idUsuario		= $id;
			$usuario		= $info["correo"];
			$nombre		= $info["nombre"];
			//$cAcceso		= $info["codigoAcceso"];
			$cluster			= $info["cluster"];
			//$poligono		= $info["poligono"];
			$casa			= $info["casa"];
			$fechaC		= $info["fechaCreacion"];
			$estado 		= $info["estado"];
			$threeC  = substr(sha1($fechaC.$usuario),0,7);
			
		}
	}
	$vUsuarios 	= $oUsuarioWeb->getUserAlldisabled();
	//$vCluster 		= $oUsuarioWeb->clusterAll();
	$vPoligono 	= $oUsuarioWeb->poligonoAll();
	
	//echo($option);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Usuarios</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#UserWeb").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				
				
				<?php
					if($option == "mUsuarioWeb"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
				<?php
					}else{
				?>
					document.getElementById("1").children[1].style.display = "none";

				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers"
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mUsuarioWeb") {
							echo 'msg = "Correo enviado.";';
						}else{
							echo 'msg = "El nuevo Usuario ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='usuariosWeb.php';",2500);
				}else if(responseText == 'pass') {
					msg = "La verificaci\u00F3n de la contrase\u00F1a no coinciden.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if (responseText == 'error1'){
					msg = "Necesita ingresar la contrase\u00F1a antigua.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 );
					document.getElementById("get").disabled = false;
				}else if (responseText == 'error2'){
					msg = "Contrase\u00F1as antigua incorrecta.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 ); 
					document.getElementById("get").disabled = false;
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
			
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >

					<div class="header"><span ><span class="ico fa fa-user-times fa-2x"></span> Verificaci&oacute;n de Usuarios </span> </div>


					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" id="1" >

								<li id="hi1"><a href="#tab1"  id="3"  >  Lista Usuarios</a></li>  
								<li id="hi2"><a href="#tab2"  id="2"  >Modificar Usuario </a></li>   

							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Correo</th>
														<th>Nombre</th>

														<th>Calle</th>

														<th>Casa</th>
														<th>Fecha de Creaci&oacute;n</th>														
														<th>Estado</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vUsuarios){
														foreach ($vUsuarios AS $id => $arrUsuario) {
													?>
														<tr>
															<td><?=$arrUsuario['correo'];?></td>		
															<td><?=$arrUsuario['nombre'];?></td>																	

															<td ><?=$arrUsuario['cluster'];?></td>

															<td ><?=$arrUsuario['casa'];?></td>
															<td ><?=$arrUsuario['fechaCreacion'];?></td>
															<td ><?php if($arrUsuario['estado']==1){echo "activo";}else{echo "Inactivo";}?></td>
															<td><span class="tip" >
																	<a href="usuariosWeb.php?opt=mUsuarioWeb&idUsuario=<?=$id;?>" title="Modificar" >
																		<img src="images/icon/icon_edit.png" > 
																	</a>
																	<?php 
																		if($arrUsuario['estado']==1){
																	?>
																			<a id="opt=bUsuario&idUsuario=<?=$id?>" class="actiones2" data-location="usuariosWeb.php" data-action="actionWebUser.php" data-tittle="Bloquear" data-msg="¿Quiere bloquear al usuario?" name="<?=$arrUsuario['correo']?>" title="Bloquear">
																				<img src="images/icon/color_18/delete.png" >
																			</a>
																	<?php
																		}elseif($arrUsuario['estado']==2){
																	?>
																			<a id="opt=hUsuario&idUsuario=<?=$id?>" class="actiones2" data-location="usuariosWeb.php" data-action="actionWebUser.php" data-tittle="Habilitar" data-msg="¿Quiere habilitar al usuario?" name="<?=$arrUsuario['correo']?>" title="Habilitar">
																				<img src="images/icon/color_18/checkmark2.png" >
																			</a>
																	<?php
																	}
																	?>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
								<div id="tab2" class="tab_content"> 
									<div class="load_page">
										<div class="formEl_b" id="msg">	
											<form id="form" novalidate action="actions/actionWebUser.php" method="post" name="form"> 
												<?php
													if ($option == "mUsuarioWeb") {
														echo '<input type="hidden" name="idUsuario" value="'.$idUsuario.' "/>';
														echo '<input type="hidden" name="opt" value="mUsuarioWeb" />';
													}
												?>
												<fieldset >
													<legend>Por favor introducir toda la informaci&oacute;n.</span></legend>
													
													<div class="section ">
														<label> Correo</label>   
														<div> 
															<input type="text" class="form-control validate[required] small" name="correo" id="correo" value="<?=$usuario;?>" readonly>
														</div>
													</div>
													<div class="section ">
														<label> Nombre</label>   
														<div> 
															<input type="text" class="validate[required] small" name="nombre" id="nombre" value="<?=$nombre;?>" readonly>
														</div>
													</div>
													

													<div class="section ">
														<label> Casa</label>   
														<div> 
															<input type="text" class="validate[required] small" name="casa" id="casa" value="<?=$casa;?>" readonly>
														</div>
													</div>
													<div class="section ">
														<label> Fecha de creaci&oacute;n</label>   
														<div> 
															<input type="text" class="validate[required] small" name="fechaC" id="fechaC" value="<?=$fechaC;?>" readonly>
														</div>
													</div>
													<?php
													if ($option == "mUsuarioWeb") {
													?>
													<div class="section ">
														<label> Nuevo Contrase&ntilde;a</label>   
														<div> 
															<input type="password"  class="validate[required,minSize[8]] small"  name="password" id="password" value="<?=$threeC;?>" readonly>
														</div>
													</div>
								  
													<div class="section ">
														  <label>Confirmar Contrase&ntilde;a</label>   
														  <div> 
														  <input type="password"  class="validate[required,minSize[8]] small"  name="cpassword" id="cpassword" value="<?=$threeC;?>"  readonly>
														  </div>
													</div>
													<?php  
													}
													?>
													<div class="section">
														<label> Estado</label>   
														<div> 
															<select name="estado" id="estado" class="chzn-select">
																<option value="0" <?php if($estado==0){echo"selected='selected'";}?>>Inactivo</option>
																<option value="1" <?php if($estado==1){echo"selected='selected'";}?>>Activo</option>
															</select>
														</div>
													</div>									
													<div class="section last">
														<div>
															<input type="submit" value="MODIFICAR" class="uibutton submit_form" name="guardar" id="get"/>
															                                
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>	
								</div><!--tab2-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>