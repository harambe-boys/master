<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cAtencionU.php";
	
	$oUsuario 	= new Usuario();
	$oAtencion 	= new Atencion();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	if(!isset($_REQUEST['id'])){
		header("Location: atencionUsuario.php");
		exit();
	}
	$vAtencion = $oAtencion->getOne($_REQUEST['id']);
	$vNotifica = $oAtencion->getNotificaciones($_REQUEST['id']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Detalle Atencion Usuario</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<style type="text/css">
		.divOk{
			background-color: bisque;
		}
		.divOk:hover{
			background-color: aliceblue;
		}
		</style>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#atencionU").addClass("select");
				$(".opt").live('click',function() { 
					  var title    = $(this).attr("data-tittle");
					  var msg    = $(this).attr("data-msg");
					  var opt    = $(this).attr("data-opt");
					  option(title,msg,opt);
				});
			});
			function option(title,msg,opt){
				var titulo = document.getElementById('ti').value;
				var mensaje = document.getElementById('m').value;
				var id = document.getElementById('id').value;
				var idd = document.getElementById('idd').value;
				var tk = document.getElementById('tk').value;
				var idu = document.getElementById('idu').value;
				if(titulo==''){
					showWarning('Ingrese el titulo para el envio de la notificacion push.',3000);
				}else if(mensaje==''){
					showWarning('Ingrese el mensaje para el envio de la notificacion push.',3000);
				}else{
					$.confirm({
						'title': title,
						'message': "<strong>&iquest;"+msg+"?</strong>",
						'buttons': {
							'SI': {
								'class': 'special',
								'action': function(){
									loading('Cargando',1);
										$.post("actions/actionAtencion.php",{
											opt: opt,
											ti: titulo,
											m: mensaje,
											id: id,
											idd: idd,
											tk: tk,
											idu:idu
										},function(data, status){
											if(status =='success'){
												procesandodata(data);
												
											}else{
												showError('No se pudo realizar la acci\u00F3n',3000);
												setTimeout(function(){unloading();},3000);
											}
										});						
										return false;	 
								}
							},
							'No': {
								'class'	: ''
								}
						}
					});
				}
			}
			function procesandodata(data){
				if(data == 'done'){
					showSuccess('Mensaje enviado correctamente',3000);
					setTimeout(function(){unloading();},3000);
				}else if(data == 'error2'){
					showSuccess('Tiene Desactivado las notificaciones',3000);
					setTimeout(function(){unloading();},3000);
				}else{
					showError('Error intente de nuevo',3000);
					setTimeout(function(){unloading();},3000);
				}
			}
		</script>
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><i class="fa fa-university fa-1x"></i> Atenci&oacute;n al Usuario </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
							
						<div class="column_left">
							<h3>Nombre de Usuario </h3>
							<h4 class="text-justify"><?=$vAtencion['nombre'];?></h4>
							<h3>Asunto</h3>
							<h4 class="text-justify"><?=$vAtencion['asunto'];?></h4>
							<h3>Descripci&oacute;n</h3>
							<h4 class="text-justify"><?=$vAtencion['descripcion'];?></h4>
							<h3>Fecha y Hora</h3>
							<h4 class="text-justify"><?=$vAtencion['fechaCreacion'];?></h4>
							<h3>Calle / Casa</h3>
							<h4 class="text-justify"><?=$vAtencion['cluster'];?> /  <?=$vAtencion['casa'];?></h4>
						</div>
						
						<div class="column_right">
							<?php if($vAtencion['descripcion'] !='' && $vAtencion['descripcion']!= null){?>
								<?php if($vAtencion['imagen'] !='' && $vAtencion['imagen']!= null){?>
								<img  class="picHolder images" src="data:image/gif;base64,<?=$vAtencion['imagen']?>" width="50%" />
								<?php } else {?>
								<h4 class="text-justify"><?php echo('No hay imagenes en esta atención');?></h4>
								<?php } ?>
							<?php } ?>
						</div>
						<div class="clear"></div>
						<div class="load_page">
							<div class="formEl_b">
								<form> 
									<input type="hidden" name="id" id="id" value="<?=$vAtencion['id']?>" />
									<input type="hidden" name="idd" id="idd" value="<?=$vAtencion['idDipositivo']?>" />
									<input type="hidden" name="tk" id="tk" value="<?=$vAtencion['tokenPush']?>" />
									<input type="hidden" name="idu" id="idu" value="<?=$vAtencion['idusuario']?>" />
									<fieldset>
										<legend>Env&iacute;o de Notificaciones Push</legend>
										<div class="section">
											<label>T&iacute;tulo<small>Ingresa el t&iacute;tulo de la notificaci&oacute;n</small></label>   
											<div> 
												<input type="text" name="ti" id="ti" class="validate[required]  large" value="" />
												<label><small>M&aacute;ximo de 150 caracteres</small></label>
											</div>
										</div>
										<div class="section">
											<label> Notificaci&oacute;n <small>Ingresa el mensaje que quiera mandar a todas las personas que realiz&oacute; la petici&oacute;n</small></label>   
											<div> 
												<textarea rows="2" cols="40" name="m" id="m" maxlength="300" class="validate[required]  large"></textarea>
												<label><small>M&aacute;ximo de 300 caracteres</small></label>
											</div>
										</div>
										<div class="section last">
											<div> 
												<a id="btnvalidar" class="action color-black opt" data-tittle="Enviar notificacion " data-msg="Quiere enviar la notificacion push al usuario" data-opt="ENPSH">
													<div class="column_4 text-center divOk"> 
														<p class="text-title"><b>Enviar notificaci&oacute;n</b><br>
														</p>
													</div>
												</a>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
						<div class="clear"></div>
						<?php
							if($vNotifica){
						?>
							<div class="load_page">
								<div class="formEl_b">
									<form> 
										<fieldset>
											<legend>Mensajes Enviados</legend>
											<?php
												foreach($vNotifica AS $id => $array){
											?>
											<div class="section">
												<label> Fecha  <?=$array['fecha'];?></label>   
												<div> 
													<p><?=$array['descripcion']?></p>
												</div>
											</div>
											<?php 
												}
											?>
										</fieldset>
									</form>
								</div>
							</div>
							<div class="clear"></div>	
						<?php 
							}
						?>
					</div> 
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
	
</html>