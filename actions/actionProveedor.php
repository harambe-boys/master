<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/cProveedor.php';

$oUsuario     		= new Usuario();
$oProveedor		= new Proveedor(); 
//$oLog  				= new Log();

session_start();

$idP =  $_SESSION['Altamira']['id'];

//echo($idUW);

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- PROVEEDORES ---- */
  
if ( $option == 'nProveedor' ) {
	try{
		//parametros 
		$nombre					= $_POST['nombre'];
		$descripcion			= $_POST['descripcion'];
		$imagen					= $_SESSION['Altamira']['imgProveedor'];
		$telefono					= $_POST['telefono'];
		$puntuacion			= $_POST['puntuacion'];
		$estado					= $_POST['estado'];
		$tipoP						= $_POST['tipoP'];
		
		//echo($imagen);
		
		copy("../IMGTEMP/".$_SESSION['Altamira']['imgProveedor']."", "../IMG/".$_SESSION['Altamira']['imgProveedor']."");
		unlink("../IMGTEMP/".$_SESSION['Altamira']['imgProveedor']);
			
		$params = array($nombre,$descripcion,$imagen,$telefono,$puntuacion,$estado,$tipoP);
		
			//var_dump($params);
			
			$save   = $oProveedor->nuevo($params);
				if ( $save ) {
					//$log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
					//$oLog->setLog($log);
					echo "done";
				} else {
					echo "error";
				}
	}catch (Exception $e){
		echo "error";
	}
}

//Modificar Usuario  
if ( $option == 'mProveedor' ) {
	try{
		//parametros 
		$idProveedor		= $_POST['idProveedor'];
		$nombre				= $_POST['nombre'];
		$descripcion		= $_POST['descripcion'];
		$imagen1     				= $_POST['oldimg'];
		if(isset($_SESSION['Altamira']['imgProveedor'])){
			$imagen				= $_SESSION['Altamira']['imgProveedor'];
		}else{
			$imagen = '';
		}
		$imagen				= $_SESSION['Altamira']['imgProveedor'];
		$telefono				= $_POST['telefono'];
		$puntuacion		= $_POST['puntuacion'];
		$estado				= $_POST['estado'];
		$tipoP					= $_POST['tipoP'];
		

		if($imagen != ''){
			copy("../IMGTEMP/".$_SESSION['Altamira']['imgProveedor']."", "../IMG/".$_SESSION['Altamira']['imgProveedor']."");
			unlink("../IMGTEMP/".$_SESSION['Altamira']['imgProveedor']);	
		}else{
			$imagen = $imagen1;
		}
			
			/*if (isset($_SESSION['lpgweb']['IMGS_hover']) && $_SESSION['lpgweb']['IMGS_hover']!="") {
				unlink("../IMG_LPG/".$hover);
				copy("../IMGTEMP/".$_SESSION['lpgweb']['IMGS_hover']."", "../IMG_LPG/".$_SESSION['lpgweb']['IMGS_hover']."");
				unlink("../IMGTEMP/".$_SESSION['lpgweb']['IMGS_hover']);
				$hover	= $_SESSION['lpgweb']['IMGS_hover'];
			}*/

			$params = array($nombre,$descripcion,$imagen,$telefono,$puntuacion,$estado,$tipoP,$idProveedor);
			$update   = $oProveedor->modificar($params);
			if ( $update ) {
				//$log = array($idUW,'Item','Modificación de item en el sistema. Item modificado: '.$nombre);
				//$oLog->setLog($log);
				echo "done";
			} else {
				echo "error";
			}
			
		
	}catch (Exception $e){
		echo "error";
	}
}

// Bloquear/Desbloquear
if($option=="bProveedor"){
	try{
		$id		= $_REQUEST['idProveedor'];
		$update   = $oProveedor->bloqueo($id);
		if ( $update ) {
				//$log = array($idUW,'Item','Bloqueo de item en el sistema. Id item modificado: '.$id);
				//$oLog->setLog($log);
				echo "done";
			
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="hProveedor"){
	try{
		$id		= $_REQUEST['idProveedor'];
		$update   = $oProveedor->habilitar($id);
		if ( $update ) {
			//$log = array($idUW,'UsuarioWeb','Habilitar item en el sistema. Id item modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

/* ---- /SUSCRIPCIONES ---- */

?>