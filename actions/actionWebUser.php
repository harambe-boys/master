<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/cUsersWeb.php';
//Clase PHPmailer
require '../classes/PHPmailer/PHPMailerAutoload.php';	

$oUsuario     		= new Usuario();
$oUsuarioWeb   	= new UsersWeb();


session_start();

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- USUARIO INACTIVOS ---- */
  
//Guarda Nuevo Usuario Web
if ( $option == 'nUsuario' ) {
	try{
		//parametros 
		$nombre	= $_POST['nombre'];
		$password 	= sha1($_POST['password']);
		$cpassword 	= sha1($_POST['cpassword']);
		
		if ($password==$cpassword) {
		
			$rol			= $_POST['idRol'];
			$estado 		= $_POST['estado'];
			
			$validateUser	= $oUsuario->validateUser($nombre);
			
			if ($validateUser==0) {
				$params = array($nombre,$password,$estado,$rol);
				$save   = $oUsuario->nuevo($params);
				if ( $save ) {
					echo "done";
				} else {
					echo "error";
				}
			}else{
				echo "exist";
			}
		}else{
			echo "pass";
		}
	}catch (Exception $e){
		echo "error";
	}
}

//Modificar Usuario Web
if ( $option == 'mUsuario' ) {
	try{
		//parametros 
		$idUsuario_web = $_POST['idUsuario_web'];
		$nombre		= $_POST['nombre'];
		$estado		= $_POST['estado'];
		$idRol		= $_POST['idRol'];
		
		if(isset($_POST['password']) && $_POST['password']<>""){
			$password 	= sha1($_POST['password']);
			
			$params = array($nombre,$password,$estado,$idRol,$idUsuario_web);
			$update   = $oUsuario->modificar($params);
			if ( $update ) {
				echo "done";
			} else {
				echo "error";
			}
		}else{
			$params = array($nombre,$estado,$idRol,$idUsuario_web);
			$update   = $oUsuario->modificar2($params);
			if ( $update ) {
				echo "done";
			} else {
				echo "error";
			}
		}
	}catch (Exception $e){
		echo $e->getMessage();
	}
}



//Modificar Usuario Web
if ( $option == 'mUsuarioWeb' ) {
	try{
		//parametros 
		$idUsuario		= $_POST['idUsuario'];
		$idCluster 	= 1;
		$idPoligono 	= $_REQUEST['idPoligono'];
		$estado 		= $_POST['estado'];
		$password1 	= $_REQUEST['password'];
		$correo 		= $_REQUEST['correo'];
		
		if(isset($_POST['password']) && $_POST['password']<>""){
			$password 	= sha1($_POST['password']);
			$params = array($password,$estado,$idUsuario);
			$update   = $oUsuarioWeb->modificarP($params);
			
			
			if(isset($idUsuario) && isset($idCluster) && isset($idPoligono)){
				$params2 = array($idUsuario, $idCluster, $idPoligono);
				$oUsuarioWeb->nuevodetalle($params2);
			}
			
			if(isset($correo) && isset($password1)){
					send_user_correo($correo,$password1);
				}
				
			if ( $update ) {
				echo "done";
			} else {
				echo "error";
			}
		}
	}catch (Exception $e){
		echo "error";
	}
}

// Bloquear/Desbloquear
if($option=="bUsuario"){
	try{
		$id		= $_REQUEST['idUsuario'];
		$update   = $oUsuarioWeb->bloqueo($id);
		
		//echo($id);
		if ( $update ) {
			echo "done"; 
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

if($option=="hUsuario"){
	try{
		$id		= $_REQUEST['idUsuario'];
		$update   = $oUsuarioWeb->habilitar($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

if($option=="bUsuarioweb"){
	try{
		$id		= $_REQUEST['idUsuario_web'];
		$update   = $oUsuario->bloqueo($id);
		if ( $update ) {
			echo "done"; 
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

if($option=="hUsuarioweb"){
	try{
		$id		= $_REQUEST['idUsuario_web'];
		$update   = $oUsuario->habilitar($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
/* ---- /USUARIO INACTIVOS ---- */

	function send_user_correo($correo,$password2){
	
		$mensaje = file_get_contents("../template/usuario_correo.php");
		$mensaje = str_replace("{{CORREO}}", utf8_decode($correo), $mensaje);
		$mensaje = str_replace("{{PASSWORD}}", utf8_decode($password2), $mensaje);
		$mensaje = str_replace("{{YEAR}}", date('Y'), $mensaje);
		
		$para = $correo;
		$asunto = 'Clave para acceder a la App Lomas de Altamira'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		//$mailPHP->SMTPDebug = 2; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		$mailPHP->SMTPSecure = "ssl";
		//indico el servidor de Gmail para SMTP
		$mailPHP->Host = "smtp.gmail.com";
		//indico el puerto que usa Gmail
		$mailPHP->Port = 465;
		//indico un usuario / clave de un usuario de gmail
		$mailPHP->Username = "rauleduardoguardado1991@gmail.com";
		$mailPHP->Password = 'vecchiasignora';
		$mailPHP->SetFrom('rauleduardoguardado1991@gmail.com', 'Lomas de Altamira'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$address = $para;
		$mailPHP->AddAddress($address, $correo);
		$mailPHP->Send();
	}



?>