<?php

include_once '../data/dataBase.php';
// Classes
include_once '../classes/cConfiguracion.php';

//Abriendo Sesiones
session_start();

// Params    
$oConfig    = new Configuracion();

if (isset($_REQUEST['opt'])) 
{
  $option = $_REQUEST['opt'];
}

/* ---- Alerta ---- */


if ( $option == 'nConfig' ) {
	try{
		$tma 	= $_REQUEST['tma'];
		$ta 	= $_REQUEST['ta'];
		$da 	= $_REQUEST['da'];
		$tmg 	= $_REQUEST['tmg'];
		$tg 	= $_REQUEST['tg'];
		$dg 	= $_REQUEST['dg'];
		$tmapp 	= $_REQUEST['tmapp'];
		
		$params = array($ta,$da,$tg,$dg,$tmg,$tma,$tmapp);
		$save   = $oConfig->setConfiguracion($params);
		if ( $save ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if ( $option == 'mConfig' ) {
	try{
		$id 	= $_REQUEST['id'];
		$tma 	= $_REQUEST['tma'];
		$ta 	= $_REQUEST['ta'];
		$da 	= $_REQUEST['da'];
		$tmg 	= $_REQUEST['tmg'];
		$tg 	= $_REQUEST['tg'];
		$dg 	= $_REQUEST['dg'];
		$tmapp 	= $_REQUEST['tmapp'];
		$params = array($ta,$da,$tg,$dg,$tma,$tmg,$tmapp,$id);
		$update = $oConfig->setModificarConfiguracion($params);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

?>