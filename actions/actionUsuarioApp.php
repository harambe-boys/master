<?php

include_once '../data/dataBase.php';
// Classes
require_once '../classes/cUsuarioApp.php';

//Abriendo Sesiones
session_start();

// Params    
$oUapp	= new UsuarioApp();

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt'];
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- Alerta ---- */


if ( $option == 'UH' ) {
	try{
		$id 	= $_GET['i'];
		$update   = $oUapp->habilitar($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if ( $option == 'UB' ) {
	try{
		$id 	= $_GET['i'];
		$update = $oUapp->bloquear($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo  "error";
	}
}
?>