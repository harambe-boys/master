<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cRoles.php';
//include_once '../classes/cLog.php';


$oRol  	= new Rol();
//$oLog  				= new Log();

$option = '';

session_start();

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}


if ( $option == "nRol") {
	try{
		$nombre		= $_POST['nombre'];
		$permisos 	= $_POST['permisos'];  
		$estado     	= $_POST['estado'];
	
			$params = array($nombre,$estado);
			$id   = $oRol->nuevo($params);
			if ( $id ) {
				foreach($permisos AS $idP){
					$params = array($id,$idP);
					$oRol->nuevoPermiso($params);
				}
				echo "done";
			} else {
				echo "error";
			}
	}catch (Exception $e){
		echo  $e;
	}
}
if ( $option == "mRol") {
	$id 				= $_POST['idRol'];
	$nombre		= $_POST['nombre'];
	$permiso	 	= $_POST['permisos'];  
	$estado     	= $_POST['estado'];
	 
	if(isset($permiso)){
		$permisos 	= $permiso; 
	}else{
		$permisos 	= array();  
	}
		try {
			$vPermisos	= $oRol->getRolPermisos($id);
			$arrayPA	= array();
			if($vPermisos){
				foreach($vPermisos AS $idP =>$array){
					$arrayPA[] = $array['idpermiso'];
					
				}
			}
			$arrayDif1 = array_diff($permisos,$arrayPA);
			$arrayDif2 = array_diff($arrayPA,$permisos);	
			
			if($arrayDif1){
				foreach($arrayDif1 AS $el){
					$params = array($id,$el);
					$oRol->nuevoPermiso($params);	
				}
			}
			if($arrayDif2){
				foreach($arrayDif2 AS $el){
					$params = array($el,$id);
					$oRol->eliminarPermiso($params);	
				}
			}
			$params = array($nombre,$estado, $id);
			$update   = $oRol->actualizar($params);
			
			if ( $update ) {
				echo "done";
			} else {
				echo "error";
			}
		}catch (Exception $e){
			echo  $e;
		}
	
}

if($option=="bRol"){
	try{
		$id		= $_REQUEST['idRol'];
		$update   = $oRol->bloqueo($id);
		if ( $update ) {
			//$log = array($idUW,'Rol','Bloqueo de Rol. ID Rol Modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="hRol"){
	try{
		$id		= $_REQUEST['idRol'];
		$update   = $oRol->habilitar($id);
		if ( $update ) {
			//$log = array($idUW,'Rol','Habilitar Rol. ID Rol Modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}



?>