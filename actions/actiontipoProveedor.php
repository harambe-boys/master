<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/ctipoProveedor.php';

$oUsuario    = new Usuario();
$oTipop 	 = new tipoProveedor();
//$oLog  				= new Log();

session_start();

$idTP =  $_SESSION['Altamira']['id'];

//echo($idUW);

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- TIPO DE PROVEEDORES ---- */
  
//Guarda Nuevo Usuario Web
if ( $option == 'nTipop' ) {
	try{
		//parametros 
		$nombre					= $_POST['nombre']; 
		$imagen					= $_SESSION['Altamira']['imgTipo'];
		$estado					= $_POST['estado'];
		
		//echo($imagen);
		//echo($nombre);
		//echo($estado);
		
		copy("../IMGTEMP/".$_SESSION['Altamira']['imgTipo']."", "../IMG/".$_SESSION['Altamira']['imgTipo']."");
		unlink("../IMGTEMP/".$_SESSION['Altamira']['imgTipo']);
			
		$params = array($nombre,$imagen,$estado);
			$save   = $oTipop->nuevo($params);
				if ( $save ) {
					//$log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
					//$oLog->setLog($log);
					echo "done";
				} else {
					echo "error";
				}
	}catch (Exception $e){
		echo "error";
	}
}

//Modificar Usuario  
if ( $option == 'mTipop' ) {
	try{
		//parametros 
		$idProveedorcategoria		= $_POST['idProveedorcategoria'];
		$nombre						= $_POST['nombre'];
		$imagen1     				= $_POST['oldimg'];
		if(isset($_SESSION['Altamira']['imgTipo'])){
			$imagen						= $_SESSION['Altamira']['imgTipo'];	
		}else{
			$imagen = '';
		}
		$estado						= $_POST['estado'];
		
		if($imagen != ''){
			copy("../IMGTEMP/".$_SESSION['Altamira']['imgTipo']."", "../IMG/".$_SESSION['Altamira']['imgTipo']."");
			unlink("../IMGTEMP/".$_SESSION['Altamira']['imgTipo']);	
		}else{
			$imagen = $imagen1;
		}	
			/*if (isset($_SESSION['lpgweb']['IMGS_hover']) && $_SESSION['lpgweb']['IMGS_hover']!="") {
				unlink("../IMG_LPG/".$hover);
				copy("../IMGTEMP/".$_SESSION['lpgweb']['IMGS_hover']."", "../IMG_LPG/".$_SESSION['lpgweb']['IMGS_hover']."");
				unlink("../IMGTEMP/".$_SESSION['lpgweb']['IMGS_hover']);
				$hover	= $_SESSION['lpgweb']['IMGS_hover'];
			}*/
			$params = array($nombre,$imagen,$estado,$idProveedorcategoria);
			$update   = $oTipop->modificar($params);
			if ( $update ) {
				//$log = array($idUW,'Item','Modificación de item en el sistema. Item modificado: '.$nombre);
				//$oLog->setLog($log);
				echo "done";
			} else {
				echo "error";
			}
			
	}catch (Exception $e){
		echo "error";
	}
}

// Bloquear/Desbloquear
if($option=="bTipoproveedor"){
	try{
		$id		= $_REQUEST['idProveedorcategoria'];
		$update   = $oTipop->bloqueo($id);
		if ( $update ) {
				//$log = array($idUW,'Item','Bloqueo de item en el sistema. Id item modificado: '.$id);
				//$oLog->setLog($log);
				echo "done";
			
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="hTipoproveedor"){
	try{
		$id		= $_REQUEST['idProveedorcategoria'];
		$update   = $oTipop->habilitar($id);
		if ( $update ) {
			//$log = array($idUW,'UsuarioWeb','Habilitar item en el sistema. Id item modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

/* ---- /SUSCRIPCIONES ---- */

?>