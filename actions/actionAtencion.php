<?php

include_once '../data/dataBase.php';
// Classes
require_once '../classes/cNotificaciones.php';
include_once "../classes/cAtencionU.php";
include_once "../classes/cLog.php";

//Abriendo Sesiones
session_start();

// Params    
$oPushService	= new PushService();
$oAtencion 		= new Atencion();
$oLog 		= new Log();

$idUW =  $_SESSION['Altamira']['id'];

if (isset($_REQUEST['opt'])) 
{
  $option = $_REQUEST['opt'];
}


/* ---- Alerta ---- */
if ( $option == 'ENPSH' ) {
	try{
		$ti 		= $_REQUEST['ti'];
		$m 		= $_REQUEST['m'];
		$id	 	= $_REQUEST['id'];
		$idu 	=  $_REQUEST['idu'];
		$idd 	= $_REQUEST['idd'];
	    $tk 		= $_REQUEST['tk'];
		$estado = 2;
		
		//echo $idu;
		$vPushAndroid 	= $oPushService->getAndroidPush1($idu);
		
		$vPushIOS 		= $oPushService->getIOSPush1($idu);
		
		//var_dump($vPushIOS );
		
		//var_dump($vPushAndroid);
		
		
		$params = array($ti,$m,$estado); 
		
		$idN 	= $oAtencion->nuevaNotificacion($params);
		
		
		$log = array('Envío de push de Atención de Usuario',$ti,$m,$idUW);
		$oLog->Log($log);
		
		
		$params1 = array($id,$idu,$idN);
		$save 	= $oAtencion->nuevoDetalle($params1);
			
		if($vPushAndroid || $vPushIOS ){
			if ( $save ) {
			
				if($idd==1){
					
					$oPushService->PushAndroid($tk,$m,$ti,$idd,$idu);	
					
				}elseif($idd==2){
					$oPushService->PushIOS( $tk,$ti,$m,$idd,$idu);
				}
			
				echo "done";
			} else {
				echo "error";
			}
			
		}else{
			echo('error2');
			
		}
	}catch(Exception $e){
		echo  $e;
		//echo($tk);
	}
}
?>