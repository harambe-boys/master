<?php

include_once '../data/dataBase.php';
// Classes
require_once '../classes/cMapaGestion.php';
include_once "../classes/cPushService.php";
include_once "../classes/cLog.php";

//Abriendo Sesiones
session_start();

// Params    
$oGestion	= new MapaGestionM();
$oPush 		= new PushService();
$oLog 		= new Log();

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt'];
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- Alerta ---- */


if ( $option == 'GV' ) {
	try{
		if (isset($_POST['i'])){
		  $id = $_POST['i'];
		}
		if (isset($_GET['i'])){
		  $id = $_GET['i'];
		}
		if(isset($_POST['text'])==true && $_POST['text'] !=""){
			$msg 	= 0;
			$AM		= $_POST['text'];	
		}else{
			if(isset($_POST['m'])){
				$msg 	= $_POST['m'];
			}else{
				if (isset($_POST['idGM'])){
				  $idGM = $_POST['idGM'];
				}
				if (isset($_GET['idGM'])){
				  $idGM = $_GET['idGM'];
				}
				$eMsg = $oGestion->getMensajesGM(4);
				if($eMsg){	
					$i=0;
					foreach($eMsg AS $d => $array){
						if($array['idG']==$idGM ){
							$data[$i] = $array['idG'];
							$i++;
						}
					}
					$k 	 = array_rand($data);
					$msg = $data[$k];
				}
			}
			$AM 	= $oGestion->getMensaje($msg);
		}
		
		
		$params = array($msg,$id);
		$update   = $oGestion->validar($params);
		if ( $update ) {
			$idM 	= $oGestion->getGMinfo($id);
			$vPush 	= $oPush->getPushGM($id);
			if($vPush){
				if($vPush['SOM']==1){
					$oPush->PushAndroidGestion($vPush['p'],$AM,3);	
				}elseif($vPush['SOM']==2){
					$oPush->PushIOSGestion($vPush['p'],$AM,3);
				}
			}
			$params = array($_SESSION['mred']['id'],$id,4);
			$oLog->setLogGestion($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GR' ) {
	try{
		$id 	= $_POST['i'];
		if (isset($_POST['i'])){
		  $id = $_POST['i'];
		}
		if (isset($_GET['i'])){
		  $id = $_GET['i'];
		}
		$update = $oGestion->rechazar($id);
		if ( $update ) {
			$params = array($_SESSION['mred']['id'],$id,5);
			$oLog->setLogGestion($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

if ( $option == 'GP' ) {
	try{
		$id 	= $_GET['i'];
		$update   = $oGestion->proceso($id);
		if ( $update ) {
			/*$idM 	= $oGestion->getGMinfo($id);
			$vPush 	= $oPush->getPushGM($id);
			if($vPush){
				if($vPush['SOM']==1){
					$oPush->PushAndroid($vPush['p'],$AM,'',false);
				}elseif($vPush['SOM']==2){
					$oPush->PushIOS($vPush['p'],$AM);
				}
			}*/
			$params = array($_SESSION['mred']['id'],$id,6);
			$oLog->setLogGestion($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if ( $option == 'GT' ) {
	try{
		$id 	= $_GET['i'];
		$update   = $oGestion->terminada($id);
		if ( $update ) {
			//$idM 	= $oGestion->getGMinfo($id);
			//$vPush 	= $oPush->getPushGM($id);
			/*if($vPush){
				if($vPush['SOM']==1){
					$oPush->PushAndroid($vPush['p'],$AM,'',false);
				}elseif($vPush['SOM']==2){
					$oPush->PushIOS($vPush['p'],$AM);
				}
			}*/
			$params = array($_SESSION['mred']['id'],$id,7);
			$oLog->setLogGestion($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo  "error";
	}
}

if ( $option == 'RG' ) {
	try{
		
		$id 	= $_POST['i'];
		$m 		= $_POST['m'];
		$params = array($m,$id);
		$update = $oGestion->reasignarGestion($params);
		if ( $update ) {
			$params = array($_SESSION['mred']['id'],$id,$_SESSION['mred']['municipio'],$m);
			$oGestion->setReasignacionGestion($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if ( $option == 'GVA' ) {
	try{
		$id = $_POST['data'];
		
		if(isset($_POST['text'])){
			if($_POST['text'] !=""){
				$m 	= 0;
				$AM	= $_POST['text'];	
			}else{
				$m 	= $_POST['m'];
				$AM = $oGestion->getMensaje($m);
			}
		}
		$v	= false;
		$arrayA = [];
		$arrayI = [];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion($e);
			if($accion){
				$params = array($m,$e);
				$oGestion->validar($params);
				$data = $oGestion->getPushInfo($e);
				if($data){
					foreach($data AS $i =>$info){
						if($info['dd']==1){
							$result = array_key_exists($info['p'],$arrayA);
							if(!$result){
								$arrayA[] = $info['p'];
							}
						}elseif($info['dd']==2){
							$result = array_key_exists($info['p'],$arrayI);
							if(!$result){
								$arrayI[] = $info['p'];
							}
						}
						
					}
				}
			}
		}
		$idM 			= $_SESSION['mred']['municipio'];
		if($arrayA){
			try{
				$cantidadU = count($arrayA);
				if($arrayA){
					$i = 0;
					$y = 0;
					$arraysend = array();
					foreach($arrayA AS $token){
						$i++;
						if($y<=500){
							array_push($arraysend, $token);
							$y++;
						}else{
							$oPush->PushAndroid($arraysend,$AM,3,'Gestion Municipal',false);
							$arraysend= array();
							$y = 1;
						}
						if($i==$cantidadU){
							$oPush->PushAndroid($arraysend,$AM,3,'Gestion Municipal',false);
							$arraysend= array();
						}
					}
				}
				
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		if($arrayI){
			try{
				$oPush->PushIOS($arrayI,$AM,3);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		echo 'done';
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GRA' ) {
	try{
		$id 	= $_POST['data'];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion($e);
			if($accion){
				$oGestion->rechazar($e);
			}
		}
		echo 'done';
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GVD' ) {
	try{
		$ids 	= $_REQUEST['ids'];
		$id 	= explode(',',$ids);
		if(isset($_REQUEST['m'])==true && $_REQUEST['m']!=''){
			$AM  	= $_REQUEST['m'];
			$k		= 0;
		}else{
			if (isset($_REQUEST['idGM'])){
			  $idGM = $_REQUEST['idGM'];
			}
			$eMsg = $oGestion->getMensajesGM(4);
			if($eMsg){	
				$i=0;
				foreach($eMsg AS $d => $array){
					if($array['idG']==$idGM ){
						$data[$i] = $array['idG'];
						$i++;
					}
				}
				$k 	 	= array_rand($data);
				$msg 	= $data[$k];
				$AM 	= $oGestion->getMensaje($msg);
				if(!$AM){
					$AM = "Tu reporte ha sido verificado y trabajaremos para resolver el problema.";
				}
			}else{
				$msg  = 0;
				$AM = "Tu reporte ha sido verificado y trabajaremos para resolver el problema.";
			}
		}
		
		$arrayA = [];
		$arrayI = [];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion($e);
			if($accion){
				$params = array($msg,$e);
				$oGestion->validar($params);
				$data = $oGestion->getPushInfo($e);
				if($data){
					foreach($data AS $i =>$info){
						if($info['dd']==1){
							$result = array_key_exists($info['p'],$arrayA);
							if(!$result){
								$arrayA[] = $info['p'];
							}
						}elseif($info['dd']==2){
							$result = array_key_exists($info['p'],$arrayI);
							if(!$result){
								$arrayI[] = $info['p'];
							}
						}
						
					}
				}
			}
		}
		if($arrayA){
			try{
				$oPush->PushAndroid($arrayA,$AM,3,'',false);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		if($arrayI){
			try{
				$oPush->PushIOS($arrayI,$AM,3);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		echo 'done1';
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GPD' ) {
	try{
		$ids 	= $_REQUEST['ids'];
		$id 	= explode(',',$ids);
		if(isset($_REQUEST['m'])==true && $_REQUEST['m']!=''){
			$AM 	= $_REQUEST['m'];
		}else{
			if (isset($_REQUEST['idGM'])){
			  $idGM = $_REQUEST['idGM'];
			}
			$eMsg = $oGestion->getMensajesGM(6);
			if($eMsg){	
				$i=0;
				foreach($eMsg AS $d => $array){
					if($array['idG']==$idGM ){
						$data[$i] = $array['idG'];
						$i++;
					}
				}
				$k 	 	= array_rand($data);
				$msg 	= $data[$k];
				$AM 	= $oGestion->getMensaje($msg);
			}else{
				$msg  = 0;
				$AM = "Estamos trabajando para resolver tu denuncia.";
			}
		}
		
		$arrayA = [];
		$arrayI = [];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion2($e);
			if($accion){
				$oGestion->proceso($e);
				$data = $oGestion->getPushInfo($e);
				if($data){
					foreach($data AS $i =>$info){
						if($info['dd']==1){
							$result = array_key_exists($info['p'],$arrayA);
							if(!$result){
								$arrayA[] = $info['p'];
							}
						}elseif($info['dd']==2){
							$result = array_key_exists($info['p'],$arrayI);
							if(!$result){
								$arrayI[] = $info['p'];
							}
						}
						
					}
				}
			}
		}
		if($arrayA){
			try{
				$oPush->PushAndroid($arrayA,$AM,3,'',false);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		if($arrayI){
			try{
				$oPush->PushIOS($arrayI,$AM,3);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		echo 'done2';
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GTD' ) {
	try{
		$ids 	= $_REQUEST['ids'];
		$id 	= explode(',',$ids);
		if(isset($_REQUEST['m'])==true && $_REQUEST['m']!=''){
			$AM 	= $_REQUEST['m'];
		}else{
			if (isset($_REQUEST['idGM'])){
			  $idGM = $_REQUEST['idGM'];
			}
			$eMsg = $oGestion->getMensajesGM(7);
			if($eMsg){	
				$i=0;
				foreach($eMsg AS $d => $array){
					if($array['idG']==$idGM ){
						$data[$i] = $array['idG'];
						$i++;
					}
				}
				$k 	 	= array_rand($data);
				$msg 	= $data[$k];
				$AM 	= $oGestion->getMensaje($msg);
			}else{
				$msg  = 0;
				$AM = "La denuncia ya fue solucionada, gracias por reportarla.";
			}
		}
		$arrayA = [];
		$arrayI = [];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion3($e);
			if($accion){
				$oGestion->terminada($e);
				$data = $oGestion->getPushInfo($e);
				if($data){
					foreach($data AS $i =>$info){
						if($info['dd']==1){
							$result = array_key_exists($info['p'],$arrayA);
							if(!$result){
								$arrayA[] = $info['p'];
							}
						}elseif($info['dd']==2){
							$result = array_key_exists($info['p'],$arrayI);
							if(!$result){
								$arrayI[] = $info['p'];
							}
						}
						
					}
				}
			}
		}
		if($arrayA){
			try{
				$oPush->PushAndroid($arrayA,$AM,3,'',false);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		if($arrayI){
			try{
				$oPush->PushIOS($arrayI,$AM,3);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		echo 'done3';
	}catch(Exception $e){
		echo  "error";
	}
}
if ( $option == 'GRD' ) {
	try{
		$ids 	= $_REQUEST['ids'];
		$id 	= explode(',',$ids);
		if(isset($_REQUEST['m'])==true && $_REQUEST['m']!=''){
			$AM 	= $_REQUEST['m'];
		}else{
			$AM 	= "El reporte fue rechazado, procure utilizar de forma correcta el servicio.";	
		}
		$arrayA = [];
		$arrayI = [];
		foreach($id AS $e){
			$accion = $oGestion->getEstadoGestion($e);
			if($accion){
				$oGestion->rechazar($e);
				$data = $oGestion->getPushInfo($e);
				if($data){
					foreach($data AS $i =>$info){
						if($info['dd']==1){
							$result = array_key_exists($info['p'],$arrayA);
							if(!$result){
								$arrayA[] = $info['p'];
							}
						}elseif($info['dd']==2){
							$result = array_key_exists($info['p'],$arrayI);
							if(!$result){
								$arrayI[] = $info['p'];
							}
						}
						
					}
				}
			}
		}
		if($arrayA){
			try{
				$oPush->PushAndroid($arrayA,$AM,3,'',false);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		if($arrayI){
			try{
				$oPush->PushIOS($arrayI,$AM,3);
			}catch(Exception $e){
				return $e->getMessage();
			}
		}
		echo 'done4';
	}catch(Exception $e){
		echo  "error";
	}
}
?>