<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/cUsuariosApp.php';


$oUsuario     	= new Usuario();
$oUsuarioApp   	= new UsuariosApp();


session_start();

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- USUARIO WEB ---- */
  

// Bloquear/Desbloquear
if($option=="bUsuario"){
	try{
		$id		= $_GET['idUsuario'];
		$update   = $oUsuarioApp->bloqueo($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		return "error";
	}
}
if($option=="hUsuario"){
	try{
		$id		= $_GET['idUsuario'];
		$update   = $oUsuarioApp->habilitar($id);
		if ( $update ) {
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		return "error";
	}
}


/* ---- /USUARIO WEB ---- */



?>