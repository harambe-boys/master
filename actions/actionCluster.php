<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/cCluster.php';

$oUsuario     = new Usuario();
$oCluster 	= new Cluster();
//$oLog  				= new Log();

session_start();

$idTP =  $_SESSION['Altamira']['id'];

//echo($idUW);

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- TIPO DE PROVEEDORES ---- */
  
//Guarda Nuevo Usuario Web
if ( $option == 'nCluster' ) {
	try{
		//parametros 
		$nombre					= $_POST['nombre'];
		$estado						= $_POST['estado'];
			
		$params = array($nombre,$estado);
			$save   = $oCluster->nuevo($params);
				if ( $save ) {
					//$log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
					//$oLog->setLog($log);
					echo "done";
				} else {
					echo "error";
				}
	}catch (Exception $e){
		echo "error";
	}
}

//Modificar Usuario  
if ( $option == 'mCalles' ) {
	try{
		//parametros 
		$idCluster				= $_POST['idCalles'];
		$nombre				= $_POST['nombre'];
		$estado					= $_POST['estado'];
		
			$params = array($nombre,$estado,$idCluster);
			$update   = $oCluster->modificar($params);
			if ( $update ) {
				//$log = array($idUW,'Item','Modificación de item en el sistema. Item modificado: '.$nombre);
				//$oLog->setLog($log);
				echo "done";
			} else {
				echo "error";
			}
			
		
	}catch (Exception $e){
		echo "error";
	}
}

// Bloquear/Desbloquear
if($option=="bCalles"){
	try{
		$id		= $_REQUEST['idCalles'];
		$update   = $oCluster->bloqueo($id);
		if ( $update ) {
				//$log = array($idUW,'Item','Bloqueo de item en el sistema. Id item modificado: '.$id);
				//$oLog->setLog($log);
				echo "done";
			
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="hCalles"){
	try{
		$id		= $_REQUEST['idCalles'];
		$update   = $oCluster->habilitar($id);
		if ( $update ) {
			//$log = array($idUW,'UsuarioWeb','Habilitar item en el sistema. Id item modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

/* ---- /SUSCRIPCIONES ---- */

?>