<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cUsuario.php';
include_once '../classes/cPoligono.php';

$oUsuario     = new Usuario();
$oPoligono 	= new Poligono();
//$oLog  				= new Log();

session_start();

$idTP =  $_SESSION['Altamira']['id'];

//echo($idUW);

if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

/* ---- TIPO DE PROVEEDORES ---- */
  
//Guarda Nuevo Usuario Web
if ( $option == 'nPoligono' ) {
	try{
		//parametros 
		$nombre					= $_POST['nombre'];
		$estado						= $_POST['estado'];
			
		$params = array($nombre,$estado);
			$save   = $oPoligono->nuevo($params);
				if ( $save ) {
					//$log = array($idUW,'Item','Ingreso de nuevo item al sistema. Item creado: '.$nombre);
					//$oLog->setLog($log);
					echo "done";
				} else {
					echo "error";
				}
	}catch (Exception $e){
		echo "error";
	}
}

//Modificar Usuario  
if ( $option == 'mPasaje' ) {
	try{
		//parametros 
		$idPoligono				= $_POST['idPasaje'];
		$nombre					= $_POST['nombre'];
		$estado						= $_POST['estado'];
		
			$params = array($nombre,$estado,$idPoligono);
			$update   = $oPoligono->modificar($params);
			if ( $update ) {
				//$log = array($idUW,'Item','Modificación de item en el sistema. Item modificado: '.$nombre);
				//$oLog->setLog($log);
				echo "done";
			} else {
				echo "error";
			}
			
		
	}catch (Exception $e){
		echo "error";
	}
}

// Bloquear/Desbloquear
if($option=="bPasaje"){
	try{
		$id		= $_REQUEST['idPasaje'];
		$update   = $oPoligono->bloqueo($id);
		if ( $update ) {
				//$log = array($idUW,'Item','Bloqueo de item en el sistema. Id item modificado: '.$id);
				//$oLog->setLog($log);
				echo "done";
			
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="hPasaje"){
	try{
		$id		= $_REQUEST['idPasaje'];
		$update   = $oPoligono->habilitar($id);
		if ( $update ) {
			//$log = array($idUW,'UsuarioWeb','Habilitar item en el sistema. Id item modificado: '.$id);
			//$oLog->setLog($log);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}

/* ---- /SUSCRIPCIONES ---- */

?>