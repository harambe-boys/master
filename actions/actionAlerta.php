<?php

include_once '../data/dataBase.php';
// Classes
include_once '../classes/cAlertasR.php';
include_once "../classes/cNotificaciones.php";
//include_once "../classes/cLog.php";

//Abriendo Sesiones
session_start();

// Params    
$oAlerta    = new AlertasR();
$oPush 		= new PushService();
//$oLog 		= new Log();

if (isset($_REQUEST['opt'])) 
{
  $option = $_REQUEST['opt'];
}
/* ---- Alerta ---- */


if ( $option == 'AV' ) {
	try{
		$id 		= $_REQUEST['i'];
		$talerta 	= $_REQUEST['talerta'];
		$text 		= $_REQUEST['text'];
		$checkbox 	= $_REQUEST['checkbox'];
		$tipo		 	= 2;
		$token   	= $oAlerta->getTokenPush($id);
		$update		= $oAlerta->validarAlerta($id);
		if ( $update ) {
			if($checkbox==1){
				if($token['idDipositivo']==1){
					$oPush->PushAndroid($token['tokenPush'],'Reporte de Alerta',$text,0);
				}elseif($token['idDipositivo']==2){
					$oPush->PushIOS($token['tokenPush'],'Reporte de Alerta',$text,0,0);
				}
				
			}
			$params = array('Reporte de Alerta',$text);
			$result = 	$oAlerta->nuevoNotificacion($params);
			if($result){
				$params = array($result,$token['idusuario']);
				$oAlerta->nuevoUsuario_Notificacion($params);
				$params = array($result,$id );
				$oAlerta->nuevoNotificacion_Alerta($params);
			}
			//$params = array($_SESSION['mred']['id'],$id,4);
			//$oLog->setLogAlerta($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if ( $option == 'AR' ) {
	try{
		$id 	= $_REQUEST['i'];
		$update = $oAlerta->rechazarAlerta($id);
		if ( $update ) {
			//$params = array($_SESSION['mred']['id'],$id,5);
			//$oLog->setLogAlerta($params);
			echo "done";
		} else {
			echo "error";
		}
	}catch(Exception $e){
		return "error";
	}
}
?>