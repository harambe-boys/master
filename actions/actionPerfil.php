<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cPerfil.php';


$oPerfil     	= new Perfil();
session_start();
if (isset($_POST['opt'])) 
{
  $option = $_POST['opt']; 
}

if (isset($_GET['opt'])) 
{
  $option = $_GET['opt'];
}

if ( $option == 'nPerfil' ) {
	try{
		//parametros
		$titulo   	= $_POST['titulo'];
		$estado   	= $_POST['estado'];
		$permisos 	= $_POST['permisos'];
		$params 	= array($titulo,$estado);
		$id		= $oPerfil->nuevoPerfil($params);
		if ( $id ) {
			foreach($permisos AS $idP){
				$params = array($idP,$id);
				$oPerfil->nuevoPermiso($params);
			}			
			echo "done";
		} else {
			echo "error";
		}
	}catch (Exception $e){
		echo "error"; $e->getMessage();
	}
}
if ( $option == 'mPerfil' ) {
	try{
		//parametros
		$id   		= $_POST['idPer'];
		$titulo   	= $_POST['titulo'];
		$estado   	= $_POST['estado'];
		$permisos 	= $_POST['permisos'];
		$vPermisos	= $oPerfil->getPermisosP($id);
		$arrayPA	= array();
		if($vPermisos){
			foreach($vPermisos AS $idP =>$array){
				$arrayPA[] = $array['id'];
			}
		}
		$arrayDif1 = array_diff($permisos,$arrayPA);
		$arrayDif2 = array_diff($arrayPA,$permisos);
		if($arrayDif1){
			foreach($arrayDif1 AS $el){
				$params = array($el,$id);
				$oPerfil->nuevoPermiso($params);	
			}
		}
		if($arrayDif2){
			foreach($arrayDif2 AS $el){
				$params = array($el,$id);
				$oPerfil->eliminarPermiso($params);	
			}
		}
		echo "done";
	}catch (Exception $e){
		echo "error"; $e->getMessage();
	}
}



?>