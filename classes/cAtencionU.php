<?php
class Atencion {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	
    function getAtencionAll()  
    {
        $sql = "SELECT a.id, a.asunto, a.descripcion, a.fechaCreacion, a.idusuario, u.idusuario, u.nombre FROM atencionusuario AS a
					INNER JOIN usuario AS u ON u.idusuario = a.idusuario WHERE 	a.id != 22 AND a.id != 94;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    = $rs->fields['id'];
                $info[$id]['asunto'] 	    = $rs->fields['asunto'];
				$info[$id]['descripcion']	= $rs->fields['descripcion'];
				$info[$id]['fechaCreacion'] = $rs->fields['fechaCreacion'];
				$info[$id]['nombre']  	    = $rs->fields['nombre'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
	function getOne($id)
    {
        $sql = "SELECT id,descripcion,asunto,fechaCreacion,imagen,estado,idusuario,correo,nombre,poligono,cluster,casa,idDipositivo,tokenPush FROM atencion_cliente WHERE id=?;";

        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info['id'] 	    	= $rs->fields['id'];
                $info['asunto'] 	    = $rs->fields['asunto'];
				$info['descripcion']	= $rs->fields['descripcion'];
				$format					= $rs->fields['fechaCreacion'];
				$fechaFormat	= date('d-m-Y H:i', strtotime($format));	
				$info['fechaCreacion'] 	= $fechaFormat;
				$info['imagen'] 		= $rs->fields['imagen'];
				$info['estado']  	    = $rs->fields['estado'];
				$info['idusuario']  	= $rs->fields['idusuario'];
				$info['correo']			= $rs->fields['correo'];
				$info['nombre']  	    = $rs->fields['nombre'];
				$info['poligono']  		= $rs->fields['poligono'];
				$info['cluster']  	    = $rs->fields['cluster'];
				$info['casa']  	    	= $rs->fields['casa'];
				$info['idDipositivo']  	= $rs->fields['idDipositivo'];
				$info['tokenPush']  	= $rs->fields['tokenPush'];
                $rs->MoveNext();
            }
            $rs->Close(); 
            return $info;
        } else {
            return false;
        }
    }
    function nuevoDetalle($params) 
	{
        $sql = "INSERT INTO notificacion_usuario_atencion (id_atencion,id_usuario_web,id_notificacion) VALUES (?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
	function nuevaNotificacion($params) 
	{
        $sql = "INSERT INTO notificacion (titulo,descripcion,estadoPush,estado) VALUES (?,?,?,1);";
			 
        $save = $this->DATA->Execute($sql, $params);
		$id   = $this->DATA->Insert_ID();
        if ($save){
            return $id;
        } else {
            return false;
        }
    }
	function getNotificaciones($id)
    {
        $sql = "SELECT id,descripcion,fecha FROM notificacion_atencion WHERE id_atencion=?;";
        $rs = $this->DATA->Execute($sql,$id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    = $rs->fields['id'];
                $info[$id]['descripcion'] 	= $rs->fields['descripcion'];
				$info[$id]['fecha']			= $rs->fields['fecha'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
}
?>