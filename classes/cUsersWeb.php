<?php
class UsersWeb{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }

	// get user
	function getUsuarioOne($idUsuario){
		try{
			$sql 	= "SELECT idusuario, correo,  nombre, codigoAcceso, cluster, casa, fechaCreacion, estado FROM usuario WHERE idusuario=? ;";
			
			$rs		= $this->DATA->Execute($sql, $idUsuario);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 								= $rs->fields['idusuario'];
					$info[$id]['correo']			= $rs->fields['correo'];
					$info[$id]['nombre']			= $rs->fields['nombre'];
					$info[$id]['codigoAcceso'] 	= $rs->fields['codigoAcceso'];
					$info[$id]['cluster'] 			= $rs->fields['cluster'];
					$info[$id]['casa'] 				= $rs->fields['casa'];
					$info[$id]['fechaCreacion'] = $rs->fields['fechaCreacion'];
					$info[$id]['estado'] 			= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	
		function getOneU($id)
    {
        $sql = "SELECT u.idusuario, u.correo, u.nombre, u.codigoAcceso, c.cluster, u.casa, u.fechaCreacion, u.tokenPush, u.idDipositivo FROM usuario AS u
					INNER JOIN cluster as c ON u.cluster = c.idcluster  WHERE idusuario=?;";

        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info['idusuario'] 		= $rs->fields['idusuario'];
                $info['correo'] 	    	= $rs->fields['correo'];
				$info['nombre'] 	    	= $rs->fields['nombre'];
				$info['codigoAcceso']	= $rs->fields['codigoAcceso'];
				$format					= $rs->fields['fechaCreacion'];
				$fechaFormat	= date('d-m-Y H:i', strtotime($format));	
				$info['fechaCreacion'] 	= $fechaFormat;
				$info['cluster'] 		= $rs->fields['cluster'];
				$info['casa']  	= $rs->fields['casa'];
				$info['idDipositivo']  	= $rs->fields['idDipositivo'];
				$info['tokenPush']  	= $rs->fields['tokenPush'];
                $rs->MoveNext();
            }
            $rs->Close(); 
            return $info;
        } else {
            return false;
        }
    }
	// get user disabled
	function getUserAlldisabled(){
		try{
			$sql 	= "SELECT u.idusuario, u.correo,  u.nombre, u.codigoAcceso, u.casa, u.fechaCreacion, u.estado, c.cluster FROM usuario AS u
						INNER JOIN cluster as c ON u.cluster = c.idcluster 
						WHERE u.estado = 0 AND u.idusuario != 40;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idusuario'];
					$info[$id]['correo']				= $rs->fields['correo'];
					$info[$id]['nombre']			= $rs->fields['nombre'];
					$info[$id]['codigoAcceso'] 	= $rs->fields['codigoAcceso'];
					$info[$id]['casa'] 				= $rs->fields['casa'];
					$info[$id]['fechaCreacion'] 	= $rs->fields['fechaCreacion'];
					$info[$id]['estado'] 			= $rs->fields['estado'];
					$info[$id]['cluster'] 			= $rs->fields['cluster'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
		// get user enable
	function getUserAllenable(){
		try{
			$sql 	= "SELECT u.idusuario, u.correo,  u.nombre, u.codigoAcceso,  c.cluster, u.casa, u.fechaCreacion, u.estado FROM usuario AS u 
						INNER JOIN cluster as c ON u.cluster = c.idcluster
						WHERE u.estado = 1 AND u.idusuario != 40;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idusuario'];
					$info[$id]['correo']				= $rs->fields['correo'];
					$info[$id]['nombre']				= $rs->fields['nombre'];
					$info[$id]['codigoAcceso'] 	= $rs->fields['codigoAcceso'];
					$info[$id]['cluster'] 				= $rs->fields['cluster'];
					$info[$id]['casa'] 				= $rs->fields['casa'];
					$info[$id]['fechaCreacion'] 	= $rs->fields['fechaCreacion'];
					$info[$id]['estado'] 				= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function clusterAll(){
		try{
			$sql 	= "SELECT idcluster, cluster FROM cluster;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idcluster'];
					$info[$id]['cluster']				= $rs->fields['cluster'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function poligonoAll(){
		try{
			$sql 	= "SELECT idpoligono, poligono FROM poligono;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idpoligono'];
					$info[$id]['poligono']			= $rs->fields['poligono'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	//validate user duplicated
	function validateUser($user)
    {
        $sql = "SELECT COUNT(idusuarioweb) AS cantidad FROM usuarioweb WHERE username = ?;";

        $rs = $this->DATA->Execute($sql, $user);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id   	= $rs->fields['cantidad'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $id;
        } else {
            return false;
        }
    }
	
	// get user by id
	function getUserWebId($idUserWeb){
		try{
			$sql 	= "
				SELECT 
				UW.idusuarioweb, UW.idrol, D.iddepartamento, UW.idmunicipio, UW.username, UW.idestado
				FROM usuarioweb AS UW
				INNER JOIN municipio AS M
				ON UW.idmunicipio = M.idmunicipio
				INNER JOIN departamento AS D
				ON M.iddepartamento = D.iddepartamento
				WHERE UW.idusuarioweb = $idUserWeb
			";
			
			$rs	= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id									= $rs->fields['idusuarioweb'];
					$info[$id]['idrol'] 					= $rs->fields['idrol'];
					$info[$id]['iddepartamento'] 	= $rs->fields['iddepartamento'];
					$info[$id]['idmunicipio'] 		= $rs->fields['idmunicipio'];
					$info[$id]['username'] 			= $rs->fields['username'];
					$info[$id]['idestado'] 			= $rs->fields['idestado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	
	function newUser($params){
		$sql = "INSERT INTO usuarioweb (idrol,idmunicipio,username,password,fechacreacion,ultimoingreso,idestado) VALUES (?,?,?,?,?,?,?);";

		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true;
		} else {
			return false;
		}
	}
	
	function nuevodetalle($params){
		$sql = "INSERT INTO detalle_usuario_cluster_poligono (idusuario, idcluster, idpoligono) VALUES (?,?,?);";

		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true;
		} else {
			return false;
		}
	}
	
	
	function modificarP($params){
		$sql = "UPDATE usuario SET password=? ,estado=? "
			."WHERE idusuario = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function modUserA($params){
		$sql = "UPDATE usuarioweb SET  idrol=?, idmunicipio=?, username=?,idestado=? "
			."WHERE idusuarioweb = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	
	function modUserPerfil1($params){
		$sql = "UPDATE usuarioweb SET  username=?, password=? "
			."WHERE idusuarioweb = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function modUserPerfil2($params){
		$sql = "UPDATE usuarioweb SET  username=? "
			."WHERE idusuarioweb = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function bloqueo($idUsuario_web) {
        $sql = "UPDATE usuario SET estado = 0 "
             . "WHERE idusuario = ?";

        $update = $this->DATA->Execute($sql, $idUsuario_web);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario_web) {
        $sql = "UPDATE usuario SET  estado = 1 "
             . "WHERE idusuario = ?";

        $update = $this->DATA->Execute($sql, $idUsuario_web);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	
	
	/*
	function getUsuariosapp(){
		try{
			$sql = "SELECT up.idusuarioapp,up.nombre,up.fechanacimiento,up.correo,up.iddispositivo,p.nombre AS nombrepais,up.estado FROM usuarioapp AS up
					INNER JOIN pais AS p ON p.idpais = up.idpais ";

			$rs = $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ) 
			{
				while(!$rs->EOF){
					$id                  			= $rs->fields['idusuarioapp'];
					$info[$id]['nombre'] 	 		= $rs->fields['nombre'];
					$info[$id]['fechanacimiento'] 	= $rs->fields['fechanacimiento'];
					$info[$id]['correo']  			= $rs->fields['correo'];
					$info[$id]['iddispositivo'] 	= $rs->fields['iddispositivo'];
					$info[$id]['nombrepais']  		= $rs->fields['nombrepais'];
					$info[$id]['estado']  			= $rs->fields['estado'];
					
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
		
	}
	function bloqueo($idUsuario) {
        $sql = "UPDATE usuarioapp SET estado = 0 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario) {
        $sql = "UPDATE usuarioapp SET  estado = 1 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }*/
	
}
?>