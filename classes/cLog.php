<?php
class Log{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }
	
	function getLogsAll(){
		try{
			$sql 	= "SELECT l.idLogAction, l.accion, l.titulo_push, l.mensaje_push, l.fechaCreacion, uw.nombre from logAction AS l
						INNER JOIN usuario_web AS uw ON l.idusuario_web = uw.idusuario_web;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 											= $rs->fields['idLogAction'];
					$info[$id]['accion']						= $rs->fields['accion'];
					$info[$id]['titulo_push']				= $rs->fields['titulo_push'];
					$info[$id]['mensaje_push']			= $rs->fields['mensaje_push'];
					$info[$id]['fechaCreacion']			= $rs->fields['fechaCreacion'];
					$info[$id]['nombre']					= $rs->fields['nombre'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
    /*function setLogAlerta($params) 
	{
        $sql = "INSERT INTO log_alertas (id_usuarioweb,id_alerta_municipal,id_estado) VALUES (?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }*/
	function setLogGestion($params) 
	{
        $sql = "INSERT INTO log_gestion (id_usuarioweb,id_gestion_municipal,id_estado) VALUES (?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
	
		function Log($params) 
	{
        $sql = "INSERT INTO logAction (accion,titulo_push,mensaje_push,idusuario_web) VALUES (?,?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
}
?>