<?php
class Cluster{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }

	// get tipo proveedor one
	function getCluster($idcluster){
		try{
			$sql 	= "SELECT idcluster, cluster, estado FROM cluster WHERE idcluster=?;";
			
			$rs		= $this->DATA->Execute($sql, $idcluster);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idcluster'];
					$info[$id]['cluster']				= $rs->fields['cluster'];
					$info[$id]['estado']				= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	// get tipo proveedor 
	function clusterAll(){
		try{
			$sql 	= "SELECT idcluster, cluster, estado FROM cluster;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 									= $rs->fields['idcluster'];
					$info[$id]['cluster']				= $rs->fields['cluster'];
					$info[$id]['estado']				= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function nuevo($params){
		$sql = "INSERT INTO cluster (cluster,estado) VALUES (?,?);";
		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true; 
		} else {
			return false;
		}
	}
	
	
	function modificar($params){
		$sql = "UPDATE cluster SET cluster=?, estado=? "
			."WHERE idcluster = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function bloqueo($idcluster) {
        $sql = "UPDATE cluster SET estado = 0 "
             . "WHERE idcluster = ?";

        $update = $this->DATA->Execute($sql, $idcluster);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idcluster) {
        $sql = "UPDATE cluster SET  estado = 1 "
             . "WHERE idcluster = ?";

        $update = $this->DATA->Execute($sql, $idcluster);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	
	
	/*
	function getUsuariosapp(){
		try{
			$sql = "SELECT up.idusuarioapp,up.nombre,up.fechanacimiento,up.correo,up.iddispositivo,p.nombre AS nombrepais,up.estado FROM usuarioapp AS up
					INNER JOIN pais AS p ON p.idpais = up.idpais ";

			$rs = $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ) 
			{
				while(!$rs->EOF){
					$id                  			= $rs->fields['idusuarioapp'];
					$info[$id]['nombre'] 	 		= $rs->fields['nombre'];
					$info[$id]['fechanacimiento'] 	= $rs->fields['fechanacimiento'];
					$info[$id]['correo']  			= $rs->fields['correo'];
					$info[$id]['iddispositivo'] 	= $rs->fields['iddispositivo'];
					$info[$id]['nombrepais']  		= $rs->fields['nombrepais'];
					$info[$id]['estado']  			= $rs->fields['estado'];
					
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
		
	}
	function bloqueo($idUsuario) {
        $sql = "UPDATE usuarioapp SET estado = 0 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario) {
        $sql = "UPDATE usuarioapp SET  estado = 1 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	*/
}
?>