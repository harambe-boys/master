<?php
class Push {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
		function getAndroidUsuario($id){
		try{
			$sql 	="SELECT u.tokenPush FROM usuario as u INNER JOIN detalle_usuario_cluster_poligono AS duc ON u.idusuario=duc.idusuario
WHERE duc.idcluster = $id AND idDipositivo = 1;";
			$sql2 = "SELECT tokenPush FROM usuario WHERE cluster = $id AND idDipositivo = 1;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql2);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	// get ios pushtoken
	function getIOSUsuario($id){
		try{
			$sql 	= "SELECT u.tokenPush FROM usuario as u INNER JOIN detalle_usuario_cluster_poligono AS duc ON u.idusuario=duc.idusuario
WHERE duc.idcluster = $id AND idDipositivo = 2;";

			$sql2 = "SELECT tokenPush FROM usuario WHERE cluster = $id AND idDipositivo = 2;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql2);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	///CLUSTER Y POLIGONO
	
	function getAndroidUsuario2($poli,$calle){
		try{
			$args = array($poli,$calle);
			$sql 	= "SELECT u.tokenPush FROM usuario AS u
						INNER JOIN detalle_usuario_cluster_poligono AS cp ON u.idusuario = cp.idusuario
						WHERE idDipositivo=1 AND estadoNotificacion = 1 AND (cp.idcluster = ? AND cp.idpoligono = ?);";
			$i = 0;
			$rs		= $this->DATA->Execute($sql,$args);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	// get ios pushtoken
	function getIOSUsuario2($poli){
		try{
			$args = array($poli);
			$sql 	= "SELECT u.tokenPush FROM usuario AS u
						INNER JOIN detalle_usuario_cluster_poligono AS cp ON u.idusuario = cp.idusuario
						WHERE idDipositivo=2 AND estadoNotificacion = 1 AND (cp.idcluster = 1 AND cp.idpoligono = ?);";
			$i = 0;
			$rs		= $this->DATA->Execute($sql,$args);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	
	///FIN DE CLUSTER Y POLIGONO
	
	 function getCluster()
    {
        $sql = "SELECT idcluster, cluster FROM cluster WHERE estado = 1;";
		
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
				$id								= $rs->fields['idcluster'];
				$info[$id]['cluster'] 	    	= $rs->fields['cluster'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
	 function getPoligono($calle)
    {
        $sql = "SELECT p.idpoligono, p.poligono, c.cluster FROM poligono as p
					INNER JOIN detalle_usuario_cluster_poligono AS cp ON p.idpoligono = cp.idpoligono
					INNER JOIN cluster AS c ON c.idcluster = cp.idcluster where c.idcluster = ?;"; 
		
        $rs = $this->DATA->Execute($sql, $calle);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
				$id							 = $rs->fields['idpoligono'];
				$info[$id]['poligono'] 	 = $rs->fields['poligono'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return FALSE;
        }
    }
	
	function nuevoDetalle($params) 
	{
        $sql = "INSERT INTO notificacion_usuario_atencion (id_atencion,id_usuario_web,id_notificacion) VALUES (?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
	function nuevaNotificacion($params) 
	{
        $sql = "INSERT INTO notificacion (titulo,descripcion,estadoPush) VALUES (?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
		$id   = $this->DATA->Insert_ID();
        if ($save){
            return $id;
        } else {
            return false;
        }
    }
	
	function PushAndroidUsuario($array,$titulo,$mensaje,$t){
	//echo($array);
		try{		
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyCv4PDSQiQgLYU6gsP3QARt1jlCVnSkpEc";
			// Set POST variables
			$url = 'https://android.googleapis.com/gcm/send';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			$fields = array(
							'registration_ids'  => ($array),
							'data'              => array("titulo"=>$titulo, "mensaje" => $mensaje, "tipo"=>$t), 
							);
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			//var_dump($fields);
			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;
		}catch(Exception $e){
			return false;
		}
	}
	


	
	function PushIOSUsuario($array,$titulo,$mensaje,$t){
		try{			
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyC4sxtQr2kjQnz-QWVSv4GB4FxArr01BQg";
			// Set POST variables
			$url = 'https://gcm-http.googleapis.com/gcm/send';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			//$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';
			
			$fields = (object)  array(
							//'registration_ids'  => ($array),
							//'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2), 
							'registration_ids' =>array($array),
							'content_available' => false,
							'priority' =>  "high",
							'notification' => (object) array("body" =>$titulo, "title"=> $mensaje, "sound"=>"default"),
							'data' => (object) array("tipo"=>$t)
							);
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			//var_dump(json_encode($fields));
			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;
		}catch(Exception $e){
			return false;
		}
	}

}
?>