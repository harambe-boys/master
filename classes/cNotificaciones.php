<?php
class PushService{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }
	// get android pushtoken
	function getAndroidPush(){
		try{
			$sql 	= "SELECT tokenPush FROM usuario WHERE idDipositivo=1 AND estadoNotificacion = 1;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function getAndroidPush1($idu){
		try{
			$sql 	= "SELECT tokenPush FROM usuario WHERE idDipositivo=1 AND estadoNotificacion = 1 AND idusuario=?;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql,$idu);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	
	// get ios pushtoken
	function getIOSPush(){
		try{
			$sql 	= "SELECT tokenPush FROM usuario WHERE idDipositivo=2  AND estadoNotificacion = 1;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	// get ios pushtoken
	function getIOSPush1($idu){
		try{
			$sql 	= "SELECT tokenPush FROM usuario WHERE  idusuario=? AND idDipositivo=2  AND estadoNotificacion = 1 ;";
			$i = 0;
			$rs		= $this->DATA->Execute($sql,$idu);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info[$i]		= $rs->fields['tokenPush'];
					$i++;
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function PushAndroid($array,$titulo,$mensaje,$idu){
	//echo($array);
		try{	
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyCv4PDSQiQgLYU6gsP3QARt1jlCVnSkpEc";
			// Set POST variables
			$url = 'https://android.googleapis.com/gcm/send';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			$fields = array(
							'registration_ids'  => array($array),
							'data'              => array("titulo"=>$titulo, "mensaje" => $mensaje, "id"=>$idu), 
							);
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;;
		}catch(Exception $e){
			return false;
		}
	}
	
	
	
	function PushIOS($array,$titulo,$mensaje,$id){
		try{			
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyC4sxtQr2kjQnz-QWVSv4GB4FxArr01BQg";
			// Set POST variables
			$url = 'https://gcm-http.googleapis.com/gcm/send';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			//$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';
			
			$fields = (object)  array(
							//'registration_ids'  => ($array),
							//'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2), 
							'registration_ids' =>array($array),
							'content_available' => false,
							'priority' =>  "high",
							'notification' => (object) array("body" =>$titulo, "title"=> $mensaje, "sound"=>"default"),
							'data' => (object) array("id"=>$id)
							);
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			//var_dump(json_encode($fields));
			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;
		}catch(Exception $e){
			return false;
		}
	}
	
	function PushAndroidGlobal($array,$titulo,$mensaje){
	//echo($array);
		try{
			//$array = "APA91bHKouzmT6j03Gh7pfuA3a-0pDD33GxBi60GbzVShPqvNA1URVNXqpoz6K9FIyPLely-RwMO0L6mPa9VY4cOcRoukOsm_JfFhBWgWluweeQYbDKZGwN0F_GGn5qJIDaYeILMTSSY";
			
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyCv4PDSQiQgLYU6gsP3QARt1jlCVnSkpEc";
			// Set POST variables
			$url = 'https://android.googleapis.com/gcm/send';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			$fields = array(
							'registration_ids'  =>$array,
							'data'              => array("titulo"=>$titulo, "mensaje" => $mensaje, "tipo"=>1), 
							); 
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;
		}catch(Exception $e){
			return false;
		}
	} 
	
	function PushIOS2Global($array,$titulo,$mensaje,$t){
		try{			
			// Mensaje ya esta todo arriba
			$idkeyserver="AIzaSyC4sxtQr2kjQnz-QWVSv4GB4FxArr01BQg";
			// Set POST variables
			$url = 'https://gcm-http.googleapis.com/gcm/send';
			//$array="ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU";
			//$array = 'ej6x2i_PGgs:APA91bHXJEDd31CiEMc5DHQUqVzLfqWJc5del6AlWjq8xeizDwE2AgidVtc7hhpRyCdA3w6jQQ_c1kjTyoCzujBdoLfR99fOOd_8yn-uPGvS2L03nu4aOTQz-DXQ9T4qXtSWiR4N-DAU';
			//QUITAMOS array($var) Y PONEMOS $arrayandroidpush PARA MANDAR A TODOS LOS DISPOSITIVOS QUE SE REQUIERA
			
			//$array = 'mWa2XfdKxHU:APA91bFtu2bvCR2y9-RXhB0F-VSGdCdTmtEdAwKS2_EixvYAzN8V4LHzERo-EqaomJIrfiVXuMJIBna6buMiSigWk0gc9_03TGP6UgrXHpOfzwWBluvpXhNczYl40sQZhExE3S8DWgkl';
			
			$fields = (object)  array(
							//'registration_ids'  => ($array),
							//'data'              => array("titulo"=>"prueba Global", "mensaje" => "prueba Global", "tipo"=>2), 
							'registration_ids' =>($array),
							'content_available' => false,
							'priority' =>  "high",
							'notification' => (object) array("body" =>$titulo, "title"=> $mensaje,  "sound"=>"default"),
							'data' => (object) array("tipo"=>$t)
							);
			
			$headers = array( 
								'Authorization: key=' . $idkeyserver,
								'Content-Type: application/json'
							);

			//var_dump(json_encode($fields));
			// Open connection
			$ch = curl_init();
			
			if (FALSE === $ch)
			throw new Exception('failed to initialize');
			
			
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url );

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch);
			if (FALSE === $result)
			 throw new Exception(curl_error($ch), curl_errno($ch));
			// Close connection
			curl_close($ch);
			return $result;
		}catch(Exception $e){
			return false;
		}
	}
}
?>