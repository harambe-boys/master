<?php
class Poligono{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }

	// get tipo proveedor one
	function getPoligono($idpoligono){
		try{
			$sql 	= "SELECT idpoligono, poligono, estado FROM poligono WHERE idpoligono=?;";
			
			$rs		= $this->DATA->Execute($sql, $idpoligono);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idpoligono'];
					$info[$id]['poligono']				= $rs->fields['poligono'];
					$info[$id]['estado']					= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	// get tipo proveedor 
	function poligonosAll(){
		try{
			$sql 	= "SELECT idpoligono, poligono, estado FROM poligono;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idpoligono'];
					$info[$id]['poligono']				= $rs->fields['poligono'];
					$info[$id]['estado']					= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function nuevo($params){
		$sql = "INSERT INTO poligono (poligono,estado) VALUES (?,?);";
		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true; 
		} else {
			return false;
		}
	}
	
	
	function modificar($params){
		$sql = "UPDATE poligono SET poligono=?, estado=? "
			."WHERE idpoligono = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function bloqueo($idpoligono) {
        $sql = "UPDATE poligono SET estado = 0 "
             . "WHERE idpoligono = ?";

        $update = $this->DATA->Execute($sql, $idpoligono);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idpoligono) {
        $sql = "UPDATE poligono SET  estado = 1 "
             . "WHERE idpoligono = ?";

        $update = $this->DATA->Execute($sql, $idpoligono);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	
	
	/*
	function getUsuariosapp(){
		try{
			$sql = "SELECT up.idusuarioapp,up.nombre,up.fechanacimiento,up.correo,up.iddispositivo,p.nombre AS nombrepais,up.estado FROM usuarioapp AS up
					INNER JOIN pais AS p ON p.idpais = up.idpais ";

			$rs = $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ) 
			{
				while(!$rs->EOF){
					$id                  			= $rs->fields['idusuarioapp'];
					$info[$id]['nombre'] 	 		= $rs->fields['nombre'];
					$info[$id]['fechanacimiento'] 	= $rs->fields['fechanacimiento'];
					$info[$id]['correo']  			= $rs->fields['correo'];
					$info[$id]['iddispositivo'] 	= $rs->fields['iddispositivo'];
					$info[$id]['nombrepais']  		= $rs->fields['nombrepais'];
					$info[$id]['estado']  			= $rs->fields['estado'];
					
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
		
	}
	function bloqueo($idUsuario) {
        $sql = "UPDATE usuarioapp SET estado = 0 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario) {
        $sql = "UPDATE usuarioapp SET  estado = 1 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	*/
}
?>