<?php
class Visitas{
	
	//Constructor
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
		
    }
	
	
	//All visitas
	function getAllVisitas(){
		try{
			date_default_timezone_set('America/El_Salvador');
			$fecha 		= date('Y-m-d');
			$date 		= date('H:i:s');
			$antes 		= date('H:i:s',strtotime($date .' -3 hours'));
			$despues 	= date('H:i:s',strtotime($date .' +3 hours'));
			if($despues>='00:00:01' && $despues<='03:00:00'){
				$despues = '23:59:59';
			}
			$sql = "SELECT v.*, u.nombre AS residente FROM visita AS v 
					INNER JOIN usuario AS u ON u.idusuario = v.idusuario
					WHERE v.estado = 0 AND v.fecha='$fecha ' AND v.hora BETWEEN '$antes' AND '$despues';"; 
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
					$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					$id 							= $rs->fields['idvisita'];
					$info[$id]['nombre']			= $rs->fields['nombre'];
					$info[$id]['placa_vehiculo']	= $rs->fields['placa_vehiculo'];
					$fecha							= $rs->fields['fecha'];
					$dia							= date('w', strtotime($fecha));
					$dia2							= date('d', strtotime($fecha));
					$mes							= date('m', strtotime($fecha));
					$format							= $dias[$dia]." ".$dia2." de ".$meses[date('n')-1]. " del ".date('Y');
					$info[$id]['dia']				= $dia2;
					$info[$id]['mes']				= $mes;
					$info[$id]['fecha']				= $rs->fields['fecha'];
					$info[$id]['fechaFormato']		= $format;
					$horaformat						= $rs->fields['hora'];
					$hora							= date('g:i A' ,  strtotime($horaformat));
					$info[$id]['hora']				= $hora;
					$info[$id]['notas']				= $rs->fields['notas'];
					$info[$id]['dui']				= $rs->fields['dui'];
					$info[$id]['notas']				= $rs->fields['notas'];
					$info[$id]['residente']			= $rs->fields['residente'];
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function getVisitasOne($id){
		try{
			$sql = "SELECT v.*, u.nombre AS residente, c.cluster, p.poligono FROM usuario AS u
						INNER JOIN detalle_usuario_cluster_poligono AS d ON u.idusuario = d.idusuario
						INNER JOIN cluster AS c ON c.idcluster = d.idcluster
						INNER JOIN poligono AS p ON p.idpoligono = d.idpoligono
						INNER JOIN visita AS v on v.idusuario = u.idusuario
					WHERE  v.idvisita = ? AND v.estado = 0"; 
			
			$rs		= $this->DATA->Execute($sql, $id);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
					$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					$id 											= $rs->fields['idvisita'];
					$info[$id]['nombre']					= $rs->fields['nombre'];
					$fecha										= $rs->fields['fecha'];
					$dia											= date('w', strtotime($fecha));
					$dia2											= date('d', strtotime($fecha));
					$mes										= date('m', strtotime($fecha));
					$format									= $dias[$dia]." ".$dia2." de ".$meses[date('n')-1]. " del ".date('Y');
					$info[$id]['dia']						= $dia2;
					$info[$id]['mes']						= $mes;
					$info[$id]['fecha']					= $rs->fields['fecha'];
					$info[$id]['fechaFormato']		= $format;
					$horaformat							= $rs->fields['hora'];
					$hora										= date('g:i A' ,  strtotime($horaformat));
					$info[$id]['hora']						= $hora;
					$info[$id]['notas']					= $rs->fields['notas'];
					$dui										= $rs->fields['dui'];
					$placa									=  $rs->fields['placa_vehiculo'];
					if($dui != ''){
						$info[$id]['dui']						= $rs->fields['dui'];
					}else{
						$info[$id]['dui']						= 'N/A';
					}
					if($placa != ''){
						$info[$id]['placa_vehiculo']						= $rs->fields['placa_vehiculo'];
					}else{
						$info[$id]['placa_vehiculo']						= 'N/A';
					}
					$info[$id]['notas']					= $rs->fields['notas'];
					$info[$id]['residente']				= $rs->fields['residente'];
					$info[$id]['cluster']					= $rs->fields['cluster'];
					$info[$id]['poligono']					= $rs->fields['poligono'];
					
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function actualizar($params){
		date_default_timezone_set('America/El_Salvador');
		$fecha 		= date('Y-m-d H:i:s');
		$sql = "UPDATE visita SET estado=?, hora_confirmacion='$fecha'"
			."WHERE idvisita = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	
	function getReporteVisitas($estado,$f1,$f2){
		try{
			if($estado==2){
				$sql = "SELECT estado,idvisita,nombre_visitante,placa_vehiculo,fecha,hora,notas,hora_confirmacion,nombre,cluster,poligono,casa 
					FROM visitante_usuario WHERE estado=0 AND fecha BETWEEN '$f1' AND '$f2';"; 
			}elseif($estado==3){
				$sql = "SELECT estado,idvisita,nombre_visitante,placa_vehiculo,fecha,hora,notas,hora_confirmacion,nombre,cluster,poligono,casa  
					FROM visitante_usuario WHERE estado = 1 AND fecha BETWEEN '$f1' AND '$f2';"; 
			}else{
				$sql = "SELECT estado,idvisita,nombre_visitante,placa_vehiculo,fecha,hora,notas,hora_confirmacion,nombre,cluster,poligono,casa  
					FROM visitante_usuario WHERE fecha BETWEEN '$f1' AND '$f2';"; 
			}
			
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 							= $rs->fields['idvisita'];
					$info[$id]['nombre_visitante']	= $rs->fields['nombre_visitante'];
					$info[$id]['placa_vehiculo']	= $rs->fields['placa_vehiculo'];
					$info[$id]['fecha']				= $rs->fields['fecha'];
					$info[$id]['hora']				= $rs->fields['hora'];
					$info[$id]['notas']				= $rs->fields['notas'];
					$info[$id]['residente']			= $rs->fields['nombre'];
					$info[$id]['hora_confirmacion']	= $rs->fields['hora_confirmacion'];
					$info[$id]['cluster']			= $rs->fields['cluster'];
					$info[$id]['poligono']			= $rs->fields['poligono'];
					$info[$id]['casa']				= $rs->fields['casa'];
					$info[$id]['estado']			= $rs->fields['estado'];
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
}
?>