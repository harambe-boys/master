<?php
class Proveedor{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }

	// get proveedor one
	function getProveedorOne($idProveedor){
		try{
			$sql 	= "SELECT idProveedor, nombre,  descripcion, imagen, telefono, puntuacion, estado, idProveedorcategoria FROM proveedor WHERE idProveedor=?;";
			
			$rs		= $this->DATA->Execute($sql, $idProveedor);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 											= $rs->fields['idProveedor'];
					$info[$id]['nombre']						= $rs->fields['nombre'];
					$info[$id]['descripcion']				= $rs->fields['descripcion'];
					$info[$id]['imagen'] 					= $rs->fields['imagen'];
					$info[$id]['telefono'] 					= $rs->fields['telefono'];
					$info[$id]['puntuacion'] 				= $rs->fields['puntuacion'];
					$info[$id]['estado'] 						= $rs->fields['estado'];
					$info[$id]['idProveedorcategoria'] 	= $rs->fields['idProveedorcategoria'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	// get Proveedores
	function getProveedorAll(){
		try{
			$sql 	= "SELECT p.idProveedor, p.nombre, p.descripcion, p.imagen, p.telefono, p.puntuacion, p.estado, pc.nombreCategoria FROM proveedor AS p"
						." INNER JOIN proveedorCategoria AS pc ON pc.idProveedorcategoria = p.idProveedorcategoria;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idProveedor'];
					$info[$id]['nombre']					= $rs->fields['nombre'];
					$info[$id]['descripcion']			= $rs->fields['descripcion'];
					$info[$id]['imagen'] 				= $rs->fields['imagen'];
					$info[$id]['telefono'] 				= $rs->fields['telefono'];
					$info[$id]['puntuacion'] 			= $rs->fields['puntuacion'];
					$info[$id]['estado'] 					= $rs->fields['estado'];
					$info[$id]['nombreCategoria'] 	= $rs->fields['nombreCategoria'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	// get tipo de proveedores
	function getTipoProveedor(){
		try{
			$sql 	= "SELECT idProveedorcategoria, nombreCategoria FROM proveedorCategoria";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idProveedorcategoria'];
					$info[$id]['nombreCategoria']	= $rs->fields['nombreCategoria'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	
	function nuevo($params){
		$sql = "INSERT INTO proveedor (nombre,descripcion,imagen,telefono,puntuacion,estado,idProveedorcategoria) VALUES (?,?,?,?,?,?,?);";
		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true; 
		} else {
			return false;
		}
	}
	
	
	function modificar($params){
		$sql = "UPDATE proveedor SET nombre=?, descripcion=?, imagen=?, telefono=?, puntuacion=?,  estado=?, idProveedorcategoria=? "
			."WHERE idProveedor = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function bloqueo($idProveedor) {
        $sql = "UPDATE proveedor SET estado = 0 "
             . "WHERE idProveedor = ?";

        $update = $this->DATA->Execute($sql, $idProveedor);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idProveedor) {
        $sql = "UPDATE proveedor SET  estado = 1 "
             . "WHERE idProveedor = ?";

        $update = $this->DATA->Execute($sql, $idProveedor);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
}
?>