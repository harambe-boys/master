<?php
class Usuario {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	//FUNCION QUE VERIFICA EL INGRESO DE LOS USUARIOS EN EL ADMINISTRADOR WEB DE LA APLICACION
    function ingreso($usuario, $contrasenia)
    {
		$sha1pass = ($contrasenia);
        $sql = "SELECT idusuario_web,nombre, idrol FROM usuario_web WHERE nombre=? AND contrasenia=? AND estado=1;";
        $rs = $this->DATA->Execute($sql, array($usuario, $sha1pass));
        if ( $rs->RecordCount() > 0 ) {
            $idusuario          	= $rs->fields['idusuario_web']; 
            $username            	= $rs->fields['nombre'];
			$_SESSION['Altamira']['rol']    = $rs->fields['idrol']; 
            $_SESSION['Altamira']['id']     	= $idusuario; 
            $_SESSION['Altamira']['user']   	= $username;
			$_SESSION['Altamira']['status']   	= "authenticate";
			
			$sql2 = "SELECT idpermiso_has_rol,idpermiso FROM permiso_has_rol WHERE idrol=?;";
			$data = $this->DATA->Execute($sql2, $rs->fields['idrol']);
			for ($j = 1; $j <= 20; $j++) {
				$_SESSION['Altamira']['permisos'][$j] = false;
			} 
			if ( $data->RecordCount()) {
				while(!$data->EOF){
					for ($i = 1; $i <= 20; $i++){
						if($data->fields['idpermiso']==$i){
							$_SESSION['Altamira']['permisos'][$i] = true;
						}
					}
					$data->MoveNext();
				} 
			}
			$data->Close();
			$rs->Close();
			return true;
        } else {
            return false; 
        }
    }
	//FUNCION QUE REGISTRA TODA ACCION QUE SE REALICE EN EL ADMINISTRADOR Y GUARDA EL REGISTRO EN LA TABLA DE LOGWEB
	function logweb($params)
    {
        $sql = "INSERT INTO logweb (datetime,action,idusuarioweb) VALUES (?,?,?);";			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
    function verSession()
    {
        session_start();
        if (isset($_SESSION['Altamira']['status'])) {
            $status = $_SESSION['Altamira']['status'];
            if ($status == "authenticate") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    function usuarios()
    {
        $sql = "SELECT u.idusuarioweb,u.idrolweb,u.username,u.password,u.fechacreacion,u.nombre,u.apellido,u.codigoempleado,u.estado,u.token,r.rol FROM usuarioweb AS u
					INNER JOIN rolweb AS r ON r.idrolweb =u.idrolweb;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    = $rs->fields['idusuarioweb'];
                $info[$id]['rol'] 	    	= $rs->fields['rol'];
				$info[$id]['username']	    = $rs->fields['username'];
				$info[$id]['password']  	= $rs->fields['password'];
				$info[$id]['fechacreacion'] = $rs->fields['fechacreacion'];
				$info[$id]['nombre']  	    = $rs->fields['nombre'];
				$info[$id]['apellido']  	= $rs->fields['apellido'];
				$info[$id]['codigoempleado']= $rs->fields['codigoempleado'];
				$info[$id]['estado']  	    = $rs->fields['estado'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
		function getUsuariowebAll(){
		try{
			$sql 	= "SELECT uw.idusuario_web, uw.nombre, uw.estado, r.nombre AS nombreRol FROM usuario_web AS uw INNER JOIN rol AS r ON r.idrol = uw.idrol;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 								= $rs->fields['idusuario_web'];
					$info[$id]['nombre']			= $rs->fields['nombre'];
					$info[$id]['estado'] 			= $rs->fields['estado'];
					$info[$id]['nombreRol']		= $rs->fields['nombreRol'];
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	 function getRolAll()
    {
        $sql = "SELECT idrol, nombre FROM rol;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    = $rs->fields['idrol'];
                $info[$id]['nombre'] 	    	= $rs->fields['nombre'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
	function getUsuario($idUsuario)
    {
        $sql = "SELECT idusuarioweb,idrolweb,username,password,fechacreacion,nombre,apellido,codigoempleado,estado,token FROM usuarioweb WHERE idusuarioweb = ?";

        $rs = $this->DATA->Execute($sql, $idUsuario);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    = $rs->fields['idusuarioweb'];
                $info[$id]['idrolweb'] 	    = $rs->fields['idrolweb'];
				$info[$id]['username']	    = $rs->fields['username'];
				$info[$id]['password']  	= $rs->fields['password'];
				$info[$id]['fechacreacion'] = $rs->fields['fechacreacion'];
				$info[$id]['nombre']  	    = $rs->fields['nombre'];
				$info[$id]['apellido']  	= $rs->fields['apellido'];
				$info[$id]['codigoempleado']= $rs->fields['codigoempleado'];
				$info[$id]['estado']  	    = $rs->fields['estado'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
	function getUsuariowebOne($idUsuario_web)
    {
        $sql = "SELECT idusuario_web,nombre,contrasenia,estado,idrol FROM usuario_web WHERE idusuario_web = ?";

        $rs = $this->DATA->Execute($sql, $idUsuario_web);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	    		= $rs->fields['idusuario_web'];
                $info[$id]['nombre'] 	    = $rs->fields['nombre'];
				$info[$id]['contrasenia']	= $rs->fields['contrasenia'];
				$info[$id]['estado']  		= $rs->fields['estado'];
				$info[$id]['idrol'] 			= $rs->fields['idrol'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
		//validate user duplicated
	function validateUser($user)
    {
        $sql = "SELECT COUNT(idusuario_web) AS cantidad FROM usuario_web WHERE nombre = ?;";

        $rs = $this->DATA->Execute($sql, $user);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id   	= $rs->fields['cantidad'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $id;
        } else {
            return false;
        }
    }
	
    function nuevo($params) 
	{
        $sql = "INSERT INTO usuario_web (nombre,contrasenia,estado,idrol) "
             . "VALUES(?, ?, ?, ?)";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }
	
	function modificar($params)
	{
        $sql = "UPDATE usuario_web SET nombre= ?,contrasenia= ?,estado= ?,idrol= ?"
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $params);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
		function modificar2($params)
	{
        $sql = "UPDATE usuario_web SET nombre= ?,estado= ?,idrol= ?"
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $params);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function bloqueo($idUsuario_web) {
        $sql = "UPDATE usuario_web SET estado = 0 "
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $idUsuario_web);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario_web) {
        $sql = "UPDATE usuario_web SET  estado = 1 "
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $idUsuario_web);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	function validar($titulo)
    {
        $sql = "SELECT COUNT(idusuarioweb) AS cantidad FROM usuarioweb WHERE username = ?;";

        $rs = $this->DATA->Execute($sql, $titulo);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id   	= $rs->fields['cantidad'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $id;
        } else {
            return false;
        }
    }
}
?>