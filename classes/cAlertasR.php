<?php
class AlertasR {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	
	function getAlertasR($idestado,$idtipoalerta,$f1,$f2){
		try{
			/*$string = '';
			$i = 0 ;
			for ($j = 5; $j <= 8; $j++){
				if($_SESSION['mred']['permisos'][$j]==true){
					if($j==5){
						$al = 2;
					}elseif($j==6){
						$al = 1;
					}elseif($j==7){
						$al = 3;
					}elseif($j==8){
						$al = 4;
					}
					if($i==0){
						$string .= "idalerta = $al ";
					}else{
						$string .= " OR idalerta = $al ";
					}
					$i = 1 ;
				}
			}
			if($string != '' ){
				$string = " ($string) AND";
			}*/
			if ($idtipoalerta == 0 && $idestado!=0){
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE estado=? AND (idTipoalerta=1 OR idTipoalerta=2 OR idTipoalerta=4) AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC; ";
				$rs		= $this->DATA->Execute($sql,array($idestado));
			} else if($idestado == 0 && $idtipoalerta == 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario  
							WHERE (idTipoalerta=1 OR idTipoalerta=2 OR idTipoalerta=4) AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql);
			} else if($idestado == 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta));
			}else if($idestado != 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND estado=? AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta,$idestado));
			}
			
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 					= $rs->fields['idAlerta'];
					$info[$id]['latitud'] 	= $rs->fields['latitud'];
					$info[$id]['longitud'] 	= $rs->fields['longitud'];
					$datetime				= $rs->fields['fechaCreacion'];
					$diferencia = strtotime(date('Y-m-d H:i:s')) - strtotime($datetime);
					//comprobamos el tiempo que ha pasado en segundos entre las dos fechas
					//floor devuelve el n�mero entero anterior, si es 5.7 devuelve 5
					if($diferencia < 60){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia) . ' segundos';
					}else if($diferencia > 60 && $diferencia < 3600){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/60) . ' minutos';
					}else if($diferencia > 3600 && $diferencia < 86400){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/3600) . ' horas';
					}else if($diferencia > 86400 && $diferencia < 2592000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/86400) . ' d�as';
					}else if($diferencia > 2592000 && $diferencia < 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/2592000) . ' meses';
					}else if($diferencia > 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/31104000) . ' a�os';
					}else{
						$info[$id]['tiempo'] = 'Error';
					}
					$info[$id]['fechaCreacion'] = $rs->fields['fechaCreacion'];
					$info[$id]['idusuario'] 	= $rs->fields['idusuario'];
					$info[$id]['idTipoalerta'] 	= $rs->fields['idTipoalerta'];
					$info[$id]['tipoAlerta'] 	= $rs->fields['tipoAlerta'];
					$info[$id]['nombre'] 		= $rs->fields['nombre'];
					$info[$id]['correo'] 		= $rs->fields['correo'];
					$info[$id]['cluster'] 		= $rs->fields['cluster'];
					$info[$id]['poligono'] 		= $rs->fields['poligono'];
					$info[$id]['casa'] 			= $rs->fields['casa'];
					$info[$id]['estado'] 			= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function getTokenPush($id){
		try{
		$sql 	= "SELECT idAlerta,tokenPush,idDipositivo,idusuario FROM alertas_usuario WHERE idAlerta=? AND estadoNotificacion=1;";
		$rs		= $this->DATA->Execute($sql,$id);
		if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info['tokenPush']		= $rs->fields['tokenPush'];
					$info['idDipositivo']	= $rs->fields['idDipositivo'];
					$info['idusuario']		= $rs->fields['idusuario'];
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function validarAlerta($id){
		try{
			$sql = "UPDATE alertas SET estado=2 WHERE  idAlerta =?;";
				 
			$update = $this->DATA->Execute($sql, $id);
			if ($update){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function nuevoNotificacion($params){
		try{
		
			$sql = "INSERT INTO notificacion (titulo,descripcion,estadoPush) VALUES (?,?,2);";
				 
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return $id;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function nuevoUsuario_Notificacion($params){
		try{
		
			$sql = "INSERT INTO usuario_has_notificacion (idNotificacion,idUsuario) VALUES (?,?);";
			$save = $this->DATA->Execute($sql, $params);
			if ($save){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function nuevoNotificacion_Alerta($params){
		try{
		
			$sql = "INSERT INTO notificacion_alerta_usuario (id_notificacion,id_alerta) VALUES (?,?);";
			$save = $this->DATA->Execute($sql, $params);
			if ($save){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function rechazarAlerta($id){
		try{
			$sql = "UPDATE alertas SET estado=3 WHERE  idAlerta =?;";
				 
			$update = $this->DATA->Execute($sql, $id);
			if ($update){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }

	function getAlertasE($idestado,$idtipoalerta,$f1,$f2){
		try{
			/*$string = '';
			$i = 0 ;
			for ($j = 5; $j <= 8; $j++){
				if($_SESSION['mred']['permisos'][$j]==true){
					if($j==5){
						$al = 2;
					}elseif($j==6){
						$al = 1;
					}elseif($j==7){
						$al = 3;
					}elseif($j==8){
						$al = 4;
					}
					if($i==0){
						$string .= "idalerta = $al ";
					}else{
						$string .= " OR idalerta = $al ";
					}
					$i = 1 ;
				}
			}
			if($string != '' ){
				$string = " ($string) AND";
			}*/
			if ($idtipoalerta == 0 && $idestado!=0){
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE estado=? AND (idTipoalerta=3 OR idTipoalerta=5 OR idTipoalerta=6 OR idTipoalerta=7 OR idTipoalerta=8) AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC; ";
				$rs		= $this->DATA->Execute($sql,array($idestado));
			} else if($idestado == 0 && $idtipoalerta == 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario  
							WHERE (idTipoalerta=3 OR idTipoalerta=5 OR idTipoalerta=6 OR idTipoalerta=7 OR idTipoalerta=8) AND (estado!=4 AND estado!=5) AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql);
			} else if($idestado == 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND (estado!=4 AND estado!=5) AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta));
			}else if($idestado != 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND estado=? AND fechaCreacion BETWEEN '$f2 00:00:00' AND '$f1 23:59:59' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta,$idestado));
			}
			
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 					= $rs->fields['idAlerta'];
					$info[$id]['latitud'] 	= $rs->fields['latitud'];
					$info[$id]['longitud'] 	= $rs->fields['longitud'];
					$datetime				= $rs->fields['fechaCreacion'];
					$diferencia = strtotime(date('Y-m-d H:i:s')) - strtotime($datetime);
					//comprobamos el tiempo que ha pasado en segundos entre las dos fechas
					//floor devuelve el n�mero entero anterior, si es 5.7 devuelve 5
					if($diferencia < 60){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia) . ' segundos';
					}else if($diferencia > 60 && $diferencia < 3600){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/60) . ' minutos';
					}else if($diferencia > 3600 && $diferencia < 86400){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/3600) . ' horas';
					}else if($diferencia > 86400 && $diferencia < 2592000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/86400) . ' d�as';
					}else if($diferencia > 2592000 && $diferencia < 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/2592000) . ' meses';
					}else if($diferencia > 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/31104000) . ' a�os';
					}else{
						$info[$id]['tiempo'] = 'Error';
					}
					$info[$id]['fechaCreacion'] = $rs->fields['fechaCreacion'];
					$info[$id]['idusuario'] 	= $rs->fields['idusuario'];
					$info[$id]['idTipoalerta'] 	= $rs->fields['idTipoalerta'];
					$info[$id]['tipoAlerta'] 	= $rs->fields['tipoAlerta'];
					$info[$id]['nombre'] 		= $rs->fields['nombre'];
					$info[$id]['correo'] 		= $rs->fields['correo'];
					$info[$id]['cluster'] 		= $rs->fields['cluster'];
					$info[$id]['poligono'] 		= $rs->fields['poligono'];
					$info[$id]['casa'] 			= $rs->fields['casa'];
					$info[$id]['estado'] 		= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function getAlertasE2($idestado,$idtipoalerta,$f1,$f2){
		try{
			/*$string = '';
			$i = 0 ;
			for ($j = 5; $j <= 8; $j++){
				if($_SESSION['mred']['permisos'][$j]==true){
					if($j==5){
						$al = 2;
					}elseif($j==6){
						$al = 1;
					}elseif($j==7){
						$al = 3;
					}elseif($j==8){
						$al = 4;
					}
					if($i==0){
						$string .= "idalerta = $al ";
					}else{
						$string .= " OR idalerta = $al ";
					}
					$i = 1 ;
				}
			}
			if($string != '' ){
				$string = " ($string) AND";
			}*/
			if ($idtipoalerta == 0 && $idestado!=0){
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE estado=? AND (idTipoalerta=3 OR idTipoalerta=5 OR idTipoalerta=6 OR idTipoalerta=7 OR idTipoalerta=8) AND fechaCreacion BETWEEN '$f1' AND '$f2' ORDER BY fechaCreacion DESC; ";
				$rs		= $this->DATA->Execute($sql,array($idestado));
			} else if($idestado == 0 && $idtipoalerta == 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario  
							WHERE (idTipoalerta=3 OR idTipoalerta=5 OR idTipoalerta=6 OR idTipoalerta=7 OR idTipoalerta=8) AND fechaCreacion BETWEEN '$f1' AND '$f2 ' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql);
			} else if($idestado == 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND fechaCreacion BETWEEN '$f1' AND '$f2' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta));
			}else if($idestado != 0 && $idtipoalerta != 0) {
				$sql 	= "SELECT idAlerta,latitud,longitud,fechaCreacion,idusuario,idTipoalerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
							FROM alertas_usuario
							WHERE idTipoalerta = ? AND estado=? AND fechaCreacion BETWEEN '$f1' AND '$f2' ORDER BY fechaCreacion DESC;";
				$rs		= $this->DATA->Execute($sql,array($idtipoalerta,$idestado));
			}
			
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 					= $rs->fields['idAlerta'];
					$info[$id]['latitud'] 	= $rs->fields['latitud'];
					$info[$id]['longitud'] 	= $rs->fields['longitud'];
					$datetime				= $rs->fields['fechaCreacion'];
					$diferencia = strtotime(date('Y-m-d H:i:s')) - strtotime($datetime);
					//comprobamos el tiempo que ha pasado en segundos entre las dos fechas
					//floor devuelve el n�mero entero anterior, si es 5.7 devuelve 5
					if($diferencia < 60){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia) . ' segundos';
					}else if($diferencia > 60 && $diferencia < 3600){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/60) . ' minutos';
					}else if($diferencia > 3600 && $diferencia < 86400){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/3600) . ' horas';
					}else if($diferencia > 86400 && $diferencia < 2592000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/86400) . ' d�as';
					}else if($diferencia > 2592000 && $diferencia < 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/2592000) . ' meses';
					}else if($diferencia > 31104000){
						$info[$id]['tiempo'] = 'Hace ' . floor($diferencia/31104000) . ' a�os';
					}else{
						$info[$id]['tiempo'] = 'Error';
					}
					$info[$id]['fechaCreacion'] = $rs->fields['fechaCreacion'];
					$info[$id]['idusuario'] 	= $rs->fields['idusuario'];
					$info[$id]['idTipoalerta'] 	= $rs->fields['idTipoalerta'];
					$info[$id]['tipoAlerta'] 	= $rs->fields['tipoAlerta'];
					$info[$id]['nombre'] 		= $rs->fields['nombre'];
					$info[$id]['correo'] 		= $rs->fields['correo'];
					$info[$id]['cluster'] 		= $rs->fields['cluster'];
					$info[$id]['poligono'] 		= $rs->fields['poligono'];
					$info[$id]['casa'] 			= $rs->fields['casa'];
					$info[$id]['estado'] 		= $rs->fields['estado'];
					$info[$id]['asunto'] 		= $rs->fields['asunto'];
					$info[$id]['descripcion'] 	= $rs->fields['descripcion'];
					$info[$id]['imagen'] 		= $rs->fields['imagen'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function getAlertasDetalle($id){
		try{
			$sql 	= "SELECT idAlerta,asunto,descripcion,imagen,tipoAlerta,nombre,correo,cluster,poligono,casa,estado
						FROM alertas_usuario
						WHERE idAlerta=?; ";
			$rs		= $this->DATA->Execute($sql,$id);
			
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$info['idAlerta'] 		= $rs->fields['idAlerta'];
					$info['asunto'] 		= $rs->fields['asunto'];
					$info['descripcion'] 	= $rs->fields['descripcion'];
					$info['imagen'] 		= $rs->fields['imagen'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function procesoAlerta($id){
		try{
			$sql = "UPDATE alertas SET estado=4 WHERE  idAlerta =?;";
				 
			$update = $this->DATA->Execute($sql, $id);
			if ($update){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function terminarAlerta($id){
		try{
			$sql = "UPDATE alertas SET estado=5 WHERE  idAlerta =?;";
				 
			$update = $this->DATA->Execute($sql, $id);
			if ($update){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
}
?>