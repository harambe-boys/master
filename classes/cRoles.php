<?php

class Rol{

	function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	
	 function getAll()
    {
        $sql = "SELECT idrol,nombre,estado,fechaCreacion FROM rol;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  	= $rs->fields['idrol'];
				$info[$id]['nombre']	= $rs->fields['nombre'];
				$info[$id]['estado']  	= $rs->fields['estado'];
				$info[$id]['fechaCreacion']  = $rs->fields['fechaCreacion'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	function getAllPermisos()
    {
        $sql = "SELECT idpermiso,nombre FROM permiso;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  	= $rs->fields['idpermiso'];
				$info[$id]['nombre']	= $rs->fields['nombre'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	function getRolPermisos($id)
    {
        $sql = "SELECT idpermiso_has_rol,idpermiso FROM permiso_has_rol WHERE idrol=?;";
        $rs = $this->DATA->Execute($sql,$id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  			= $rs->fields['idpermiso_has_rol'];
				$info[$id]['idpermiso']	= $rs->fields['idpermiso'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	function getOne($id)
    {
        $sql = "SELECT idrol,nombre,estado FROM rol WHERE idrol = ? ;";

        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
               $id                   = $rs->fields['idrol'];
				$info[$id]['nombre'] = $rs->fields['nombre'];
				$info[$id]['estado'] = $rs->fields['estado'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



	function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO rol (nombre,estado) VALUES (?,?);";
				 
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return $id;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	function nuevoPermiso($params) 
	{
		try{
		
			$sql = "INSERT INTO permiso_has_rol (idrol,idpermiso) VALUES (?,?);";
				 
			$save = $this->DATA->Execute($sql, $params);
			if ($save){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	function actualizar($params) 
	{
		try{
			$sql = "UPDATE rol SET nombre=?,estado=? WHERE idrol = ?";
			$save = $this->DATA->Execute($sql, $params);
			if ($save){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function bloqueo($id) {
        $sql = "UPDATE rol SET estado = 0 "
             . "WHERE idrol = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	
	function habilitar($id) {
        $sql = "UPDATE rol SET estado = 1 "
             . "WHERE idrol = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	function eliminar($id) {
        $sql = "delete from rol where idrol = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function eliminarPermiso($params) {
        $sql = "DELETE FROM permiso_has_rol WHERE idpermiso=? AND idrol=?;";

        $update = $this->DATA->Execute($sql,$params);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	
	
}
?>