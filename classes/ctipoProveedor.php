<?php
class tipoProveedor{
	//Constructor
	
    function __construct() 
    {
        global $DATA;
        $this->DATA = $DATA;
    }

	// get tipo proveedor one
	function getTipoProveedorOne($idProveedorcategoria){
		try{
			$sql 	= "SELECT idProveedorcategoria, nombreCategoria, imagenCategoria, estado FROM proveedorCategoria WHERE idProveedorcategoria=?;";
			
			$rs		= $this->DATA->Execute($sql, $idProveedorcategoria);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idProveedorcategoria'];
					$info[$id]['nombreCategoria']	= $rs->fields['nombreCategoria'];
					$info[$id]['imagenCategoria']	= $rs->fields['imagenCategoria'];
					$info[$id]['estado']					= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	// get tipo proveedor 
	function getTipoProveedorAll(){
		try{
			$sql 	= "SELECT idProveedorcategoria, nombreCategoria, imagenCategoria, estado FROM proveedorCategoria;";
			
			$rs		= $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ){
				while ( !$rs->EOF ) {
					$id 										= $rs->fields['idProveedorcategoria'];
					$info[$id]['nombreCategoria']	= $rs->fields['nombreCategoria'];
					$info[$id]['imagenCategoria']	= $rs->fields['imagenCategoria'];
					$info[$id]['estado']					= $rs->fields['estado'];
				
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

	function nuevo($params){
		$sql = "INSERT INTO proveedorCategoria (nombreCategoria,imagenCategoria,estado) VALUES (?,?,?);";
		$save = $this->DATA->Execute($sql, $params);
		if ($save){
			return true; 
		} else {
			return false;
		}
	}
	
	
	function modificar($params){
		$sql = "UPDATE proveedorCategoria SET nombreCategoria=?, imagenCategoria=?, estado=? "
			."WHERE idProveedorcategoria = ?";

		$update = $this->DATA->Execute($sql, $params);
		if ($update){
			return true;
		} else {
			return false;
		}
	}
	function bloqueo($idProveedorcategoria) {
        $sql = "UPDATE proveedorCategoria SET estado = 0 "
             . "WHERE idProveedorcategoria = ?";

        $update = $this->DATA->Execute($sql, $idProveedorcategoria);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idProveedorcategoria) {
        $sql = "UPDATE proveedorCategoria SET  estado = 1 "
             . "WHERE idProveedorcategoria = ?";

        $update = $this->DATA->Execute($sql, $idProveedorcategoria);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	
	
	/*
	function getUsuariosapp(){
		try{
			$sql = "SELECT up.idusuarioapp,up.nombre,up.fechanacimiento,up.correo,up.iddispositivo,p.nombre AS nombrepais,up.estado FROM usuarioapp AS up
					INNER JOIN pais AS p ON p.idpais = up.idpais ";

			$rs = $this->DATA->Execute($sql);
			if ( $rs->RecordCount() > 0 ) 
			{
				while(!$rs->EOF){
					$id                  			= $rs->fields['idusuarioapp'];
					$info[$id]['nombre'] 	 		= $rs->fields['nombre'];
					$info[$id]['fechanacimiento'] 	= $rs->fields['fechanacimiento'];
					$info[$id]['correo']  			= $rs->fields['correo'];
					$info[$id]['iddispositivo'] 	= $rs->fields['iddispositivo'];
					$info[$id]['nombrepais']  		= $rs->fields['nombrepais'];
					$info[$id]['estado']  			= $rs->fields['estado'];
					
					$rs->MoveNext(); 
				}
				$rs->Close();
				return $info;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
		
	}
	function bloqueo($idUsuario) {
        $sql = "UPDATE usuarioapp SET estado = 0 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($idUsuario) {
        $sql = "UPDATE usuarioapp SET  estado = 1 "
             . "WHERE idusuarioapp = ?";

        $update = $this->DATA->Execute($sql, $idUsuario);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	*/
}
?>