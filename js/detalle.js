var table;
var estado;
var gestion;
var fechaI;
var fechaF;
var t_element = [];
$(document).ready(function() {	
	asignarvalores();
	$("#AlertasE").addClass("select");
	
	setTimeout(function(){					
		$('.dataTables_filter ').css('top','-63px');					
	},1600);
	
	$("#datepicker1").datepicker({ 
		autoSize: true,
		appendText: '(dd-mm-yyyy)',
		dateFormat: 'dd-mm-yy',
		maxDate: $('#datepicker2').val(),
	  onClose: function( selectedDate ) {
		$( "#datepicker2" ).datepicker( "option", "minDate", selectedDate );
	  }
	});
	
	$("#datepicker2").datepicker({ 
		autoSize: true,
		appendText: '(dd-mm-yyyy)',
		dateFormat: 'dd-mm-yy',
		minDate: $('#datepicker1').val(),
		maxDate: new Date(),
	  onClose: function( selectedDate ) {
		if(selectedDate==''){
			selectedDate = new Date();
		}
		$( "#datepicker1" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	
	table = $('#data_table').dataTable({
		"sPaginationType":"full_numbers",
		"aaSorting": [],
		"order" : [] 
	});
});
	
function asignarvalores(){
	estado	= $("#estado").val();
	gestion	= $("#tipo").val();
	fechaI	= $("#datepicker1").val();
	fechaF 	= $("#datepicker2").val();
	obtenerdatos();
}

function filtrar(){
	loading('Cargando',1);
	var ta,te,f1,f2;
	ta = $("#tipo").val();
	te = $("#estado").val();
	f1 = $("#datepicker1").val();
	f2 = $("#datepicker2").val();
	
	if(f1==''){
		showWarning('Debe seleccionar fecha inicial',3000);
	}else if(f2==''){
		showWarning('Debe seleccionar fecha final',3000);
	}else{
		$("#tablegestion").html('');
		table.fnClearTable();
		asignarvalores();
	}
	setTimeout(function(){					
		$('.dataTables_filter ').css('top','-63px');
		unloading();
	},2000);
}

function obtenerdatos(){
	$.post("json/DetalleJSON.php",{
		e:estado,
		g:gestion,
		fechaI:fechaI,
		fechaF:fechaF
	}, function (data,status){
		if(status =='success'){
			if (data!='ndata'){
				data = JSON.parse(data);
				imprimirtabla(data);
			}else{
				data = 'nodata';
			}
		}else{
			data = 'erdata';
		}
		return data;
	});
}

function imprimirtabla(data){
	data.forEach(function(e){
		if(e['estado']==1){
			estado = 'Pendiente';
		}else if(e['estado']==2){
			estado = 'Aprobada';
		}else if(e['estado']==3){
			estado = 'Rechazada';
		}else if(e['estado']==4){
			estado = 'En proceso';
		}else if(e['estado']==5){
			estado = 'Terminada';
		}
		table.fnAddData ([
            e['tipoAlerta'],
            e['time'],
            e['nombre'],
            e['cluster'],
            e['poligono'],
            e['casa'],
            estado,
			cuerpodeopciones(e)
        ] );
	});
}
function cuerpodeopciones(data){
	var string;
	var form = "'detalleform'";
	var v6	 = '"'+data['tipoAlerta']+'"';
	var v7	 = '"'+data['time']+'"';
	var v8	 = '"'+data['nombre']+'"';
	var v9	 = '"'+data['correo']+'"';
	var v10	 = '"'+data['cluster']+'"';
	var v11  = '"'+data['poligono']+'"';
	var v12	 = '"'+data['casa']+'"';
	string = "<span class='tip'><button title='Ver' class='btn btn-default open-link' type='submit' onclick='sendform("+data['id']+","+data['estado']+","+data['idTipoalerta']+","+data['latitud']+","+data['longitud']+","+v6+","+v7+","+v8+","+v9+","+v10+","+v11+","+v12+")'>"+
				'<span class="fa fa-external-link fa-2x" aria-hidden="true"></span></button></span>';
	return string;
}
function sendform(v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12){
	$("#detalleform").submit( function() {
		$('<input />').attr('type', 'hidden').attr('name', "id").attr('value', v1).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "estado").attr('value', v2).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "idTipoalerta").attr('value', v3).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "lat").attr('value', v4).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "lon").attr('value', v5).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "tipoAlerta").attr('value', v6).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "time").attr('value', v7).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "nombre").attr('value', v8).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "correo").attr('value', v9).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "cluster").attr('value', v10).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "poligono").attr('value', v11).appendTo('#detalleform');
		$('<input />').attr('type', 'hidden').attr('name', "casa").attr('value', v12).appendTo('#detalleform');
		return true;
	});
}
