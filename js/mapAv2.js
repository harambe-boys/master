var map;
var centervar;
var arraycenter 	= [];
var allMarkers  	= [];
var cityCircle		= [];
//array para alertas
var arrayGA			= [];

//array para guardar mensajes
var mensajes 		= [];
//array para los markers que se muestran en el mapa
var mapMarkers		= [];
var mensajes 		= [];
var ta = 0;
var es = 0;

var updownM	= (screen.height - 100);
var leftrightM 	= screen.width;
var updown 	= $(window).height();
var leftright 	= $(window).width();
var rad = function(x) {
	return x * Math.PI / 180;
};

var getDistance = function(p1lat,p1lng, p2lat,p2lng) {
	var R = 6378137; // Earth�s mean radius in meter
	var dLat = rad(p2lat - p1lat);
	var dLong = rad(p2lng - p1lng);
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(rad(p1lat)) * Math.cos(rad(p2lat)) *
		Math.sin(dLong / 2) * Math.sin(dLong / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d; // returns the distance in meter
};

$('#map-mred').width(leftright-217);
$('#map-mred').height(updown-45);

$(window).load(function(){
	$('.content-filter').fadeIn();
	$('.content-filter').width(leftright-237);
	$('#filtro-link').on('click', function(event){
		event.preventDefault();
		$('.content-filter').toggleClass('content-filter-down');
	});
});

$(window).resize(function(){
	var updownM	= (screen.height - 100);
	var leftrightM 	= screen.width;
	var updown 	= $(window).height();
	var leftright 	= $(window).width();
	$('#map-mred').width(leftright-200);
	$('#map-mred').height(updown-45);
	
	$('.content-filter').width(leftright-220);
});

function _showMessageM(id_id){
	if(id_id == 1){
		showWarning('No se encontraron resultados.',3000);
		//$('#nmapa').attr('id','nmapa1');
		$('#nmapa').hide();
	} else if(id_id == 2){
		//$('#nmapa').attr('id','nmapa1');
		$('#nmapa').hide();
	} else {
		$('#nmapa').show();
	}
}
	
$('#filtro').on('click', function(event){
	event.preventDefault();
	ta = $('#talerta').val();
	es = $('#estado').val();
	aplicarFiltro();
});				

$('#nmapa').on('click', function(event){
	event.preventDefault();
	ta = $('#talerta').val();
	_urlMap();
	
});

function _urlMap(){
	var randomnumber = Math.floor((Math.random()*100)+1); 
	window.open('mapa_AlertasF.php'+'?ta='+ta+'',"_blank","FILTRO"+randomnumber+",directories=no,titlebar=no,toolbar=no,location=0,status=0,menubar=no,scrollbars=no,resizable=0,height="+updownM+",width="+leftrightM+"");
}
/* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */

function setMensaje(){
	$.post("json/msgA.php",{
	},function(data, status){
		if(status =='success'){
			data = data.replace(/^\s*|\s*$/g,"");
			if (data != 'ndata'){
				data = JSON.parse(data);
				data.forEach(function(element){
					mensajes.push ([parseInt(element['id']),element['msg'],parseInt(element['idA'])]);
				});
			}
		}
	});
}

function initialize() {				
	var styles = [
		{
			stylers: [
				{ hue: "#2196F3" },
				{ saturation: 120 }
			]
		},{
			featureType: "road",
			elementType: "geometry",
			stylers: [
				{ lightness: 300 },
				{ visibility: "simplified" }
			]
		},{
			featureType: "road",
			elementType: "labels",
			stylers: [
				{ visibility: "on" }
			]
		}
	];
	
	var styledMap = new google.maps.StyledMapType(styles, {name: "IMOVES"});

	var mapOptions = {
		zoom: 15,
		scrollwheel: true,
		mapTypeControlOptions: {
			mapTypeIds: [
				google.maps.MapTypeId.HYBRID,
				google.maps.MapTypeId.ROADMAP, 
				'map_style'
			]
		}
	};

	map 		= new google.maps.Map(document.getElementById('map-mred'),mapOptions);
	centervar 	= new google.maps.LatLngBounds();					
	
	setNuevoMarkers(false);
	
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
	unloading();
}

function setNuevoMarkers(s){
	if(!s){
		loading('Cargando..',1);
	}
	arraycenter = [];
	$.post("json/Av2json.php",{
		es: es,
		ta: ta
	},function(data, status){
		if(status =='success'){
			if (data!='ndata'){
				data = JSON.parse(data);
				data.forEach(function(element){
					elemento = [element['id'],element['lat'],element['lon'],element['time'],element['idAM'],element['t'],element['tiempo'],element['idE'],element['p']];
					var p = false;
					arrayGA.forEach(function(r){
						if(r[0] == element['id']){
							p = true;
							r[7] = element['idE'];
						}
					});
					if(!p){
						arrayGA.push(elemento);
					}
				});
				setArrayPushElement(arrayGA,s);
				if(!s){
					centermarker();
					unloading();
					setTimeout(function(){
						setNuevoMarkers(true);
					},15000);
				}
			}else{
				if(!s){
					showWarning('No se encontraron alertas por el momento',2000);
					centermarker();
					unloading();
					setTimeout(function(){
						setNuevoMarkers(true);
					},10000);
				}
			}
		}else{
			showError('No se pudo realizar la accion',3000);
			unloading();
			setTimeout(function(){
				setNuevoMarkers(true);
			},10000);
		}
	});	
}

function setArrayPushElement(arrayGA,s1){
	v2 = [];
	if(arrayGA.length > 0){
		arrayGA.forEach(function(e){
			var pts = [];
			var is	= [];
			var es	= [];
			if(v2.length==0){
				pts.push(e[8]);
				is.push(e[0]);
				es.push(e[7]);
				v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
			}else{
				var p = false;
				v2.forEach(function(e2){
					if((getDistance(e[1],e[2],e2[0],e2[1]))<20 && e[4]==e2[2]){
						var t1 = e[3].split(/[/ :]/);
						t1 = new Date(t1[0], t1[1]-1, t1[2], t1[3], t1[4], t1[5]);
						var t2 = e2[4].split(/[/ :]/);
						t2 = new Date(t2[0], t2[1]-1, t2[2], t2[3], t2[4], t2[5]);
						var s = Math.round((t1-t2)/1000);
						if((s/60)<20){
							e2[6].push(e[8]);
							e2[7].push(e[0]);
							e2[8].push(e[7]);
							e2[5]++;
							p = true;
						}else{
							p = false;
						}	
						
					}else{
						p = false;
					}
				});
				if(!p){
					pts = [];
					is	= [];
					es	= [];
					pts.push(e[8]);
					is.push(e[0]);
					es.push(e[7]);
					v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
				}
			}
		});
		setMarkerArraysG(v2,s1);
	}
}

function setIcon(g){
	var icon = '';
	if(g == 1){
		icon =  'images/mred/alerta_4_1.png';
	}else if(g == 2){
		icon =  'images/mred/alerta_2.png';
	}else if(g == 3){
		icon =  'images/mred/alerta_3.png';
	}else if(g == 4){
		icon =  'images/mred/alerta_1.png';
	}
	return icon;
}

function setMarkerArraysG(v,s){
	v.forEach(function(e){
		if(mapMarkers.length>0){
			p = false;
			mapMarkers.forEach(function(r){
				if(r.val == e[7][0]){
					p = true;
				}
			});
			if(!p){
				createmarker(e,s);
			}
		}else{
			createmarker(e,s);	
		}
	});
	unloading();
}

function createmarker(e,s){
	var position = new google.maps.LatLng(e[0],e[1]);
	arraycenter.push(position);
	var marker = new google.maps.Marker({
		position: position,
		map: map,
		animation: google.maps.Animation.DROP,
		icon: setIcon(e[2]),
		title: ""+e[3]+"",
		val: e[7][0]
	});
	var color;
	if(e[2]==1){
		color = '#00BED8';
	}else if(e[2]==2){
		color = '#FF9800';
	}else if(e[2]==3){
		color = '#FF3E40';
	}else if(e[2]==4){
		color = '#00B141';
	}
	var OptionsL = {
	  strokeColor: color,
	  strokeOpacity: 0.50,
	  strokeWeight: 1,
	  fillColor: color,
	  fillOpacity: 0.25,
	  map: map,
	  center: position,
	  radius: Math.sqrt(e[5])+35
	};
	
	if(e[5]>=2){
		var Circle = new google.maps.Circle(OptionsL);
		cityCircle.push(Circle);
	}
	var infowindow = new google.maps.InfoWindow({
		maxWidth: 450,
		animation: google.maps.Animation.DROP
	});
	mapMarkers.push(marker);
	google.maps.event.addListener(marker, 'click', function(event) {
		var el = setContenidoNuevo(arrayGA,e[7][0]);
		var content = attachSecretMessage(el,marker);
		infowindow.setContent(content);
		infowindow.open(marker.get('map'), marker);
		marker.setAnimation(null);
	});
	google.maps.event.addListener(marker, 'mouseout', function(event) {
		marker.setZIndex(1);
	});
	if(s){
		sonido();
	}
}

function attachSecretMessage(v,marker) {
	var content = setContenidoInfo(v,marker);
	return content;
}

function setContenidoInfo(v,marker){
	var pen 	= 0;
	var apro 	= 0;
	var rec 	= 0;
	var usu 	= [];
	var cusu	= 0;
	var temp	= '';
	v[6].forEach(function(u){
		p = false;
		if(usu.length<=0){
			usu.push(u);
			cusu++;
		}else{
			if((usu.indexOf(u))==-1){
				usu.push(u);
				cusu++;
			}
		}
	});
	v[8].forEach(function(r){
		if(r==3){
			pen++;
		}else if(r==4){
			apro++
		}else if(r==5){
			rec++;
		}
	});
	var tiempo = Transcurrido(v[4]);
	if(tiempo[1]>0){
		if(tiempo[1]>1){
			temp	 = tiempo[1] + ' horas ';
		}else{
			temp	 = tiempo[1] + ' hora ';
		}
	}
	if((parseInt(tiempo[2]))>0){
		if((parseInt(tiempo[2]))>1){
			temp = temp + tiempo[2] + ' minutos ';
		}else{
			temp = temp + tiempo[2] + ' minuto ';
		}
	}
	if((parseInt(tiempo[3]))>0){
		if((parseInt(tiempo[3]))>1){
			temp = temp + tiempo[3] + ' segundos';
		}else{
			temp = temp + tiempo[3] + ' segundo';
		}
	}
	var inputselec = '<p>Mensaje:<select  id="select'+v[7][0]+'" style="width: 250px;"><option style="width: 250px;" value="0">Seleccione</option>';
	mensajes.forEach(function(entry){
		if(entry[2]==v[2]){
			inputselec = inputselec+'<option style="width: 250px;" value="'+entry[0]+'">'+entry[1]+'</option>';
		}
	});
	var coor = marker.getPosition();
	inputselec = inputselec+'</select></p>';
	var string = '';
	if(pen>0){
		var string  = 	'<input type="button" value="Validar" class="uibutton submit_form" name="Validar" onclick="validar(['+v[7]+'],'+v[7][0]+','+coor.A+','+coor.F+')" />'+
					'<input type="button" value="Rechazar" class="uibutton submit_form" name="Rechazar" onclick="rechazar(['+v[7]+'],'+v[7][0]+','+coor.A+','+coor.F+')" />';
					//'<input type="button" value="Quitar de pantalla" class="uibutton submit_form" name="Quitar" onclick="deleteMarker('+v[7][0]+')" />';
	}
	var string2 = '';
	if(pen>0){
		string2 = '<p>Pendientes: '+pen+'</p>';
	}
	var string3 = '';
	if(apro>0){
		string3 = '<p>Aceptadas: '+apro+'</p>';
	}
	var string4 = '';
	if(rec>0){
		string4 = '<p>Rechazadas: '+rec+'</p>';
	}
	var alerta = '<div id="content-mred">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h4 id="firstHeading" class="firstHeading">'+v[3]+'</h4>'+
		'<p>Fecha y Hora: '+v[4]+'</p>'+inputselec+
		'<p>Total: '+v[5]+'</p>'+string2+''+string3+''+string4+'<p>N de usuarios que reportaron: '+cusu+'</p>'+
		'<p>Reportado hace: '+temp+'</p>'+string+'</div>';
	
	return alerta;
}

function formatString(string, len){
	if (string.length <len){
		addchar=(len - string.length);
		for (i = 0; i <addchar; i++){
			string="0"+string;
		}
	}
	if (string.length> len){
		string=substr(string,0,len);
	}
	return string;
}

function Transcurrido(t){
	// Hora de llegada
	// En mi caso uso formato "yyyy-mm-dd h:MM:ss". cada quien usa el que mas le convenga
	Hora = t; 
	// convertimos valores de cada tiempo en milisegundos
	var msecPerMinute = 1000 * 60;
	var msecPerHour = msecPerMinute * 60;
	var msecPerDay = msecPerHour * 24;

	// Convertimos hora de llegada a milisegundos
	var date = new Date(Hora);
	var dateMsec = date.getTime();

	// Convertimos hora actual a milisegundos
	var date2 = new Date();
	var dateMsec2 = date2.getTime();

	// Restamos los 2 tiempos
	var interval = dateMsec2 - dateMsec;

	// calculamos Dias, Horas, Minutos y Segundos
	var days = Math.floor(interval / msecPerDay );
	interval = interval - (days * msecPerDay );

	var hours = Math.floor(interval / msecPerHour );
	interval = interval - (hours * msecPerHour );

	var minutes = Math.floor(interval / msecPerMinute );
	interval = interval - (minutes * msecPerMinute );

	var seconds = Math.floor(interval / 1000 );
	
	// Concatenamos el resultado
	Tiempo = [days , hours , formatString(String(minutes),2), formatString(String(seconds),2)];
	
	return Tiempo;
}

function deleteMarker(markerId) {
	for (var i=0; i<mapMarkers.length; i++) {
		var v = parseInt(mapMarkers[i].val);
		if (v === markerId) {
			mapMarkers[i].setMap(null);
		}
	}
}

function aplicarFiltro(){
	arrayGA = [];
	mapMarkers.forEach(function(m){
		m.setMap(null);
	});
	mapMarkers = [];
	cityCircle.forEach(function(c){
		c.setMap(null);
	});
	cityCircle = [];
	setNuevoMarkers(true);
}

function setContenidoNuevo(arrayGA,val){
	v2 = [];
	if(arrayGA.length > 0){
		arrayGA.forEach(function(e){
			var pts = [];
			var is	= [];
			var es	= [];
			if(v2.length==0){
				pts.push(e[8]);
				is.push(e[0]);
				es.push(e[7]);
				v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
			}else{
				var p = false;
				v2.forEach(function(e2){
					if((getDistance(e[1],e[2],e2[0],e2[1]))<20 && e[4]==e2[2]){
						var t1 = e[3].split(/[/ :]/);
						t1 = new Date(t1[0], t1[1]-1, t1[2], t1[3], t1[4], t1[5]);
						var t2 = e2[4].split(/[/ :]/);
						t2 = new Date(t2[0], t2[1]-1, t2[2], t2[3], t2[4], t2[5]);
						var s = Math.round((t1-t2)/1000);
						if((s/60)<20){
							e2[6].push(e[8]);
							e2[7].push(e[0]);
							e2[8].push(e[7]);
							e2[5]++;
							p = true;
						}else{
							p = false;
						}	
						
					}else{
						p = false;
					}
				});
				if(!p){
					pts = [];
					is	= [];
					es	= [];
					pts.push(e[8]);
					is.push(e[0]);
					es.push(e[7]);
					v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
				}
			}
		});
		v2.forEach(function(i){
			i[7].forEach(function(e){
				if(e==val){
					ret = i;
				}
			});
		});
		return ret;
	}
}

function validar(v1,id,lat,lng){
	var valselect = document.getElementById('select'+id).value;
	if(valselect!=0){
		$.confirm({
			'title': "Validar Alerta",
			'message': "<strong>&iquest;Esta seguro de validar la alerta?</strong>",
			'buttons': {
				'Yes': {
					'class': 'special',
					'action': function(){
						loading('Cargando',1);
							$.post("actions/actionAlerta.php",{
								opt: "AVA",
								m: valselect,
								data: v1
							},function(data, status){
								if(status =='success'){
									if(data == 'done'){
										showSuccess('Alertas validadas',3000);
										unloading();
										aplicarFiltro();
										var position = new google.maps.LatLng(lat,lng);
										map.setCenter(position);
									}else{
										showError('Error intente de nuevo',3000);
										unloading();
									}
								}else{
									showError('No se pudo realizar la accion',3000);
									unloading();
								}
							});						
							return false;	 
					}
				},
				'No': {
					'class'	: ''
					}
			}
		});
	}else{
		showWarning('<font color=black>Seleccione el mensaje para validar la alerta</font>',3000);
	}
	
}

function rechazar(v1,lat,lng){
	$.confirm({
		'title': "Rechazar Alerta",
		'message': "<strong>&iquest;Esta seguro de rechazar la alerta?</strong>",
		'buttons': {
			'Yes': {
				'class': 'special',
				'action': function(){
					loading('Cargando',1);
						$.post("actions/actionAlerta.php",{
							opt: "ARA",
							data: v1
						},function(data, status){
							if(status =='success'){
								if(data == 'done'){
									showSuccess('Alertas rechazadas',3000);
									unloading();
									aplicarFiltro();
									var position = new google.maps.LatLng(lat,lng);
									map.setCenter(position);
								}else{
									showError('Error intente de nuevo',3000);
									unloading();
								}
							}else{
								showError('No se pudo realizar la accion',3000);
								unloading();
							}
						});						
						return false;	 
				}
			},
			'No': {
				'class'	: ''
				}
		}
	});
}

function animatemarker(){
	markersFilter2.forEach(function(val){
		if(val.ema == 3){
			val.setAnimation(google.maps.Animation.BOUNCE);
		}
	});					
}

function sonido(){
	var audio = document.createElement("audio");

	if (audio != null && audio.canPlayType && audio.canPlayType("audio/mpeg"))
	{
		audio.src = "sonido/RSG.mp3";
		audio.play();
	}
}

function centermarker(){
	if(arraycenter.length==0){
		// Try HTML5 geolocation
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				map.setCenter(pos);
			}, function() {
				handleNoGeolocation(true);
			});
		} else {
		  // Browser doesn't support Geolocation
		  handleNoGeolocation(false);
		}
	}else{
		var centervar;
		centervar 	= new google.maps.LatLngBounds();	
		arraycenter.forEach(function(n){
		   centervar.extend(n);
		});
		 map.fitBounds(centervar);
		 unloading();
	}
}

$(document).ready(function() {
	$("#AlertaM").addClass("select");
	setMensaje();
	initialize();
});
