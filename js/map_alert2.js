var map;
var centervar;
var arraycenter 	= [];
var markersFilter  	= [];
var markersFilter2  = [];
var arrFilter		= [];
var arra2Filter 	= [];
var ta;
var ea;

var updownM	= (screen.height - 100);
var leftrightM 	= screen.width;
var updown 	= $(window).height();
var leftright 	= $(window).width();
$('#mapa').width(leftright-217);
$('#mapa').height(updown-45);

$('#filtro').on('click', function(event){
	event.preventDefault();
	ta = $('#talerta').val();
	ea = $('#ealerta').val();
	loading('Cargando',1);
	setNuevoMarkers(ta,ea,1);
	setTimeout(function(){unloading();},5000);
});
				
$(window).load(function(){
	$('.content-filter').fadeIn();
	$('.content-filter').width(leftright-237);
	$('#filtro-link').on('click', function(event){
		event.preventDefault();
		$('.content-filter').toggleClass('content-filter-down');
	});
});

$(window).resize(function(){
	var updownM	= (screen.height - 100);
	var leftrightM 	= screen.width;
	var updown 	= $(window).height();
	var leftright 	= $(window).width();
	$('#mapa').width(leftright-200);
	$('#mapa').height(updown-45);
	
	$('.content-filter').width(leftright-220);
});

function initialize() {				
	// Create an array of styles.
	var styles = [
		{
			stylers: [
				{ hue: "#2196F3" },
				{ saturation: 120 }
			]
		},{
			featureType: "road",
			elementType: "geometry",
			stylers: [
				{ lightness: 300 },
				{ visibility: "simplified" }
			]
		},{
			featureType: "road",
			elementType: "labels",
			stylers: [
				{ visibility: "on" }
			]
		}
	];
	
	var styledMap = new google.maps.StyledMapType(styles);

	var mapOptions = {
		zoom: 18,
		scrollwheel: true,
		center: {lat: 13.676676, lng:  -89.219938},
		mapTypeControlOptions: {
			mapTypeIds: [
				google.maps.MapTypeId.HYBRID,
				google.maps.MapTypeId.ROADMAP, 
				'map_style'
			]
		}
	};

	map 		= new google.maps.Map(document.getElementById('mapa'),mapOptions);
	centervar 	= new google.maps.LatLngBounds();
	
	loading('Cargando',1);
	setMarkersFilter(0,0);
	
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
}

function setMarkersFilter(idtpa,ide){
	markersFilter=[];
	$.ajax({
		type: 'POST',
		url: 'json/AlertasJSON2.php?idtpa='+idtpa+'&ide='+ide,
		data: $(this).serialize(),
		success: function(data) {

			data = data.replace(/^\s*|\s*$/g,"");
			if (data!='ndata'){

				data = JSON.parse(data);
				data.forEach(function(element){
					elemento = [element['id'],element['latitud'],element['longitud'],element['time'],element['idusuario'],element['idTipoalerta'],element['tipoAlerta'],element['nombre'],element['correo'],element['cluster'],element['poligono'],element['casa'],element['estado'],element['tiempo']];
					markersFilter.push(elemento);
					arrFilter.push(parseInt(element['id'])); 
				});
				ponerMarkersFilter(markersFilter);
				_showMessageM(0);
				setTimeout(function(){unloading();},3000);
			} else if(data=='ndata'){ 
				_showMessageM(1);
				setTimeout(function(){unloading();},3000);
			}else if(data=='error'){ 
				_showMessageM(1);
				setTimeout(function(){unloading();},3000);
			}
			centermarker();
		}	
	}) 
}

function ponerMarkersFilter(markersFilter){

	for (var i = 0; i < markersFilter.length; i++) {
		var position = new google.maps.LatLng(markersFilter[i][1],markersFilter[i][2]);
		var icon = '';
		arraycenter.push(position);
		if(markersFilter[i][5]==3){
			if(markersFilter[i][12]==1){
				icon =  'images/pickers/ruido-a.png';
			}else if(markersFilter[i][12]==2){
				icon =  'images/pickers/ruido-b.png';
			}else if(markersFilter[i][12]==3){
				icon =  'images/pickers/ruido-c.png';
			}
		}else if(markersFilter[i][5]==5){
			if(markersFilter[i][12]==1){
				icon =  'images/pickers/alumbrado-a.png';
			}else if(markersFilter[i][12]==2){
				icon =  'images/pickers/alumbrado-b.png';
			}else if(markersFilter[i][12]==3){
				icon =  'images/pickers/alumbrado-c.png';
			}
		}else if(markersFilter[i][5]==6){
			if(markersFilter[i][12]==1){
				icon =  'images/pickers/fuga-a.png';
			}else if(markersFilter[i][12]==2){
				icon =  'images/pickers/fuga-b.png';
			}else if(markersFilter[i][12]==3){
				icon =  'images/pickers/fuga-c.png';
			}
		}else if(markersFilter[i][5]==7){
			if(markersFilter[i][12]==1){
				icon =  'images/pickers/basura-a.png';
			}else if(markersFilter[i][12]==2){
				icon =  'images/pickers/basura-b.png';
			}else if(markersFilter[i][12]==3){
				icon =  'images/pickers/basura-c.png';
			}
		}else if(markersFilter[i][5]==8){
			if(markersFilter[i][12]==1){
				icon =  'images/pickers/otros-a.png';
			}else if(markersFilter[i][12]==2){
				icon =  'images/pickers/otros-b.png';
			}else if(markersFilter[i][12]==3){
				icon =  'images/pickers/otros-c.png';
			}
		}
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			animation: google.maps.Animation.DROP,
			icon: icon,
			val : markersFilter[i][0],
			ta : markersFilter[i][5],
			ema: markersFilter[i][12]
		});
		markersFilter2.push(marker);
		attachSecretMessage(marker, markersFilter[i][6],markersFilter[i][0],markersFilter[i][3],markersFilter[i][5],markersFilter[i][13],markersFilter[i][12],markersFilter[i][7],markersFilter[i][9],markersFilter[i][10],markersFilter[i][11]);
		
	}
}

function attachSecretMessage(marker, msg, id, datetime,talerta,text,e,pt,clu,pol,cas) {

	inputselec = '<p>Agrege un mensaje personalizado<br></p>';
	inputselec = inputselec+'<p><textarea rows="2" cols="40" name="textcustom'+id+'" id="textcustom'+id+'"></textarea></p><p>Quiere enviar notificaci\u00F3n push <input type="checkbox" name="checkbox'+id+'" id="checkbox'+id+'" value="" /></p>';
	if(e==1 ){
		var string  = 	'<input type="button" value="Validar" class="uibutton submit_form" name="Validar" onclick="validar('+id+','+talerta+')" />'+
						'<input type="button" value="Rechazar" class="uibutton submit_form" name="Rechazar" onclick="rechazar('+id+')" />';
	}else{
		inputselec = '';
		var string = '';
	}
	var alerta = '<div id="content-mred">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h4 id="firstHeading" class="firstHeading">'+msg+'</h4>'+
		'<div id="bodyContent">'+
		'<p>Usuario: '+pt+'</p>'+
		'<p>Calle: '+clu+'</p>'+
		'<p>Pasaje: '+pol+'</p>'+
		'<p>Casa: '+cas+'</p>'+
		'<p>Fecha y Hora: '+datetime+'</p>'+inputselec+
		'<p>Tiempo: '+text+'</p>'+
		'</div>'+string+'</div>';

	
	var infowindow = new google.maps.InfoWindow({
		content: alerta,
		maxWidth: 350,
		animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(marker.get('map'), marker);
		marker.setAnimation(null);
	});
}

function validar(id,talerta){
	var textcustom = document.getElementById('textcustom'+id).value;
	
	if($("#"+"checkbox"+id).is(':checked')) {  
		var checkbox = 1;
	} else {  
		var checkbox = 0;
	}
	if( checkbox==1 && textcustom==''){
		showWarning('<font color=black>Ingrese el mensaje para validar la alerta</font>',3000);	
	}else{
		$.confirm({
			'title': "Validar Alerta",
			'message': "<strong>&iquest;Esta seguro que quiere validar la alerta? </strong>",
			'buttons': {
				'Si': {
					'class': 'special',
					'action': function(){
						loading('Procesando');
						$('#preloader').html('Procesando...');
							$.post("actions/actionAlerta.php",{
								opt: "AV",
								checkbox:checkbox,
								text: textcustom,
								talerta:talerta,
								i: id
							},function(data, status){
								if(status =='success'){
									if(data == 'done'){
										deleteMarkerFilter(id);
										setNuevoMarkers($('#talerta').val(),$('#ealerta').val(),0);
										showSuccess('Alerta Validada',5000); 
										setTimeout(function(){unloading();},2000);
									}else{
										showError('ERROR:INTENTE DE NUEVO',5000); 
										setTimeout(function(){unloading();},2000);
									}
								}else{
									showError('No se pudo realizar la accion',3000);
									setTimeout(function(){unloading();},2000);
								}
							});	
							return false;	 
					}
				},
				'No': {
					'class'	: ''
					}
			}
		});
	}
	
}

function rechazar(id){
	$.confirm({
		'title': "Rechazar Alerta",
		'message': "<strong>&iquest;Esta seguro de Rechazar la alerta?</strong>",
		'buttons': {
			'Si': {
				'class': 'special',
				'action': function(){
					loading('Procesando');
					$('#preloader').html('Procesando...');
						$.post("actions/actionAlerta.php",{
							opt: "AR",
							i: id
						},function(data, status){
							if(status =='success'){
								if(data == 'done'){
									deleteMarkerFilter(id);
									setNuevoMarkers($('#talerta').val(),$('#ealerta').val(),0);
									showSuccess('Alerta Rechazada',5000);
									setTimeout(function(){unloading();},3000);
								}else{
									showError('ERROR:INTENTE DE NUEVO',5000); 
									setTimeout(function(){unloading();},3000);
								}
							}else{
								showError('No se pudo realizar la accion',3000);
								setTimeout(function(){unloading();},3000);
							}
						});
						return false;	 
				}
			},
			'No': {
				'class'	: ''
				}
		}
	});
}
			
function deleteMarkerFilter(markerId) {
	position = arrFilter.indexOf(markerId);
	if ( ~position ) arrFilter.splice(position, 1);
	for (var i=0; i<markersFilter2.length; i++) {
		var v = parseInt(markersFilter2[i].val);
		if (v === markerId) {
			markersFilter2[i].setMap(null);
		}
	}
}

function setNuevoMarkers(idFiltro,idEstado,idMessage){
	arra2Filter = [];
	arraycenter = [];
	$.ajax({
		type: 'POST',
		url: 'json/AlertasJSON2.php?idtpa='+idFiltro+'&ide='+idEstado,
		data: $(this).serialize(),
		success: function(data) {
			data = data.replace(/^\s*|\s*$/g,"");
			if (data!='ndata'){
				data = JSON.parse(data);
				data.forEach(function(element){
					var e2 = parseInt(element['id']);
					arra2Filter.push(e2);
					position = arrFilter.indexOf(e2);
					if ( position < 0 ){
						arrFilter.push(parseInt(element['id']));
						elemento = [element['id'],element['latitud'],element['longitud'],element['time'],element['idusuario'],element['idTipoalerta'],element['tipoAlerta'],element['nombre'],element['correo'],element['cluster'],element['poligono'],element['casa'],element['estado'],element['tiempo']];
						ponerMarkers2Filter(elemento);
					}
				});
				verificarmarkersFilter(1);
			} else if(data=='ndata'){ 
				centermarker();
				_showMessageM(idMessage);
			}else if(data=='error'){ 
				centermarker();
				_showMessageM(idMessage); 
			}
		}	
	}); 
}
	
function ponerMarkers2Filter(element){
	var position = new google.maps.LatLng(element[1],element[2]);
	var icon = '';
	arraycenter.push(position);
	if(element[5]==4){
		if(element[12]==1){
			icon =  'images/pickers/ruido-a.png'; 
		}else if(element[12]==2){
			icon =  'images/pickers/ruido-b.png';
		}else if(element[12]==3){
			icon =  'images/pickers/ruido-c.png';
		}
	}else if(element[5]==5){
		if(element[12]==1){
			icon =  'images/pickers/alumbrado-a.png';
		}else if(element[12]==2){
			icon =  'images/pickers/alumbrado-b.png';
		}else if(element[12]==3){
			icon =  'images/pickers/alumbrado-c.png'; 
		}
	}else if(element[5]==6){
		if(element[12]==1){
			icon =  'images/pickers/fuga-a.png';
		}else if(element[12]==2){
			icon =  'images/pickers/fuga-b.png';
		}else if(element[12]==3){
			icon =  'images/pickers/fuga-c.png';
		}
	}else if(element[5]==7){
		if(element[12]==1){
			icon =  'images/pickers/basura-a.png';
		}else if(element[12]==2){
			icon =  'images/pickers/basura-b.png';
		}else if(element[12]==3){
			icon =  'images/pickers/basura-c.png';
		}
	}else if(element[5]==8){
		if(element[12]==1){
			icon =  'images/pickers/otros-a.png';
		}else if(element[12]==2){
			icon =  'images/pickers/otros-b.png';
		}else if(element[12]==3){
			icon =  'images/pickers/otros-c.png';
		}
	}
	var marker = new google.maps.Marker({
		position: position,
		map: map,
		animation: google.maps.Animation.DROP,
		icon: icon,
		val : element[0],
		ta : element[5],
		ema: element[12]
	});
	markersFilter2.push(marker);
	attachSecretMessage(marker, element[6],element[0],element[3],element[5],element[13],element[12],element[7]); 
}

function verificarmarkersFilter(v){
	d = [];
	arrFilter.forEach(function(element){
		var e1 = parseInt(element);
		position = arra2Filter.indexOf(e1);
		if(position<0){
			d.push(element);
		}
	});
	$.each(d,function(i,val){
		deleteMarkerFilter(val)
	});
	if(v==1){
		centermarker();
	}
}

function centermarker(){
	if(arraycenter.length==0){
		// Try HTML5 geolocation
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				map.setCenter(pos);
			}, function() {
					handleNoGeolocation(true);
			});
		} else {
		  // Browser doesn't support Geolocation
		  handleNoGeolocation(false);
		}
	}else{
		arraycenter.forEach(function(n){
		   centervar.extend(n);
		});
		map.setCenter(centervar.getCenter());
		map.fitBounds(centervar);
	}
}

function animatemarker(){
	markersFilter2.forEach(function(val){
		if(val.ema == 1){
			val.setAnimation(google.maps.Animation.BOUNCE);
		}
	});					
}

function setMarkerInterval(idFiltro,idEstado,idMessage){
	arra2Filter = [];
	$.ajax({
		type: 'POST',
		url: 'json/AlertasJSON2.php?idtpa='+idFiltro+'&ide='+idEstado,
		data: $(this).serialize(),
		success: function(data) {
			data = data.replace(/^\s*|\s*$/g,"");
			if (data!='ndata'){
				data = JSON.parse(data);
				data.forEach(function(element){
					var e2 = parseInt(element['id']);
					arra2Filter.push(e2);
					position = arrFilter.indexOf(e2);
					if ( position < 0 ){
						arrFilter.push(parseInt(element['id']));
						elemento = [element['id'],element['latitud'],element['longitud'],element['time'],element['idusuario'],element['idTipoalerta'],element['tipoAlerta'],element['nombre'],element['correo'],element['cluster'],element['poligono'],element['casa'],element['estado'],element['tiempo']];
						ponerMarkers2Filter(elemento);
						document.getElementById("mp3").play();
					}
				});
				verificarmarkersFilter(0);
				_showMessageM(0);
			} else if(data=='error'){ 
				
				_showMessageM(idMessage);
			}
		}	
	}); 
}

function _showMessageM(id_id){
	if(id_id == 1){
		showWarning('No se encontraron resultados.',3000);
		$('#alertMessage').html('No se encontraron resultados');
		markersFilter2.forEach(function(e){
			deleteMarkerFilter(e.val);
		});
	}
}