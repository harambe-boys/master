
//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function uploadAjaxIMG2(v,action){
	loading('Procesando',1);
	var nombre1= 'imagen'+v;
	
	console.log(nombre1);
	var inputFileImage1 = document.getElementById(nombre1);
	var file1 = inputFileImage1.files[0];
	
	console.log(file1);
	
	
	var data = new FormData();

	data.append('archivo',file);
	var url = "uploadprocess/"+action;
	var result = beforeSubmitIMG(v);
	
	//alert(result);
	if(result!=false){
		$.ajax({
			url:url,
			type:'POST',
			contentType:false,
			data:data,
			processData:false,
			cache:false
		}).done(function( data ){
			afterSuccessIMG(data,v);
		});
	}
	
} 
function afterSuccessIMG(responseText,v)
{
	responseText = responseText.replace(/^\s*|\s*$/g,"");
	if (responseText == 'error1'){
		$("#output"+v).html("La imagen no mayor de 135px de Ancho");
		setTimeout( "unloading()", 1000 );
	}else{
		$("#output"+v).html(responseText);
		setTimeout( "unloading()", 1000 );
	}
	$('#submit-btn'+v).show(); //hide submit button

}
function beforeSubmitIMG(v){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{

		if( !$('#imagen'+v).val()) //check empty input filed
		{
			$("#submit-btn"+v).html("No tiene seleccionada ninguna imagen!");
			unloading();
			return false
		}
		
		var fsize = $('#imagen'+v)[0].files[0].size; //get file size
		var ftype = $('#imagen'+v)[0].files[0].type; // get file type
		
		
		//allow only valid image file types 
		switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output"+v).html("<b>"+ftype+"</b> Archivo no soportado!");
				unloading();
				return false
        }
		
		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576) 
		{
			$("#output"+v).html("<b>"+bytesToSize(fsize) +"</b> Imagen muy pesada!");
			unloading();
			return false
		}
				
		$('#submit-btn'+v).hide(); //hide submit button
		$("#output"+v).html(""); 
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output"+v).html("Actualiza tu navegador para soportar la subida de imagenes!");
		unloading();
		return false;
	}
}