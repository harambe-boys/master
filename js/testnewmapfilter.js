				var map;
				var centervar;
				var arraycenter 	= [];
				var allMarkers  	= [];
				//array para separar tipos de gestiones
				var arrayG1			= [];
				var arrayG2			= [];
				var arrayG3			= [];
				var arrayG4			= [];
				
				//array para agrupar gestiones por distancia
				var arrayG1G		= [];
				var arrayG2G		= [];
				var arrayG3G		= [];
				var arrayG4G		= [];
				//array para guardar mensajes
				var mensajes 		= [];
				//array para los markers que se muestran en el mapa
				var mapMarkers		= [];
				
				
				var arrFilter		= [];
				var arra2Filter 	= [];
				var mensajes 		= [];
				var ta;
				var ea;
				var updownM	= (screen.height - 100);
				var leftrightM 	= screen.width;
				var updown 	= $(window).height();
				var leftright 	= $(window).width();
				var rad = function(x) {
					return x * Math.PI / 180;
				};
				var getDistance = function(p1lat,p1lng, p2lat,p2lng) {
					var R = 6378137; // Earth�s mean radius in meter
					var dLat = rad(p2lat - p1lat);
					var dLong = rad(p2lng - p1lng);
					var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
						Math.cos(rad(p1lat)) * Math.cos(rad(p2lat)) *
						Math.sin(dLong / 2) * Math.sin(dLong / 2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					var d = R * c;
					return d; // returns the distance in meter
				};
				$('#map-mred').width(leftright-217);
				$('#map-mred').height(updown-45);
			
				$(window).load(function(){
					$('.content-filter').fadeIn();
					$('.content-filter').width(leftright-237);
					$('#filtro-link').on('click', function(event){
						event.preventDefault();
						$('.content-filter').toggleClass('content-filter-down');
					});
				});
				
				$(window).resize(function(){
					var updownM	= (screen.height - 100);
					var leftrightM 	= screen.width;
					var updown 	= $(window).height();
					var leftright 	= $(window).width();
					$('#map-mred').width(leftright-200);
					$('#map-mred').height(updown-45);
					
					$('.content-filter').width(leftright-220);
				});
				
				function _showMessageM(id_id){
					if(id_id == 1){
						showWarning('No se encontraron resultados.',3000);
						//$('#nmapa').attr('id','nmapa1');
						$('#nmapa').hide();
					} else if(id_id == 2){
						//$('#nmapa').attr('id','nmapa1');
						$('#nmapa').hide();
					} else {
						$('#nmapa').show();
					}
				}
					
				$('#filtro').on('click', function(event){
					event.preventDefault();
					ta = $('#talerta').val();
					ea = $('#ealerta').val();
					setNuevoMarkers(ta,ea);
				});				
				
				$('#nmapa').on('click', function(event){
					event.preventDefault();
					talerta = $('#talerta').val();
					ealerta = $('#ealerta').val();
					
					_urlMap();
					
				});
				
				function _urlMap(){
					var randomnumber = Math.floor((Math.random()*100)+1); 
					window.open('gestion_municipal_mapa.php'+'?talerta='+talerta+'&ealerta='+ealerta+'',"_blank","FILTRO"+randomnumber+",directories=no,titlebar=no,toolbar=no,location=0,status=0,menubar=no,scrollbars=no,resizable=0,height="+updownM+",width="+leftrightM+"");
				}
				/* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
				
				function setMensaje(){
					$.ajax({
						type: 'POST',
						url: 'json/msgG.php',
						data: $(this).serialize(),
						success: function(data) {
							data = data.replace(/^\s*|\s*$/g,"");
							if (data != 'ndata'){
								data = JSON.parse(data);
								data.forEach(function(element){
									mensajes.push ([parseInt(element['id']),element['msg'],parseInt(element['idG'])]);
								});
							}
						}
					});
					
				}
				
				function initialize() {				
					var styles = [
						{
							stylers: [
								{ hue: "#2196F3" },
								{ saturation: 120 }
							]
						},{
							featureType: "road",
							elementType: "geometry",
							stylers: [
								{ lightness: 300 },
								{ visibility: "simplified" }
							]
						},{
							featureType: "road",
							elementType: "labels",
							stylers: [
								{ visibility: "on" }
							]
						}
					];
					
					var styledMap = new google.maps.StyledMapType(styles, {name: "IMOVES"});
				
					var mapOptions = {
						zoom: 15,
						scrollwheel: true,
						mapTypeControlOptions: {
							mapTypeIds: [
								google.maps.MapTypeId.HYBRID,
								google.maps.MapTypeId.ROADMAP, 
								'map_style'
							]
						}
					};

					map 		= new google.maps.Map(document.getElementById('map-mred'),mapOptions);
					centervar 	= new google.maps.LatLngBounds();					
					
					setNuevoMarkers();
					
					map.mapTypes.set('map_style', styledMap);
					map.setMapTypeId('map_style');
					unloading();
				}
				
				function setNuevoMarkers(){
					loading('Cargando..',1);
					arraycenter = [];
					$.ajax({
						type: 'POST',
						url: 'json/testjson.php',
						data: $(this).serialize(),
						success: function(data) {
							data = data.replace(/^\s*|\s*$/g,"");
							if (data!='ndata'){
								data = JSON.parse(data);
								data.forEach(function(element){
									elemento = [element['id'],element['lat'],element['lon'],element['time'],element['idGM'],element['t'],element['tiempo'],element['idE'],element['p']];
									if(element['idGM']==1){
										arrayG1.push(elemento);
									}else if(element['idGM']==2){
										arrayG2.push(elemento);
									}else if(element['idGM']==3){
										arrayG3.push(elemento);
									}else if(element['idGM']==4){
										arrayG4.push(elemento);
									}
								});
								setArraysMarker();
							}
						}	
					});
				}
				
				function setArraysMarker(){
					setArrayPushElement(arrayG1,arrayG1G);
					setArrayPushElement(arrayG2,arrayG2G);
					setArrayPushElement(arrayG3,arrayG3G);
					setArrayPushElement(arrayG4,arrayG4G);
					centermarker();
				}
				
				function setArrayPushElement(v1,v2){
					if(v1.length > 0){
						v1.forEach(function(e){
							var pts = [];
							var is	= [];
							var es	= [];
							if(v2.length==0){
								pts.push(e[8]);
								is.push(e[0]);
								es.push(e[7]);
								v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
								
							}else{
								var p = false;
								v2.forEach(function(e2){
									if((getDistance(e[1],e[2],e2[0],e2[1]))<20){
										var t1 = e[3].split(/[- :]/);
										t1 = new Date(t1[0], t1[1]-1, t1[2], t1[3], t1[4], t1[5]);
										var t2 = e2[4].split(/[- :]/);
										t2 = new Date(t2[0], t2[1]-1, t2[2], t2[3], t2[4], t2[5]);
										var s = Math.round((t1-t2)/1000);
										if((s/60)<20){
											e2[6].push(e[8]);
											e2[7].push(e[0]);
											e2[8].push(e[7]);
											e2[5]++;
											p = true;
										}else{
											p = false;
										}	
										
									}else{
										p = false;
									}
								});
								if(!p){
									pts = [];
									is	= [];
									es	= [];
									pts.push(e[8]);
									is.push(e[0]);
									es.push(e[7]);
									v2.push([e[1],e[2],e[4],e[5],e[3],1,pts,is,es]);
								}
							}
						});
						setMarkerArraysG(v2);
					}
				}
				
				function setIcon(g){
					var icon = '';
					if(g == 1){
						icon =  'images/mred/GM/B1.png';
					}else if(g == 2){
						icon =  'images/mred/GM/A2.png';
					}else if(g == 3){
						icon =  'images/mred/GM/C3.png';
					}else if(g == 4){
						icon =  'images/mred/GM/O4.png';
					}
					return icon;
				}
				
				function setMarkerArraysG(v){
					v.forEach(function(e){
						var position = new google.maps.LatLng(e[0],e[1]);
						arraycenter.push(position);
						var marker = new google.maps.Marker({
							position: position,
							map: map,
							animation: google.maps.Animation.DROP,
							icon: setIcon(e[2]),
							title: ""+e[5]+""
						});
						var color;
						if(e[2]==1){
							color = '#FF3E40';
						}else if(e[2]==2){
							color = '#00B141';
						}else if(e[2]==3){
							color = '#FF9800';
						}else if(e[2]==4){
							color = '#00BED8';
						}
						var OptionsL = {
						  strokeColor: color,
						  strokeOpacity: 0.50,
						  strokeWeight: 1,
						  fillColor: color,
						  fillOpacity: 0.25,
						  map: map,
						  center: position,
						  radius: Math.sqrt(e[5])+35
						};
						attachSecretMessage(marker,e);
						cityCircle = new google.maps.Circle(OptionsL);
						mapMarkers.push(marker);
						google.maps.event.addListener(marker, 'mouseout', function() {
							marker.setZIndex(1);
						});
					});
				}
				
				function attachSecretMessage(marker,v) {
					console.log(v);
					var pen 	= 0;
					var apro 	= 0;
					var rec 	= 0;
					var usu 	= [];
					var cusu	= 0;
					v[6].forEach(function(u){
						var p = false;
						if(usu.length<=0){
							usu.push(u);
							cusu++;
						}else{
							if((usu.indexOf(u))==-1){
								usu.push(u);
								cusu++;
							}
						}
					});
					
					v[7].forEach(function(r){
						if(r==3){
							pen++;
						}else if(r==4){
							apro++
						}else if(r==5){
							rec++;
						}
					});
					var inputselec = '<p>Mensaje:<select  id="select'+v[7][0]+'" style="width: 250px;"><option style="width: 250px;" value="0">Seleccione</option>';
					mensajes.forEach(function(entry){ 
						if(entry[2]==v[2]){
							inputselec = inputselec+'<option style="width: 250px;" value="'+entry[0]+'">'+entry[1]+'</option>';
						}
					});
					inputselec = inputselec+'</select></p>';
					
					var string  = 	'<input type="button" value="Validar" class="uibutton submit_form" name="Validar" onclick="validar('+v+')" />'+
									'<input type="button" value="Rechazar" class="uibutton submit_form" name="Rechazar" onclick="rechazar('+v+')" />';
					
					var alerta = '<div id="content-mred">'+
						'<div id="siteNotice">'+
						'</div>'+
						'<h4 id="firstHeading" class="firstHeading">'+v[3]+'</h4>'+
						'<p>Fecha y Hora: '+v[4]+'</p>'+inputselec+
						'<p>Total: '+v[5]+'</p><p>Aceptadas: '+apro+'</p><p>Pendientes: '+pen+'</p><p>N de usuarios que reportaron: '+cusu+'</p>'+string+'</div>';
				
					
					var infowindow = new google.maps.InfoWindow({
						content: alerta,
						maxWidth: 350,
						animation: google.maps.Animation.DROP
					});

					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(marker.get('map'), marker);
						marker.setAnimation(null);
					});
				}
				
				function deleteMarkerFilter(markerId) {
					position = arrFilter.indexOf(markerId);
					if ( ~position ) arrFilter.splice(position, 1);
					for (var i=0; i<markersFilter2.length; i++) {
						var v = parseInt(markersFilter2[i].val);
						if (v === markerId) {
							markersFilter2[i].setMap(null);
						}
					}
				}
				
				
				function verificarmarkersFilter(v){
					d = [];
					arrFilter.forEach(function(element){
						var e1 = parseInt(element);
						position = arra2Filter.indexOf(e1);
						if(position<0){
							d.push(element);
						}
					});
					$.each(d,function(i,val){
						deleteMarkerFilter(val)
					});
					if(v==1){
						centermarker();
					}
				}
				
				function animatemarker(){
					markersFilter2.forEach(function(val){
						if(val.ema == 3){
							val.setAnimation(google.maps.Animation.BOUNCE);
						}
					});					
				}
				
				function sonido(){
					var audio = document.createElement("audio");

					if (audio != null && audio.canPlayType && audio.canPlayType("audio/mpeg"))
					{
						audio.src = "sonido/RSG.mp3";
						audio.play();
					}
				}
				
				function centermarker(){
					if(arraycenter.length==0){
						// Try HTML5 geolocation
						if(navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
								map.setCenter(pos);
							}, function() {
								handleNoGeolocation(true);
							});
						} else {
						  // Browser doesn't support Geolocation
						  handleNoGeolocation(false);
						}
					}else{
						arraycenter.forEach(function(n){
						   centervar.extend(n);
						});
						 map.fitBounds(centervar);
					}
				}
				
				$(document).ready(function() {
					$("#GestionM").addClass("select");
					setMensaje();
					initialize();
					/*setInterval(function(){
						setMarkerInterval(ta,ea);	
						animatemarker();
					},10000);*/
				});
				