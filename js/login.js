	$(document).ready(function () {	  
		$('#login').show().animate({   opacity: 1 }, 2000);
		$('.shadow').hide();
		$('#login').css('border','0');
		$('.logo').show().animate({   opacity: 1,top: '40%'}, 800,function(){			
			$('.logo').show().delay(1200).animate({   opacity: 1,top: '9%' }, 300,function(){
				$('.formLogin').animate({   opacity: 1,left: '0' }, 300);
				$('.userbox').animate({ opacity: 0 }, 200).hide();
				$('.shadow').fadeIn( 6000 );
				$('#login').css('border','5px #FFF solid');
			});		
		})	
		$(".on_off_checkbox").iphoneStyle();
		$('.tip a ').tipsy({gravity: 'sw'});
		$('.tip input').tipsy({ trigger: 'focus', gravity: 'w' });
	});

	$('.userload').click(function(e){
		$('.formLogin').animate({   opacity: 1,left: '0' }, 300);			    
		  $('.userbox').animate({ opacity: 0 }, 200,function(){
			  $('.userbox').hide();				
		   });
	});
	    
	$('#formLogin').submit(function(e){				
		if($('#username').val() == "" || $('#password').val() == ""){
			showError("Ingrese usuario / Password");
			$('.inner').jrumble({ x: 4,y: 0,rotation: 0 });	
			$('.inner').trigger('startRumble');
			setTimeout('$(".inner").trigger("stopRumble")',500);
			setTimeout('hideTop()',5000);
			return false;
		} else {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					data = data.replace(/^\s*|\s*$/g,"");
					if (data == 'done'){
						hideTop();
						loading('Validando',1);		
						setTimeout( "unloading()", 2000 );
						setTimeout( "Login()", 2500 );
					} else  {
						showError('Usuario / Password no validos'); 
						$('.inner').jrumble({ x: 4,y: 0,rotation: 0 });	
						$('.inner').trigger('startRumble');
						setTimeout('$(".inner").trigger("stopRumble")',500);
						setTimeout('hideTop()',5000);
						return false;
					}
				}	
			}) 
			return false;
		}
	});	

	function Login(){
		$("#login").animate({   opacity: 1,top: '49%' }, 200,function(){
			$('.userbox').show().animate({ opacity: 1 }, 500);
			$("#login").animate({   opacity: 0,top: '60%' }, 500,function(){
				$(this).fadeOut(200,function(){
					$(".text_success").slideDown();
					$("#successLogin").animate({opacity: 1,height: "200px"},500);   			     
				});							  
			})	
		})	
		setTimeout( "window.location.href='index.php'", 3000 );
	}

	$('#alertMessage').click(function(){
		hideTop();
	});

	function showError(str){
		$('#alertMessage').addClass('error').html(str).stop(true,true).show().animate({ opacity: 1,right: '10'}, 500);	
		
	}

	function showSuccess(str){
		$('#alertMessage').removeClass('error').html(str).stop(true,true).show().animate({ opacity: 1,right: '10'}, 500);	
	}

	function hideTop(){
		$('#alertMessage').animate({ opacity: 0,right: '-20'}, 500,function(){ $(this).hide(); });	
	}	
	
	function loading(name,overlay) {  
		$('body').append('<div id="overlay"></div><div id="preloader">'+name+'..</div>');
			if(overlay==1){
				$('#overlay').css('opacity',0.1).fadeIn(function(){  $('#preloader').fadeIn();	});
				return  false;
			}
		$('#preloader').fadeIn();	  
	}
	
	function unloading() {  
		$('#preloader').fadeOut('fast',function(){ $('#overlay').fadeOut(); });
	}
