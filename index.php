<?php
	require_once 'data/dataBase.php';
	require_once 'classes/cUsuario.php';
	require_once 'classes/cPush.php';
	
	$oUser 		= new Usuario();
	$oPush 		= new Push();

	if ( !$oUser->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	$nombreCurrent = $_SESSION['Altamira']['user'];
	
	$idCluster = '';
	$idPoligono = '';
	
	$vCluster = $oPush ->getCluster();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<link rel="shortcut icon" type="image/ico" href="../images/lomas_altamira.ico" /> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Lomas de Altamira</title>
        <?php
			include_once "cssyjscomun.php";
		?>
        <script type="text/javascript">	
			$(document).ready(function() {
				getPoli();
				$("#1m").addClass("select");
				
				
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    true,
					resetForm:    true
				};
				var options2 = {
					target:       '#alertMessage',
					beforeSubmit: validate2,
					success:      successful2,
					clearForm:    false,
					resetForm:    false
				};

				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#form2').submit(function() {
					$(this).ajaxSubmit(options2);
					return false;
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				if (!form.t.value) {
					msg = 'Necesita ingresar el titulo para la notificacion';
					is_error = true;
				}else if (!form.m.value) {
					msg = 'Necesita ingresar el mensaje para la notificacion';
					is_error = true;
				}
				
				if (is_error == true) {
					showWarning('<font color=black>'+msg+'</font>',7000);
					return false;
				} else {
					loading('Procesando',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					msg = "Push Enviada Exitosamente.";
					setTimeout( "showSuccess(msg,5000);", 2000 );
					setTimeout( "unloading()", 3000 );
				} else  {
					msg = "ERROR. No se pudo enviar ninguna Notificacion.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
				} 
			}
			
			function validate2(){
				var form2     = document.form2;
				var is_error = false;
				var msg      = 'Ocurrio un error!';
				
				if (!form2.tI.value) {
					msg = 'Necesita ingresar el titulo de usuario.';
					is_error = true;
				}else if (!form2.mI.value) {
					msg = 'Necesita ingresar el mensaje del usuario';
					is_error = true;
				}
				
				
				if (is_error == true) {
					showWarning('<font color=black>'+msg+'</font>',7000);
					return false;
				} else {
					loading('Procesando');
				}	
			}
		  
			function successful2(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					msg = "Mensaje al usuario enviado correctamente.";
					setTimeout( "showSuccess(msg,5000);", 2000 );
					setTimeout( "unloading()", 3000 );
				} else  {
					msg = "ERROR. No se pudo enviar el mensaje al usuario.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
				} 
				
				
			}
			
			function getPoli(){
				$("#poligono").remove();
				html = "<select type = 'hidden' name='poligono' id='poligono' class='chzn-select'>"+
							"<option value='' >Todos</option>"+
						"</select>";	
				$("#poli_div").append(html);
				var cluster = $("#cluster").find("option:selected").val();
			
				//console.log(cluster);
				$.post("json/poligono.php", {
					cluster:cluster
				}, function (data, status) {
					if (status == 'success') {
						//alert(data);
						data = data.replace(/^\s*|\s*$/g, "");
						
							if(data === "ndata"){
								//alert('NO hay datos');
							}else{
								dataJ = JSON.parse(data);
								
								
									$.each(dataJ, function(is, items) {
											
								    	$("#poligono").append($('<option>', {
												value: items.idpoligono,
												text: items.eldato
											}));	
									});
									$("#poligono").selectmenu({
												style: 'dropdown',
												transferClasses: true,
												width: null
											});
							}
					}
				});
				
				
				
				
			}
			
			
		/*function send_notificacionG(){
			var msg = $("#n_global").val();
			if(msg!=''){
				$.confirm({
					'title': "Envio de notificaciones globales",
					'message': "<strong>Desea enviar la notificacion en este momento.</strong>",
					'buttons': {
						'Si': {
							'class': 'special',
							'action': function(){
								loading('Procesando',1);
								$.post("notificaciones/pushglobal.php",{
									msg:msg
								},function(data, status){
									if(status =='success'){
										data = data.replace(/^\s*|\s*$/g,"");
										if (data == 'n_session'){
											location.reload(true);
										}else if (data == 'n_msg'){
											mg = "Ingrese el mensaje de la notificacion.";
											showWarning(mg,5000);
										}else if (data == 'no_send'){
											mg = "No se pudo enviar la notificacion.";
											showWarning(mg,5000);
										}else if (data == 'error_msg'){
											mg = "Error. Intente mas tarde o contacte al soporte tecnico.";
											showError(mg,5000);
										}else if (data == 'done'){
											mg = "Notificacion enviada correctamente.";
											showSuccess(mg,5000);
										}
									}
								});		
								setTimeout('unloading();',3000);		
								return false;
							}
						},
						'No': {
							'class'	: ''
							}
					}
				});
			}else{
				mg = "Ingrese el mensaje de la notificacion.";
				showWarning(mg,5000);
			}
		}*/
		</script>	

		<?php
			$cluster = "<script> document.write(d)</script>";
			
			//echo($cluster);
			?>		
    </head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>
          
            
		<div id="content">
			<div class="inner">
				<div class="topcolumn" style="display:none;">
					<div class="logo" style="margin-bottom: 25px;"></div>
				</div>
				<div class="clear"></div>
				<!--// two column window -->
				<div style="z-index: 10; position: relative;left: 35%;padding: 20px 0 20px 0;" ><img src=""></img></div>
                    
				<div style="color:#3484C2; text-shadow: 1px 1px #bababa; width:100%; margin:0px auto; height:50em; position:relative;" >
                    <div style="display: block;position: reliative;top: 0;left: 0;right: 0;" >
                        <p style="z-index:10; font-size:36px; font-weight:bold; text-align:center;" >Bienvenido<p>            
                        <p style="z-index:10; font-size:22px; text-align:center;" >Administrador de alertas y gestiones de la Aplicaci&oacute;n de Lomas de Altamira.</p>
                    </div>
				<?php 
					if($_SESSION['Altamira']['permisos'][15]){
				?>
					<div class=""  style="width: 100%;text-shadow: none;">
						<div class="load_page">
							<div class="formEl_b">
								<form id="form" action="notificaciones/pushglobal.php" method="post" name="form"> 
									<fieldset>
										<legend>Env&iacute;o de Notificaciones Push Globales</legend>
										<div class="section ">
											<label style="width: 100%;"> Titulo <small>Ingresa el titulo de la notificaci&oacute;n</small></label>   
											
											<div style="z-index: 12;margin-left: 0;"> 
												<input type="text"  name="t" id="t" maxlength="30" class="validate[required]  large"/>
												<label><small>M&aacute;ximo de 30 caracteres</small></label>
											</div>
										</div>
										<div class="section ">
											<label style="width: 100%;"> Notificaci&oacute;n <small>Ingresa el mensaje que quiera mandar a todas las personas de Lomas de Altamira</small></label>   
											
											<div style="z-index: 12;margin-left: 0;"> 
												<input type="text"  name="m" id="m" maxlength="80" class="validate[required]  large"/>
												<label><small>M&aacute;ximo de 80 caracteres</small></label>
											</div>
										</div>
										<div class="section last">
											<div style="margin-left:0;">
												<input type="submit" value="ENVIAR" class="uibutton submit_form" form="form" style="z-index: 12;"/>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					
				<?php 
				}
				if($_SESSION['Altamira']['permisos'][15]){
				?>
					<div class=""  style="width: 100%;text-shadow: none;">
						<div class="load_page">
							<div class="formEl_b">
								<form id="form2" action="notificaciones/pushUsuario.php" method="post" name="form2"> 
									<fieldset>
										<legend>Env&iacute;o de mensajes a Usuarios Espec&iacute;ficos</legend>
										<label style="text-transform: none;">Secci&oacute;n para enviar mensajes todos los usuarios de Lomas de Altamira</label>
										<br>
										<div>
											<br>
											<br>
											<br>
											<label class="pUser"> Calle</label>  
												<div> 
													<select name="cluster" id="cluster" onchange="getPoli()" class="chzn-select">
														<?php
															if($vCluster){
																foreach ($vCluster AS $id => $arrCluster) {
																	if($idCluster == $id){
																		$selected = "selected = 'selected'";
														?>
														<option value="<?=$id;?>"  <?=$selected?>><?=$arrCluster['cluster'];?></option>
														<?php
																	} else {
														?>
														<option value="<?=$id;?>"><?=$arrCluster['cluster'];?></option>
														<?php
																	}
																}
															}
														?>
													</select>
													
												</div>
										</div>
										<div>
											<br>
											<br>
											<br>
											<label class="pUser" style="display: none"> Pasaje</label>
												<div id="poli_div" style="display: none">
												</div>
										</div>
										<div>
										<div class="section ">
											<label style="width: 100%;"> Titulo</label>   
											<div style="z-index: 12;margin-left: 0;"> 
												<input type="text"  name="tI" id="tI" maxlength="150" class="validate[required]  large"/>
											</div>
										</div>
										<div class="section ">
											<label style="width: 100%;"> Mensaje<small>Ingresa el mensaje que quiera mandar a las personas de Lomas de Altamira</small></label>   
											<div style="z-index: 12;margin-left: 0;"> 
												<input type="text"  name="mI" id="mI" maxlength="150" class="validate[required]  large"/>
												<label><small>M&aacute;ximo de 150 caracteres</small></label>
											</div>
										</div>
										<div class="section last">
											<div style="margin-left:0;">
												<input type="submit" value="ENVIAR" class="uibutton submit_form" form="form2" style="z-index: 12;"/>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
                </div>
				<?php 
				}
				?>
				
                <div class="column_left">
				</div>
				<div class="column_right" >
				</div>
				<div class="clear"></div>

				<?php
					include_once "footer.php";
				?>
			</div> <!--// End inner -->
		</div> <!--// End content --> 
	</body>
</html>