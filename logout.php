<?php
session_start();
$_SESSION = array();
$session_name = session_name();

unset($_SESSION['mred']['id']);
unset($_SESSION['mred']['user']);
unset($_SESSION['mred']['status']);
unset($_SESSION['mred']['municipio']);

session_destroy();

header("Location: login.php");
exit();
?>
