<?php
	//Data
	include_once "data/dataBase.php";
	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cAtencionU.php";
	
	$oUsuario 		= new Usuario();
	$oAtencion 	= new Atencion();

	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	  if (!$_SESSION['Altamira']['permisos'][6]) {
		header("Location: index.php");
		exit();
	}
	
	$vAtenciones 	= $oAtencion->getAtencionAll();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="shortcut icon" type="image/ico" href="../images/lomas_altamira.ico" /> 
        <title>Atenci&oacute;n de Usuario</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#atencionU").addClass("select");
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers",
					"aaSorting": []
				});
			});
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><span class="ico fa fa-users fa-2x"></span>Atenci&oacute;n de Usuario  </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >
								<li id="hi1"><a href="#tab1"  id="3"  >  Lista de Atenci&oacute;n de Usuario</a></li>  
							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Nombre del Usuario</th>
														<th>Asunto</th>
														<th>Descripci&oacute;n</th>
														<th>Fecha de Creaci&oacute;n</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vAtenciones){
														foreach ($vAtenciones AS $id => $arrAtencion) {
													?>
														<tr>
															<td ><?=$arrAtencion['nombre'];?></td>
															<td><?=$arrAtencion['asunto'];?></td>
															<td><?=$arrAtencion['descripcion'];?></td>			
															<td ><?=$arrAtencion['fechaCreacion'];?></td>
															<td>
																<span class="tip" >
																	<a title="Detalle" href="detalle_atencion.php?id=<?=$id;?>">
																		<img src="images/icon/color_18/checkmark2.png" >
																	</a>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>