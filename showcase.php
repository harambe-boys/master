
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lomas de Altamira</title>

    <!-- Bootstrap -->
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
	<img src="http://52.23.92.102/Altamira/images/Altamira 1024x1024.png" class="img-responsive center-block" style="width:70%;">	
	<div class="container-fluid">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
		  <h5 class="panel-title">
			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			REGLAMENTO INTERNO DE LA ASOCIACIÓN DE VECINOS DE LOMAS DE ALTAMIRA.

			</a>
		  </h5>
		</div>
		<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		  <div class="panel-body">
			Con el propósito de lograr un ambiente de seguridad, privacidad y tranquilidad que beneficie a todos los vecinos y como residencial de carácter privado y exclusivo, se establece el siguiente Reglamento para la convivencia armoniosa dentro de la misma.
			<br>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			  ART&Iacute;CULO UNO:   
			</a>
		  </h5>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		  <div class="panel-body center-block">
			En este reglamento se entiende por vecino toda persona que habite inmueble dentro de la Colonia Lomas de Altamira.
			<br>
		  </div>
		</div>
	  </div>
	 <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			  ART&Iacute;CULO DOS:
			</a>
		  </h5>
		</div>
		<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
		  <div class="panel-body">
				La naturaleza de La Colonia Lomas de Altamira es de carácter privado y de uso exclusivo  habitacional, por lo que no se permite la instalación de negocios de ninguna índole, ni el uso de las viviendas para otros fines que los de residir.
			<br>
		  </div>
		</div>
	  </div>
	 <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingFour">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			  ART&Iacute;CULO TRES:
			</a>
		  </h5>
		</div>
		<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
		  <div class="panel-body">
			Todo Vecino tiene derecho a:
			<ul>
					<li>a)	Conocer los Estatutos y el reglamento que legalmente rigen a la Asociación.</li>
					<li>b)	Que sus opiniones, críticas y quejas sobre cualquier situación dentro de la colonia sean escuchadas con el objeto de que contribuyan al mejoramiento de la misma. </li>
					<li>c)	Participar en el desarrollo de planes, proyectos o actividades sociales que se realicen por parte de la comunidad. </li>
					<li>d)	Solicitar información sobre el uso y estado de los fondos de la Asociación, avances de obras y actividades que se realicen dentro de la colonia.</li>
					<li>e)	Que se respete su privacidad y tranquilidad dentro de la colonia. </li>
					<li>f)	Hacer uso y disfrutar del parque recreativo de la colonia y sus instalaciones. </li>
					<li>g)	Ser auxiliado y protegido por el servicio de seguridad en cualquier evento delictivo o accidental ocurrido dentro de la colonia. </li>
			</ul>
			<br>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingFive">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			  ART&Iacute;CULO CUATRO:
			</a>
		  </h5>
		</div>
		<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
		  <div class="panel-body">
			Toda la desavenencia o malestar de un vecino podrá reportarlo por escrito a la Junta Directiva para estudiar su caso y tomar las providencias correspondientes.
			<br>
		  </div>
		</div>
	  </div>
	 <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingSix">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
			  ART&Iacute;CULO CINCO:
			</a>
		  </h5>
		</div>
		<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
		  <div class="panel-body">
			Todo vecino debe cumplir con  los Estatutos de la Asociación este reglamento interno y cualquier otra regulación que sea aprobada en Asamblea General.
			<br>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingSeven">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
			   ART&Iacute;CULO SEIS:
			</a>
		  </h5>
		</div>
		<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
		  <div class="panel-body">
		  Para ingresar a la colonia todo vecino o visitante debe identificarse.
			<ul>
				<li>a)	Para facilidad y comodidad de los residentes, la Junta Directiva proporcionará un distintivo (calcomanía) que deberá colocarse en su vehículo para agilizar su ingreso. Este distintivo podrá ser cambiado periódicamente para garantizar la privacidad y seguridad de los residentes.</li>
				<li>b)	Los Visitantes deben indicar el nombre y la dirección de la persona que visitan y dejar  un documento con fotografía al vigilante, previo a su ingreso. </li>
			</ul> 
			<br>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingEight">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
			   ART&Iacute;CULO SIETE:
			</a>
		  </h5>
		</div>
		<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
		  <div class="panel-body">
			 La velocidad vehicular máxima es de 30 km/h dentro de la colonia.
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingNine">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO OCHO:
			</a>
		  </h5>
		</div>
		<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
		  <div class="panel-body">
				Se debe evitar ruidos excesivos de los vehículos producidos por escapes abiertos y sonidos de claxon estridentes usados de  forma insistente. 
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO NUEVE:
			</a>
		  </h5>
		</div>
		<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
		  <div class="panel-body">
				No se permite el ingreso de vehículos comerciales con altoparlantes.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingEleven">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DIEZ:
			</a>
		  </h5>
		</div>
		<div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
		  <div class="panel-body">
				Se debe evitar en las calles y aceras el uso de motos recreativas para menores de edad (bicimotos, tricimotos, cuadrimotos  así como el uso de patines y patineta.) que puedan ocasionar accidentes vehiculares, evitando alterar la tranquilidad de los residentes.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwelve">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
			  ART&Iacute;CULO ONCE:
			</a>
		  </h5>
		</div>
		<div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
		  <div class="panel-body">
				Se debe respetar y conservar los rótulos instalados en las calles con relación a la circulación de vehículos  y personas.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DOCE:
			</a>
		  </h5>
		</div>
		<div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
		  <div class="panel-body">
				Todos los residentes y visitantes deben respetar el acceso a las áreas privadas que corresponden a otros vecinos o que sean para uso común de la comunidad (cocheras, calles, aceras, redondeles, etc.) no estacionando vehículos de manera que impidan o dificulten la libre circulación vehicular o peatonal y el acceso a sus residencias. Todo lo anterior en cumplimiento a lo establecido en el Reglamento General de Tránsito.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingFourteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO TRECE:
			</a>
		  </h5>
		</div>
		<div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
		  <div class="panel-body">
				Todo residente debe respetar la tranquilidad y privacidad del vecindario inmediato. Tratando de no perjudicarlo con sonidos de aparatos con volumen excesivamente alto especialmente después de las 11:00 de la noche.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingFifteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO CATORCE:
			</a>
		  </h5>
		</div>
		<div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
		  <div class="panel-body">
				Para evitar consecuencias lamentables, se recuerda la prohibición de la ley de hacer uso inadecuado de armas de fuego dentro de la colonia.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingSixteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO QUINCE:
			</a>
		  </h5>
		</div>
		<div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
		  <div class="panel-body">
				Todos los vecinos deberán  conservar el ornato y la limpieza de la colonia y mantener en óptimas condiciones la parte exterior de su residencia.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingSeventeen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DIECISEIS:
			</a>
		  </h5>
		</div>
		<div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
		  <div class="panel-body">
				Los vecinos deben respetar las leyes de urbanismo y arquitectura al construir o modificar su casa.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingEighteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DIECISIETE:
			</a>
		  </h5>
		</div>
		<div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
		  <div class="panel-body">
				Los vecinos de la colonia deberán respetar las zonas verdes privadas y comunes de la colonia.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingNineteen">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNineteen" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DIECIOCHO:
			</a>
		  </h5>
		</div>
		<div id="collapseNineteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNineteen">
		  <div class="panel-body">
				No se permite practicar deportes extremos en las calles y avenidas de la colonia 
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwenty">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO DIECINUEVE:
			</a>
		  </h5>
		</div>
		<div id="collapseTwenty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwenty">
		  <div class="panel-body">
				No será permitido depositar ripio o basura en las áreas comunes como aceras, calles, predios baldíos, zonas verdes, etc.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyone">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyone" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTE:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyone" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyone">
		  <div class="panel-body">
				Todos los residentes que tengan perros deben vacunarlos y cumplir con las normas de sanidad correspondientes, y cuando circulen por la colonia deben colocarle cadena. Si son perros bravos se les debe colocar  bozal para evitar que puedan  causar algún daño a cualquier persona. En todo caso, el propietario es responsable por el perjuicio que ocasione su perro.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyTwo">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyTwo" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTIUNO:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyTwo">
		  <div class="panel-body">
				Los dueños de perros deben tener cuidado de que estos no ensucien las áreas privadas de los otros vecinos, las zonas verdes o las áreas comunes y en caso de hacerlo se tendrán que limpiar dichas áreas. Tenemos muchas quejas sobre los perros que pasean solos por las calles de la residencial y que son una amenaza para las personas que caminan o niños jugando. En caso de reportarse de nuevo perros ambulando solos en el residencial procederemos a llamar a la Alcaldía para que sean remitidos. 
		  </div>
		</div>
	  </div>
	   <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyThree">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyThree" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTIDOS:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyThree">
		  <div class="panel-body">
				Todo vecino debe cancelar mensualmente la cuota social aprobada por         La Asamblea General de Vecinos, la cual no es negociable en cualquier circunstancia. Se entiende que la cuota es por cada casa de habitación independientemente si está habitada o no, aquellos lotes que sean divididos para construir en ellos varias viviendas, deberán cancelar una cuota por cada vivienda habitacional si estas están identificadas individualmente por la dirección  de correo.
		  </div>
		</div>
	  </div>
	  
	   <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyFour">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFour" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTITRES:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFour">
		  <div class="panel-body">
				Para el uso familiar o alquiler del parque de la colonia, se deberá reservarlo con anticipación y por escrito a la administración y cancelar los derechos respectivos.
		  </div>
		</div>
	  </div>
	  
	  
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyFive">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFive" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTICUATRO:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyFive">
		  <div class="panel-body">
				Los terrenos baldíos privados dentro de la colonia deberán pagar media cuota mensual vigente sin que eso les dé derecho a limpieza de parte del personal de Avela ya que la limpieza de los terrenos baldíos privados es responsabilidad de sus propietarios.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentySix">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentySix" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTICINCO:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentySix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySix">
		  <div class="panel-body">
				Para facilitar el acceso y control de los visitantes se recomienda que aquellos vecinos que tengan reuniones, fiestas y otras actividades  proporcionen a la caseta de vigilancia  un listado previo escrito    para la debida identificación de los visitantes
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentySeven">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentySeven" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTISEIS:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentySeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentySeven">
		  <div class="panel-body">
				Fuera de las reparaciones de emergencia, se prohíbe reparar vehículos en las calles, igualmente las aceras deberán permanecer libres de obstáculos.
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyEight">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyEight" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTISIETE:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyEight">
		  <div class="panel-body">
				Los vecinos y visitantes están obligados a tener consideración y respeto a los empleados, personal de mantenimiento, personal administrativo de Avela y miembros de la seguridad de la colonia así mismo este personal deberá ser atento y respetuoso con los vecinos y visitantes.
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwentyNine">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyNine" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTIOCHO:
			</a>
		  </h5>
		</div>
		<div id="collapseTwentyNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwentyNine">
		  <div class="panel-body">
				Los empleados o personal de servicio autorizados por cada residente   deberán portar un  carnet de identificación emitido por la administración  con su respectiva fotografía.
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirty">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirty" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO VEINTINUEVE:
			</a>
		  </h5>
		</div>
		<div id="collapseThirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirty">
		  <div class="panel-body">
				Salvo Emergencias no se permiten trabajos de reparación y construcción en las viviendas los días sábados por las tardes, domingos y días feriados,  procurando dejar siempre las áreas aledañas a la vivienda debidamente limpias. Los horarios aprobados por La Asamblea General de Vecinos, son de Lunes a Viernes de 8:00 A.M. a 5:00 P.M. y el día Sábado de 8:00A.M. a 12:00 A.M. Después de cualquier construcción se les ruega recoger todo el ripio y restos de materiales como también dejar los tragantes libres de los mismos. Así mismo los trabajos a realizarse en el parque respetaran el horario siguiente: de lunes a viernes de 8am. a 12am. y de 2pm. a 5pm. los sábados de 8am. a 12am.  
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirtyOne">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirtyOne" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO TREINTA:
			</a>
		  </h5>
		</div>
		<div id="collapseThirtyOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirtyOne">
		  <div class="panel-body">
				No se permitirá  tener  rastras o furgones de gran dimensión estacionados  dentro de la colonia.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirtyTwo">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirtyTwo" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO TREINTA Y UNO:
			</a>
		  </h5>
		</div>
		<div id="collapseThirtyTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirtyTwo">
		  <div class="panel-body">
				Para reportar problemas de orden publico los vecinos deberán  llamar a las autoridades competentes a los teléfonos del CAM Tel.:  2511-6112 y PNC 911, TEL.: 2298-1439 delegación San Jacinto, ya que estos problemas no incumben  a la junta directiva.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirtyThree">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirtyThree" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO TREINTA Y DOS:
			</a>
		  </h5>
		</div>
		<div id="collapseThirtyThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirtyThree">
		  <div class="panel-body">
				Todo vecino o residente cuando tenga que sacar o regalar enseres o artículos voluminosos deberá reportarlo por escrito a la administración con copia a la caseta de seguridad.
		  </div>
		</div>
	  </div>
	  
	  <div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThirtyFour">
		  <h5 class="panel-title">
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirtyFour" aria-expanded="false" aria-controls="collapseNine">
			  ART&Iacute;CULO TREINTA Y TRES:
			</a>
		  </h5>
		</div>
		<div id="collapseThirtyFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirtyFour">
		  <div class="panel-body">
				Para gozar de los beneficios como socio activo de la colonia deberán estar al día con sus cuotas. 
		  </div>
		</div>
	  </div>
	</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script>
      $(document).ready(function() {

        $('#showCase1,#showCase2,#showCase3,#showCase4,#showCase5,#showCase6,#showCase7,#showCase8,#btn-back').hide();

        $("#menu1").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase1,#btn-back").show();
        });

        $("#menu2").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase2,#btn-back").show();
        });

        $("#menu3").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase3,#btn-back").show();
        });

        $("#menu4").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase4,#btn-back").show();
        });

        $("#menu5").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase5,#btn-back").show();
        });

        $("#menu6").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase6,#btn-back").show();
        });

        $("#menu7").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase7,#btn-back").show();
        });

        $("#menu8").on('click',function(e){
          e.preventDefault();
          $("#home_1").hide();
          $("#showCase8,#btn-back").show();
        });

        $("#btn-back").on('click',function(e){
          e.preventDefault();
          $('#showCase1,#showCase2,#showCase3,#showCase4,#showCase5,#showCase6,#showCase7,#showCase8').hide();
          $("#home_1").show();
          $('#btn-back').hide();
        });

      });
    </script>
  </body>
</html>
