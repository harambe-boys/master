var arrFilter = [];
var interval;
$(document).ready(function(){
	setContenido();
	$("#search").on('keyup', function(){
	  var matcher = new RegExp($(this).val(), 'gi');
	  $('.imoves-list').show().not(function(){
		  return matcher.test($(this).find('.name').text())
	  }).hide();
	});
});

function setContenido(){
	clearInterval(interval);
	arra2Filter = [];
	$.ajax({
		type: 'POST',
		url: 'json/peticiones.php',
		data: $(this).serialize(),
		success: function(data) {
			data = data.replace(/^\s*|\s*$/g,"");
			if (data!='ndata'){
				data = JSON.parse(data);
				data.forEach(function(element){
					var e2 = parseInt(element['id']);
					arra2Filter.push(e2);
					position = arrFilter.indexOf(e2);
					if ( position < 0 ){
						arrFilter.push(parseInt(element['id']));
						elemento = [element['id'],element['dia'],element['mes'],element['fechaFormato'],element['hora'],element['nombre']];
						set_contenido(elemento);
					}
				});
			}
			verificar_existencia(arra2Filter);
		}	
	});
	set_intervalo();	
}

function set_contenido(elemento){
	var html =	"<div class='media imoves-list' id='element_"+elemento[0]+"'>"+
					"<a href='detalle.php?id="+elemento[0]+"'>"+
						"<div class='media-left media-middle'>"+
							"<div class='imoves-circle'>"+
								"<p><span>"+elemento[1]+"</span>/<span>"+elemento[2]+"</span></p>"+
							"</div>"+
						"</div>"+
						"<div class='media-body media-middle'>"+
							"<h4 class='media-heading name'>"+elemento[5]+"</h4>"+
							"<p>"+elemento[3]+" "+elemento[4]+"</p>"+
						"</div>"+
						"<div class='media-right media-middle'>"+
							"<span class='imoves-arrow glyphicon glyphicon-chevron-right'></span>"+
						"</div>"+
					"</a>"+
					"<div class='imoves-separador'></div>"+
				"</div>";
	$('#body_peticion').append(html);
}

function set_intervalo(){
	interval = setInterval(function(){					
		setContenido();
	},10000);
}

function verificar_existencia(arra2Filter){
	if(arra2Filter.length==0){
		arrFilter.forEach(function(element){
			var e = parseInt(element);
			delete_div(e);
		});
		arrFilter = [];
	}else{
		if(arrFilter.length>0){
			arrFilter.forEach(function(element){
				var e = parseInt(element);
				position = arra2Filter.indexOf(e);
				if ( position < 0 ){
					delete_div(e);
				}
			});
		}
	}
	arrFilter = arra2Filter;
}

function delete_div(e){
	$('#element_'+e).remove();
}