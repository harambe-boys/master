<?php
	include_once "../data/dataBase.php";
	//Clases
	include_once "../classes/cUsuario.php";
	include_once "../classes/cVisitas.php";
	
	$oVisitas 	= new Visitas();
	
	$option    		= "";
	
	if (isset($_GET['opt'])) {
		$option    		= $_GET['opt'];
	}
	
	$vVisitaOne = $oVisitas->getVisitasOne($_REQUEST['id']);
	if(!$vVisitaOne){
		header('Location:index.php');
		exit();
	}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lomas de Altamira</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="confirm/jquery.confirm.css">
	<link rel="stylesheet" href="css/main.css">
	<script type="text/javascript">
	

	function guardarformulario(){
		$.confirm({
			'title': "Confirmar",
			'message': "<strong>&iquest;Esta seguro de Confirmar?</strong>",
			'buttons': {
				'Si': {
					'class': 'special',
					'action': function(){
						loading('Procesando..',1);
						var form = $("#form").serializeJSON(); 
						$.ajax({
							url:'actions/actionVisita.php',
							type:'POST',
							data: { valor: form }
						}).done(function( data ){
						
							if(data == 'done'){
								msg = "Accion realizada correctamente.";
								showSuccess(msg,5000);
								window.setTimeout("document.location.href='index.php';",2500);
							}else{
								msg = "ERROR. INTENTELO DE NUEVO.";
								showError(msg,7000);
								window.setTimeout("location.reload(true);",2500);
							}
						});
						return false;	 
					}
				},
				'No': {
					'class'	: ''
					}
			}
		});
	}
	</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
	<?php
		if($vVisitaOne){
			foreach($vVisitaOne AS $id=>$arrayVisitasOne){
	?>
    <div class="imoves_header">
      <div class="container-fluid">

        <a class="imoves-back" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Atr&aacute;s</a>&nbsp;&nbsp; <?=$arrayVisitasOne['residente']; ?>
      </div>
    </div>
	
    <div class="imoves-body" id="msg">
		<div id="alertMessage" class="alertMessage warning"></div>
        <div class="imoves-check2-ok"></div>
			<form id="form"  name="form" onsubmit="return false;">  
				<div class="imoves-detail-header">
					<input type="hidden" name="idVisita" value="<?=$_REQUEST['id'];?>"/>
					<input type="hidden" name="opt" value="mVisita" />
					
				  <div class="imoves-detail-circle">
					<p><span><?=$arrayVisitasOne['dia'];?></span>/<span><?=$arrayVisitasOne['mes']; ?></span></p>
				  </div>
				</br>
				  <p class="text-center"><label>Fecha de visita: </label> <?=$arrayVisitasOne['fechaFormato']; ?> <?=$arrayVisitasOne['hora']; ?></p>
				  <h2 class="text-center"><?=$arrayVisitasOne['nombre']; ?></h2>
				</br>
				  <p class="text-left"><label>DUI: </label><?=$arrayVisitasOne['dui']; ?></p>
				  <p class="text-left"><label>Placa de veh&iacute;culo: </label> <?=$arrayVisitasOne['placa_vehiculo'];?></p>
				  
				  <p class="text-left"><label>Residente: </label> <?=$arrayVisitasOne['residente'];?></p>
				  <!--<p class="text-left"><label>Cl&uacute;ster: </label> <?=$arrayVisitasOne['cluster'];?></p>-->
				  <p class="text-left"><label>Pol&iacute;gono: </label> <?=$arrayVisitasOne['poligono'];?></p>
				  <a class="btn btn-primary center-block imoves-btn-confirm" onclick="guardarformulario();"><span class="glyphicon glyphicon-ok"></span> Confirmar Visita </a>
				</div>
			</form>
        <div class="imoves-detail-description">
          <p>
           <?=$arrayVisitasOne['notas'];?>
          </p>
        </div>
	<?php
			}
		}
	?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="confirm/jquery.confirm.js"></script>
	<script src="js/jquery.serializejson.js"></script>
	<script src="js/element.js"></script>
	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
  </body>
</html>
