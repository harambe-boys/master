<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lomas de Altamira</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="confirm/jquery.confirm.css">
	<link rel="stylesheet" href="css/main.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="imoves_header">
      <div class="container-fluid">
	</div>
    </div>
	
    <div class="imoves-body" id="msg">
        <div class="imoves-check2-ok"></div>
		<div style="padding: 100px 60px 0 60px;">
            <a href="visitas.php" class="btn btn-primary btn-lg center-block imoves-btn-confirm" style="padding: 50px 2px 50px 2px;/* font-size: 50px; */"><span class="glyphicon glyphicon-ok"></span> Ir a pagina de visitas </a>
		</div>		
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="confirm/jquery.confirm.js"></script>
	<script src="js/jquery.serializejson.js"></script>
	<script src="js/element.js"></script>
	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
  </body>
</html>
