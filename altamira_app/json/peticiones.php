<?php
	//Data
	include_once "../../data/dataBase.php";
	//Clases
	include_once "../../classes/cVisitas.php";
	
	$oVisitas 		= new Visitas();
	$vVisitasAll 	= $oVisitas->getAllVisitas();
	$arr = false;
	$i = 0;
	try{
		if($vVisitasAll){
			foreach($vVisitasAll AS $id => $array){
				$arr[$i]['id'] 			= $id;
				$arr[$i]['dia'] 		= $array['dia'];
				$arr[$i]['mes'] 		= $array['mes'];
				$arr[$i]['fechaFormato']= $array['fechaFormato'];
				$arr[$i]['hora'] 		= $array['hora'];
				$arr[$i]['nombre'] 		= $array['nombre'];
				$i++;
			}
			if($arr){
				echo json_encode($arr);
			}else{
				echo 'ndata';
			}
		}else{
			echo 'ndata';
		}
	}catch(Exception $e){
		echo $e;
	}
	
?>