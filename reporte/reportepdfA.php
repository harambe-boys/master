<?php
require_once('fpdf/fpdf.php');
session_start();

$vData = $_SESSION['mred']['objRA'];
class PDF extends FPDF{
	var $widths;
	var $aligns;

	function SetWidths($w){
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			
			$this->Rect($x,$y,$w,$h);

			$this->MultiCell($w,5,$data[$i],0,$a,'true');
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}

	function Header(){
		//$this->Image('../images/mred/logo_mred.png',10,8,33);
		$this->SetFont('Arial','',12);
		$this->Cell(100);
		$this->Text(100,18,'Reportes Tecla App alertas municipales',0,'C', 0);
		$this->Ln(10);
	}

	function Footer(){
		$this->SetY(-15);
		$this->SetFont('Arial','B',8);
		$this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');

	}
	
	// Tabla coloreada
	function FancyTable($header, $data,$pdf){
		// Colores, ancho de l�nea y fuente en negrita
		$this->SetFillColor(11,83,170);
		$this->SetTextColor(255);
		$this->SetDrawColor(38,38,38);
		$this->SetLineWidth(.5);
		$this->SetFont('','B',10);
		// Cabecera
		$w = array(50, 45, 50, 45, 55);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],8,$header[$i],1,0,'C',true);
		$this->Ln();
		// Restauraci�n de colores y fuentes
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Datos
		$fill = false;
		$pdf->Ln(1);
		foreach($data as $row => $array) {
			$estado = '';
			if($array['ide']==3){
				$estado = 'Pendiente';
			}elseif($array['ide']==4){
				$estado = 'Aprobado';
			}elseif($array['ide']==5){
				$estado = 'Rechazada';
			}
			$fecha = date('d/m/Y H:i:s',strtotime($array['f']));
			$this->Cell($w[0],46,$array['t'],0,0,'C',$fill);$txt = stripslashes($array['t']);
			$txt = iconv('UTF-8', 'windows-1252', $txt);
			$this->Cell($w[0],46,$txt,0,0,'C',$fill);
			$this->Cell($w[0],46,$array['idp'],0,0,'C',$fill);
			$this->Cell($w[1],46,$fecha,0,0,'C',$fill);
			$this->Cell($w[2],46,$estado,0,0,'C',$fill);
			//$this->Cell($w[2],46,'http://maps.googleapis.com/maps/api/staticmap?center='.$array['lat'].','.$array['lon'].'&zoom=16&size=130x130&sensor=false&markers=color:blue%7Clabel:S%7C'.$array['lat'].','.$array['lon'].'',0,0,'C',$fill);
			$urlImage = 'http://maps.googleapis.com/maps/api/staticmap?center='.$array['lat'].','.$array['lon'].'&zoom=15&size=130x130&sensor=false&markers=color:blue%7C'.$array['lat'].','.$array['lon'].'&key=AIzaSyCTliRswidWchX2f41HCPPp3oWRG1cBfcA';
			$img = $pdf->Image($urlImage,null,null,0,0,'PNG');
			$this->Cell($w[3],1,'',0,0,'C',false,$img);
			$this->Ln();
			$fill = !$fill;
		}

		// L�nea de cierre
		$this->Cell(array_sum($w),0,'',0);
	}

}

	$pdf = new PDF();
	// T�tulos de las columnas
	$header = array('Tipo de Alerta','Usuario', 'Fecha', 'Estado','Lugar');
	// Carga de datos
	$pdf->SetFont('Arial','',10);
	$pdf->AddPage('L');
	$pdf->SetMargins(15,20,20);
	$pdf->Ln(10);

    $pdf->SetFont('Arial','',10);
	$pdf->Cell(0,6,'Reporte de Alertas',0,1);
	$pdf->Cell(0,6,'Fecha y Hora: '.date("d/m/Y H:i:s"),0,1);
	 
	$pdf->Ln(10);
	$pdf->FancyTable($header,$vData,$pdf);
	$pdf->Output();
?>