<?php
session_start();
//var_dump ($_SESSION['all']);
//$_SESSION['all'];

require_once('fpdf/fpdf.php');
require('conexion.php');
include_once('../classes/cReporte.php');
$estado = $_SESSION['all']['1'];
$fecha1 = $_SESSION['all']['2'];
$fecha2 = $_SESSION['all']['3'];
class PDF extends FPDF
{
var $widths;
var $aligns;


function SetWidths($w)
{
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a)
{
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data)
{
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++)
	{
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		
		$this->Rect($x,$y,$w,$h);

		$this->MultiCell($w,5,$data[$i],0,$a,'true');
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h)
{
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}

function Header()
{

	$this->SetFont('Arial','',10);
	$this->Text(63,18,'Yupi Reportes',0,'C', 0);
	$this->Image('../../desarrollo/admin/ybase/images/yupiReporte.jpg', 20, 8, 41);
	$this->Ln(10);
}

function Footer()
{
	$this->SetY(-15);
	$this->SetFont('Arial','B',8);
	$this->Cell(100,10,'Yupi',0,0,'L');
	$this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');

}

}

if($estado==3){
	
	$reporte= $estado;
	$con = new DB;
	$reportes = $con->conectar();	
	
	$strConsulta = "SELECT * FROM estado WHERE idestado = '$reporte'";
	
	$reportes = mysql_query($strConsulta);
	
	$fila = mysql_fetch_array($reportes);

	$pdf=new PDF('L','mm','Letter');
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(20,20,20);
	$pdf->Ln(10);

    $pdf->SetFont('Arial','',12);
	$pdf->Cell(0,6,'Reporte de Carreras',0,1);
	$pdf->Cell(0,6,'Fecha y Hora: '.date("d/m/Y H:i:s"),0,1);
	$pdf->Cell(0,6,'Estado: '.$fila['nombre'],0,1);
	 
	$pdf->Ln(10);

	//$pdf->SetWidths(array(65, 60, 55, 50, 20));
	$pdf->SetWidths(array(25, 38, 38, 30, 40, 40));
	$pdf->SetFont('Arial','B',10);
	$pdf->SetFillColor(241,187,0);
    $pdf->SetTextColor(255);

		for($i=0;$i<1;$i++)
			{
				$pdf->Row(array('Fecha y hora', 'Punto de Inicio', 'Punto de Destino', 'Telefono', 'Usuario', 'Correo Usuario'));
			}
	
	$historial = $con->conectar();	
	$strConsulta = "SELECT p.idpeticion,p.direccion,p.fecha,p.idusuarioapp,p.idestado,p.idtipotaxi,p.destino,p.telefono,u.nombre,u.apellido,u.correo
					FROM peticion AS p INNER JOIN usuarioapp AS u 
					ON u.idusuarioapp = p.idusuarioapp 
					WHERE p.idestado = '$reporte' AND p.fecha BETWEEN '$fecha1' AND '$fecha2'  ORDER BY p.fecha DESC;";
	
					
	$historial = mysql_query($strConsulta);
	$numfilas = mysql_num_rows($historial);
	
	for ($i=0; $i<$numfilas; $i++)
		{
			$fila = mysql_fetch_array($historial);
			$pdf->SetFont('Arial','',10);
			
			if($i%2 == 1)
			{			
				$pdf->SetFillColor(255,255,255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($fila['fecha'], $fila['direccion'], $fila['destino'],$fila['telefono'], $fila['nombre'].' '.$fila['apellido'],$fila['correo']));
								
			}
			else
			{
				$pdf->SetFillColor(255,255,255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($fila['fecha'], $fila['direccion'], $fila['destino'], $fila['telefono'],$fila['nombre'].' '.$fila['apellido'],$fila['correo']));
			}
		}
		
		$pdf->Output();
		
		echo "contenido 1 ";
}else{

	$reporte= $estado;
	$con = new DB;
	$reportes = $con->conectar();	
	
	$strConsulta = "SELECT * FROM estado WHERE idestado = '$reporte'";
	
	$reportes = mysql_query($strConsulta);
	
	$fila = mysql_fetch_array($reportes);

	$pdf=new PDF('L','mm','Letter');
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(20,20,20);
	$pdf->Ln(10);

    $pdf->SetFont('Arial','',12);
	$pdf->Cell(0,6,'Reporte de Carreras',0,1);
	$pdf->Cell(0,6,'Fecha y Hora: '.date("d/m/Y H:i:s"),0,1);
	$pdf->Cell(0,6,'Estado: '.$fila['nombre'],0,1);
	 
	$pdf->Ln(10);

	//$pdf->SetWidths(array(65, 60, 55, 50, 20));
	$pdf->SetWidths(array(25, 40, 40, 40, 15, 40, 15, 25));
	$pdf->SetFont('Arial','B',10);
	$pdf->SetFillColor(241,187,0);
    $pdf->SetTextColor(255);

		for($i=0;$i<1;$i++)
			{
				$pdf->Row(array('Fecha y hora', 'Punto de Inicio', 'Punto de Destino', 'Usuario', 'Unidad', 'Conductor', 'Tarifa', 'Cooperativa'));
			}
	
	$historial = $con->conectar();	
	/*$strConsulta = "SELECT p.idpeticion,p.direccion,p.fecha,p.idusuarioapp,p.idestado,p.idtipotaxi,p.destino,p.telefono,u.nombre,u.apellido,u.correo
					FROM peticion AS p INNER JOIN usuarioapp AS u 
					ON u.idusuarioapp = p.idusuarioapp 
					WHERE p.idestado = '$reporte' AND p.fecha BETWEEN '$fecha1' AND '$fecha2'  ORDER BY p.fecha DESC;";
	*/
	$strConsulta = "SELECT a.idasignacion, p.fecha, p.direccion, p.destino, p.idestado, u.nombre AS nombreUsu, u.apellido AS apellidoUsu, t.unidad, 
						ta.nombre AS nombreTax, ta.apellido AS apellidoTax, a.precioEstimado, c.nombre AS cooperativa
					FROM asignacion AS a
						INNER JOIN peticion AS p ON a.idpeticion= p.idpeticion
						INNER JOIN usuarioapp AS u ON p.idusuarioapp = u.idusuarioapp
						INNER JOIN taxi AS t ON a.idtaxi = t.idtaxi 
						INNER JOIN taxista AS ta ON a.idtaxista = ta.idtaxista
						INNER JOIN puntotaxi AS pt ON t.idpuntotaxi=pt.idpuntotaxi 
						INNER JOIN company AS c ON pt.idcompany=c.idcompany
					WHERE p.idestado = '$reporte' AND p.fecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY p.fecha DESC;";
					
	$historial = mysql_query($strConsulta);
	$numfilas = mysql_num_rows($historial);
	
	for ($i=0; $i<$numfilas; $i++)
		{
			$fila = mysql_fetch_array($historial);
			$pdf->SetFont('Arial','',10);
			
			if($i%2 == 1)
			{			
				$pdf->SetFillColor(255,255,255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($fila['fecha'], $fila['direccion'], $fila['destino'], $fila['nombreUsu'].' '.$fila['apellidoUsu'],'# '.$fila['unidad'],$fila['nombreTax'].' '.$fila['apellidoTax'],'$ '.$fila['precioEstimado'],$fila['cooperativa']));
								
			}
			else
			{
				$pdf->SetFillColor(255,255,255);
    			$pdf->SetTextColor(0);
				$pdf->Row(array($fila['fecha'], $fila['direccion'], $fila['destino'], $fila['nombreUsu'].' '.$fila['apellidoUsu'],'# '.$fila['unidad'],$fila['nombreTax'].' '.$fila['apellidoTax'],'$ '.$fila['precioEstimado'],$fila['cooperativa']));
			}
		}
		
		$pdf->Output();


 //echo "contenido 2 ";
}
	
//$pdf->Output();
?>