<?php 
session_start();

if(!$_SESSION['Altamira']['reporte_excel']){
	echo "Error en los datos";
	exit();
}
$vData = $_SESSION['Altamira']['reporte_excel'];


$html = "";
$html .= "<!DOCTYPE html><body>";
$html .= "<table>";
$html .= "	<thead>";
$html .= "		<tr>";
$html .= "			<th>Nombre visitante</th>";
$html .= "			<th>Placa</th>";
$html .= "			<th>Fecha</th>";
$html .= "			<th>Hora</th>";
$html .= "			<th>Residente</th>";
$html .= "			<th>Calle</th>";
$html .= "			<th>Casa</th>";
$html .= "			<th>Notas</th>";
$html .= "			<th>Estado</th>";
$html .= "			<th>Hora de confirmaci&oacute;n</th>";
$html .= "		</tr>";
$html .= "	</thead>";
$html .= "	<tbody>";
		if($vData){
			foreach ($vData AS $id => $array) {
				$fecha = date('d/m/Y',strtotime($array['fecha']));
				if($array['estado']==1){ $txt = 'Procesado';}elseif($array['estado']==0){$txt = 'No Procesado';}
				if($array['hora_confirmacion']){
					$fecha2 = date('d/m/Y H:i:s',strtotime($array['hora_confirmacion']));
				}else{
					$fecha2 = "";
				}
$html .= "			<tr>";
$html .= "				<td >".$array['nombre_visitante']."</td>";
$html .= "				<td >".$array['placa_vehiculo']."</td>";
$html .= "				<td >$fecha</td>";
$html .= "				<td >".$array['hora']."</td>";
$html .= "				<td >".$array['residente']."</td>";
$html .= "				<td >".$array['cluster']."</td>";
$html .= "				<td >".$array['casa']."</td>";
$html .= "				<td >".$array['notas']."</td>";
$html .= "				<td >".$txt."</td>";
$html .= "				<td >".$fecha2."</td>";
$html .= "			</tr>";
			}
		}	
$html .= "	</tbody>";
$html .= "</table>";
$html .= "</body></html>";

header( "Content-type: application/vnd.ms-excel; name= 'excel'");
header("Content-Disposition: filename=report_visitantes".time()."-".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");

$dados_recebido = iconv('utf-8','iso-8859-1',$html);
echo $dados_recebido;
?>