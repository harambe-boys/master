<?php 
session_start();

$vData = $_SESSION['mred']['objRG'];


$html = "";
$html .= "<!DOCTYPE html><body>";
$html .= "<table>";
$html .= "	<thead>";
$html .= "		<tr>";
$html .= "			<th>Tipo de Gestion</th>";
$html .= "			<th>Fecha reporte</th>";
$html .= "			<th>OS movil</th>";
$html .= "			<th>Estado</th>";
$html .= "			<th>Mapa</th>";
$html .= "		</tr>";
$html .= "	</thead>";
$html .= "	<tbody>";
		if($vData){
			foreach ($vData AS $id => $array) {
				if($array['idd']==1){$os = "Android";}elseif($array['idd']==2){$os = "IOS";}
				if($array['ide']==3){$e = "Pendiente";}elseif($array['ide']==4){$e =  "Aprobada";}elseif($array['ide']==5){$e = "Rechazada";}elseif($array['ide']==6){$e = "En proceso";}elseif($array['ide']==7){$e = "Terminada";}
				$fecha = date('d/m/Y H:i:s',strtotime($array['f']));
$html .= "			<tr>";
$html .= "				<td >".$array['t']."</td>";
$html .= "				<td >$fecha</td>";
$html .= "				<td >$os</td>";
$html .= "				<td >$e</td>";
$html .= "				<td>";
$html .= "					http://maps.googleapis.com/maps/api/staticmap?center=".$array['lat'].",".$array['lon']."&zoom=14&size=130x130&sensor=false&markers=color:blue%7C".$array['lat'].",".$array['lon']."";
$html .= "				</td>";
$html .= "			</tr>";
			}
		}	
$html .= "	</tbody>";
$html .= "</table>";
$html .= "</body></html>";

header( "Content-type: application/vnd.ms-excel; name= 'excel'");
header("Content-Disposition: filename=report".time()."-".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");

$dados_recebido = iconv('utf-8','iso-8859-1',$html);
echo $dados_recebido;
?>