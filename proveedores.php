<?php
	//Data
	include_once "data/dataBase.php";
	//Clases
	include_once "classes/cUsuario.php";
	include_once "classes/cProveedor.php";
	
	$oUsuario 		= new Usuario();
	$oProveedor 	 	= new Proveedor();
	
	//$path = 'http://52.23.92.102/Salazar/IMGSALAZAR/';
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	
	  if (!$_SESSION['Altamira']['permisos'][4]) {
		header("Location: index.php");
		exit();
	}
	
	$nombre = $_SESSION['Altamira']['user'];
	$url = dirname($_SERVER["PHP_SELF"]); 
	$idProveedor		= "";
	$nombre			= "";
	$descripcion		= "";
	$imagen		 	= "";
	$telefono			= "";
	$puntuacion		= "";
	$estado 			= "";
	$idTipop 			= "";
	$_SESSION['Altamira']['imgProveedor']="";
	$option    			= "";
	
	 $path = 'http://altamira.buzzcoapp.com/IMG/';
		
	if (isset($_GET['opt'])) {
		$option    		= $_GET['opt'];
		$idProveedor 	= $_GET['idProveedor'];
	}
	
	if ($option == "mProveedor") {
		$vProveedor = $oProveedor->getProveedorOne($idProveedor);
		foreach ($vProveedor AS $id => $info){  
			$idProveedor	= $id;
			$nombre			= $info["nombre"];
			$descripcion	= $info["descripcion"];
			$imagen			= $info["imagen"];
			$telefono		= $info["telefono"];
			$puntuacion 	= $info["puntuacion"];
			$estado 		= $info["estado"];
			$idTipop 		= $info["idProveedorcategoria"];
		}
	}
	
	$vProveedores 	= $oProveedor->getProveedorAll();
	$vTipoProveedor	= $oProveedor->getTipoProveedor();
	//$vRol			= $oUsuarioWeb->getRolAll();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Proveedores</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#Proveedor").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				<?php
					if($option == "mProveedor"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers",
					"aaSorting": []
				});
				
				 $('[id^=telefono]').keypress(validateNumber);
			});
			
			function validateNumber(event) {
				var key = window.event ? event.keyCode : event.which;

				if (event.keyCode === 8 || event.keyCode === 46
					|| event.keyCode === 37 || event.keyCode === 39) {
					return true;
				}
				else if ( key < 48 || key > 57 ) {
					return false;
				}
				else return true;
			};
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				if (!form.nombre.value) {
					msg = 'Ingrese nombre del Proveedor';
					is_error = true;
				}else if(!form.descripcion.value){
					msg = 'Ingresar la descripci&oacute;n del Proveedor';
					is_error = true;
				}else if(!form.imagenI.value){
					msg = 'Ingresar una imagen';
					is_error = true;
				}else if(!form.telefono.value){
					msg = 'Ingresar un tel&eacute;fono';
					is_error = true;
				}else if(!form.puntuacion.value){
					msg = 'Ingresar una  puntuaci&oacute;n';
					is_error = true;
				}else if(!form.tipoP.value){
					msg = 'Ingresar un tipo de proveedor';
					is_error = true;
				}else if(!form.estado.value){
					msg = 'Ingresar un estado';
					is_error = true;
				}
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mProveedor") {
							echo 'msg = "El Proveedor ha sido Modificado.";';
						}else{
							echo 'msg = "El nuevo Proveedor ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='proveedores.php';",2500);
				} else if(responseText == 'exist') {
					msg = "Nombre no disponible, intente con otro nombre.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if(responseText == 'pass') {
					msg = "La verificaci\u00F3n de la contrase\u00F1a no coinciden.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if (responseText == 'error1'){
					msg = "Necesita ingresar la contrase\u00F1a antigua.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 );
					document.getElementById("get").disabled = false;
				}else if (responseText == 'error2'){
					msg = "Contrase\u00F1as antigua incorrecta.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 ); 
					document.getElementById("get").disabled = false;
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
			
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><span class="ico  fa fa-user fa-2x"></span>Proveedores  </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >
								<li id="hi1"><a href="#tab1"  id="3"  >  Lista de Proveedores</a></li>  
								<li id="hi2"><a href="#tab2"  id="2"  ><?php if ($option == "mProveedor") { echo "Modificar"; }else{ echo "Crear"; }?> Proveedor</a></li>   
							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>nombre</th>
														<th>Descripci&oacute;n</th>
														<th>Imagen</th>
														<th>Telefono</th>
														<th>Puntuaci&oacute;n</th>
														<th>Tipo de la Categor&iacute;a</th>
														<th>Estado</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vProveedores){
														foreach ($vProveedores AS $id => $arrProveedor) {
													?>
														<tr>
															<td><?=$arrProveedor['nombre'];?></td>
															<td><?=$arrProveedor['descripcion'];?></td>															
															<td><img src="<?=$path .$arrProveedor['imagen'];?>" /></td>
															<td ><?=$arrProveedor['telefono'];?></td>
															<td ><?=$arrProveedor['puntuacion'];?></td>
															<td ><?=$arrProveedor['nombreCategoria'];?></td>
															<td ><?php if($arrProveedor['estado']==1){echo "activo";}else{echo "Inactivo";}?></td>
															<td><span class="tip" >
																	<a href="proveedores.php?opt=mProveedor&idProveedor=<?=$id;?>" title="Modificar" >
																		<img src="images/icon/icon_edit.png" > 
																	</a>
																	<?php 
																		if($arrProveedor['estado']==1){
																	?>
																			<a id="opt=bProveedor&idProveedor=<?=$id?>" class="actiones2"  data-action="actionProveedor.php" data-location="proveedores.php" data-tittle="Bloquear" data-msg="&iquest;Quiere bloquear al usuario?" name="<?=$arrProveedor['nombre']?>" title="Bloquear">
																				<img src="images/icon/color_18/delete.png" >
																			</a>
																	<?php
																		}elseif($arrProveedor['estado']==0){
																	?>
																			<a id="opt=hProveedor&idProveedor=<?=$id?>" class="actiones2"  data-action="actionProveedor.php" data-location="proveedores.php" data-tittle="Habilitar" data-msg="&iquest;Quiere habilitar al usuario?" name="<?=$arrProveedor['nombre']?>" title="Habilitar">
																				<img src="images/icon/color_18/checkmark2.png" >
																			</a>
																	<?php
																	}
																	?>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
								<div id="tab2" class="tab_content"> 
									<div class="load_page">
										<div class="formEl_b" id="msg">	
											<form id="form" action="actions/actionProveedor.php" method="post" name="form"> 
												<?php
													if ($option == "mProveedor") {
														echo '<input type="hidden" name="idProveedor" value="'.$idProveedor.' "/>';
														echo '<input type="hidden" name="opt" value="mProveedor" />';
														echo '<input type="hidden" name="oldimg" id="oldimg" value="'.$imagen.'" />';
													}else{
														echo '<input type="hidden" name="opt" value="nProveedor" />';
													}
												?>
												<fieldset >
													<legend>Por favor introducir toda la informaci&oacute;n.</span></legend>
													
														
													<div class="section ">
														<label> Nombre del Proveedor</label>   
														<div> 
															<input type="text" class="validate[required] large" name="nombre" id="nombre" value="<?=$nombre;?>" />
														</div>
													</div>
													<div class="section ">
														<label> Descripci&oacute;n</label>   
														<div> 
															<textarea class="form-control validate[required] large" rows="10" name="descripcion" id="descripcion" value=""><?=$descripcion;?></textarea>
														</div>
													</div>
													<div class="section ">
														<label class="control-label"> Imagen</label>
														<div class="controls">
															<small>Permitidas: Jpeg, Jpg y Png. | Tama&ntilde;o maximo 1 MB  y tama&ntilde;o de 135x135px.</small>
															<div id="upload-wrapperI" >
																<input name="imagenI" class="fileupload " id="imagenI" type="file" />
																<div style="margin-top: 2%;margin-left: 20%;">
																	<input type="button"  id="submit-btnI" value="Upload" onclick="uploadAjaxIMG('I','process.php')" />
																</div>
																<div id="outputI" style="margin-left: 20%;">
																	<?php
																		if($imagen){
																	?>
																			<img src="<?=$path.$imagen?>" align="center"/>
																	<?php	
																		}
																	?>
																</div>
															</div>
														</div>
													</div>
													<div class="section ">
														<label> Tel&eacute;fono</label>   
														<div> 
															<input type="input" class="validate[required] large" name="telefono" id="telefono" size="8" maxlength="8" value="<?=$telefono;?>" />
														</div>
													</div>
													<div class="section ">
														<label> Puntuaci&oacute;n</label>   
														<div> 
															<input type="text" class="validate[required] large" name="puntuacion" id="puntuacion" value="<?php if ($option == "mProveedor") { echo $puntuacion; }else{ echo 0; }?>" readonly />
														</div>
													</div>
													<div class="section">
														<label> Tipo de Proveedor</label>   
														<div> 
															<select name="tipoP" id="tipoP" class="chzn-select">
																<?php
																	if($vTipoProveedor){
																		foreach ($vTipoProveedor AS $id => $arrTipop) {
																			if($idTipop == $id){
																				$selected = "selected = 'selected'";
																?>
																<option value="<?=$id;?>"  <?=$selected?>><?=$arrTipop['nombreCategoria'];?></option>
																<?php
																			} else {
																?>
																<option value="<?=$id;?>"><?=$arrTipop['nombreCategoria'];?></option>
																<?php
																			}
																		}
																	}
																?>
															</select>
														</div>
													</div>												
													<div class="section">
														<label> Estado</label>   
														<div> 
															<select name="estado" id="estado" class="chzn-select">
																<option value="0" <?php if($estado==0){echo"selected='selected'";}?>>Inactivo</option>
																<option value="1" <?php if($estado==1){echo"selected='selected'";}?>>Activo</option>
															</select>
														</div>
													</div>											
													<div class="section last">
														<div>
														<?php
															if ($option == "mProveedor") {
																echo '<input type="submit" value="MODIFICAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}else{
																echo '<input type="submit" value="GUARDAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}
														?>      
														<a class="btn btn-danger" style="padding: 2px 12px 2px 12px; border-radius: 0px;border: 1px solid #000000;" href="http://52.23.92.102/Altamira/proveedores.php" role="button">Cancelar</a>														
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>	
								</div><!--tab2-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>