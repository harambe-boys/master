<?php
	//Data
	include_once "data/dataBase.php";

	//Clases
	include_once "classes/cUsuario.php";
	
	$oUsuario 		= new Usuario();
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	$nombreCurrent = $_SESSION['Altamira']['user'];
	$url = dirname($_SERVER["PHP_SELF"]); 
	
	if(!isset($_SESSION['Altamira']['f1'])){
		$_SESSION['Altamira']['f1'] = date('Y-m-d');
	}
	if(!isset($_SESSION['Altamira']['f2'])){
		$_SESSION['Altamira']['f2'] = date('Y-m-d');
	}
	if(!isset($_SESSION['e'])){
		$_SESSION['Altamira']['e'] = 0;
	}
	if(!isset($_SESSION['g'])){
		$_SESSION['Altamira']['g'] = 0;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="shortcut icon" type="image/ico" href="../images/lomas_altamira.ico" /> 
        <title>Alertas espec&iacute;ficas</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript" src="js/detalle.js"></script>
		
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><i class="fa fa-filter fa-1x"></i> Filtro Alertas Espec&iacute;ficas </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
							
						<form >
							<div class="formEl_b" id="msg">
								<fieldset>
									<div class="filtroDetalleMapa">
										<div class="filtroDetalleMapa">
										<div class="section">
											<label> Tipo de Alerta</label>  
											<div>
												<select name="tipo" id="tipo">
													<option value="0" <?php if($_SESSION['Altamira']['g']==0){echo "selected='selected'";}?> >Todas</option>
													<option value="3" <?php if($_SESSION['Altamira']['g']==3){echo "selected='selected'";}?> >Ruido Excesivo</option>
													<option value="5" <?php if($_SESSION['Altamira']['g']==5){echo "selected='selected'";}?> >Alumbrado</option>
													<option value="6" <?php if($_SESSION['Altamira']['g']==6){echo "selected='selected'";}?> >Fuga de Agua</option>
													<option value="7" <?php if($_SESSION['Altamira']['g']==7){echo "selected='selected'";}?> >Basura Acumulada</option>
													<option value="8" <?php if($_SESSION['Altamira']['g']==8){echo "selected='selected'";}?> >Excremento de animales</option>
												</select>
											</div>
										</div>
										<div class="section">
											<label> Estado</label>  
											<div>
												<select name="estado" id="estado">
													<option value="0" <?php if($_SESSION['Altamira']['e']==0){echo "selected='selected'";}?> >Todos</option>
													<option value="1" <?php if($_SESSION['Altamira']['e']==1){echo "selected='selected'";}?> >Pendiente</option>
													<option value="2" <?php if($_SESSION['Altamira']['e']==2){echo "selected='selected'";}?> >Aprobada</option>
													<option value="3" <?php if($_SESSION['Altamira']['e']==3){echo "selected='selected'";}?> >Rechazada</option>
													<option value="4" <?php if($_SESSION['Altamira']['e']==4){echo "selected='selected'";}?> >En Proceso</option>
													<option value="5" <?php if($_SESSION['Altamira']['e']==5){echo "selected='selected'";}?> >Terminada</option>
												</select>
											</div>
										</div>
										<div class="section">
											<label> Fecha inicial</label>  
											<div>
												<input id="datepicker1" name="datepicker1" type="text" class="form-control" onclick='$(this).datepicker().datepicker( "show" );'value="<?php echo date('d-m-Y', strtotime($_SESSION['Altamira']['f1'])); ?>"/>
											</div>
										</div>
										<div class="section">	
											<label> Fecha fin</label>  
											<div>
												<input id="datepicker2" name="datepicker2" type="text" class="form-control" onclick='$(this).datepicker().datepicker( "show" );'value="<?php echo date('d-m-Y',strtotime($_SESSION['Altamira']['f2'])); ?>"/>
											</div>
										</div>
										<div class="section last">
											<div>
												<input type="button" value="Filtrar" class="uibutton submit_form" name="Filtrar" onclick="filtrar();"/>
											</div>
										</div>
									</div>
								</fieldset>
							</div> 
						</form>
					</div> 
				</div>
				
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><i class="fa fa-university fa-1x"></i> Alertas Espec&iacute;ficas</span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
							
						<div id="uploadTab">
							<div class="tabs">
								
							</div>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar" name="detalleform" id="detalleform" method="post" action="detalle_alerta_select.php">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Tipo de Alerta</th>
														<th>Fecha de Reporte</th>
														<th>Usuarios</th>
														<th>Calle</th>
														<th>Casa</th>
														<th>Estado</th>
														<th>Opciones</th>
													</tr>
												</thead>
												<tbody id="tablegestion">
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
							<div class="clear"/>
						</div> 
					</div> 
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
	
</html>