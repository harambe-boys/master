/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
* Licensed under the MIT License (LICENSE.txt).
*
* Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
* Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
* Thanks to: Seamus Leahy for adding deltaX and deltaY
*
* Version: 3.0.6
* 
* Requires: 1.2.2+
*/

(function ($) {

    var types = ['DOMMouseScroll', 'mousewheel'];

    if ($.event.fixHooks) {
        for (var i = types.length; i; ) {
            $.event.fixHooks[types[--i]] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener) {
                for (var i = types.length; i; ) {
                    this.addEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function () {
            if (this.removeEventListener) {
                for (var i = types.length; i; ) {
                    this.removeEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function (fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event, args = [].slice.call(arguments, 1), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if (orgEvent.wheelDelta) { delta = orgEvent.wheelDelta / 120; }
        if (orgEvent.detail) { delta = -orgEvent.detail / 3; }

        // New school multidimensional scroll (touchpads) deltas
        deltaY = delta;

        // Gecko
        if (orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaY = 0;
            deltaX = -1 * delta;
        }

        // Webkit
        if (orgEvent.wheelDeltaY !== undefined) { deltaY = orgEvent.wheelDeltaY / 120; }
        if (orgEvent.wheelDeltaX !== undefined) { deltaX = -1 * orgEvent.wheelDeltaX / 120; }

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

})(jQuery);
/**
* @author trixta
* @version 1.2
*/
(function ($) {

    var mwheelI = {
        pos: [-260, -260]
    },
	minDif = 3,
	doc = document,
	root = doc.documentElement,
	body = doc.body,
	longDelay, shortDelay;

    function unsetPos() {
        if (this === mwheelI.elem) {
            mwheelI.pos = [-260, -260];
            mwheelI.elem = false;
            minDif = 3;
        }
    }

    $.event.special.mwheelIntent = {
        setup: function () {
            var jElm = $(this).bind('mousewheel', $.event.special.mwheelIntent.handler);
            if (this !== doc && this !== root && this !== body) {
                jElm.bind('mouseleave', unsetPos);
            }
            jElm = null;
            return true;
        },
        teardown: function () {
            $(this)
			.unbind('mousewheel', $.event.special.mwheelIntent.handler)
			.unbind('mouseleave', unsetPos);
            return true;
        },
        handler: function (e, d) {
            var pos = [e.clientX, e.clientY];
            if (this === mwheelI.elem || Math.abs(mwheelI.pos[0] - pos[0]) > minDif || Math.abs(mwheelI.pos[1] - pos[1]) > minDif) {
                mwheelI.elem = this;
                mwheelI.pos = pos;
                minDif = 250;

                clearTimeout(shortDelay);
                shortDelay = setTimeout(function () {
                    minDif = 10;
                }, 200);
                clearTimeout(longDelay);
                longDelay = setTimeout(function () {
                    minDif = 3;
                }, 1500);
                e = $.extend({}, e, { type: 'mwheelIntent' });
                return $.event.handle.apply(this, arguments);
            }
        }
    };
    $.fn.extend({
        mwheelIntent: function (fn) {
            return fn ? this.bind("mwheelIntent", fn) : this.trigger("mwheelIntent");
        },

        unmwheelIntent: function (fn) {
            return this.unbind("mwheelIntent", fn);
        }
    });

    $(function () {
        body = doc.body;
        //assume that document is always scrollable, doesn't hurt if not
        $(doc).bind('mwheelIntent.mwheelIntentDefault', $.noop);
    });
})(jQuery);

		(function(){
    
    var special = jQuery.event.special,
        uid1 = 'D' + (+new Date()),
        uid2 = 'D' + (+new Date() + 1);
        
    special.scrollstart = {
        setup: function() {
            
            var timer,
                handler =  function(evt) {
                    
                    var _self = this,
                        _args = arguments;
                    
                    if (timer) {
                        clearTimeout(timer);
                    } else {
                        evt.type = 'scrollstart';
                        jQuery.event.handle.apply(_self, _args);
                    }
                    
                    timer = setTimeout( function(){
                        timer = null;
                    }, special.scrollstop.latency);
                    
                };
            
            jQuery(this).bind('scroll', handler).data(uid1, handler);
            
        },
        teardown: function(){
            jQuery(this).unbind( 'scroll', jQuery(this).data(uid1) );
        }
    };
    
    special.scrollstop = {
        latency: 300,
        setup: function() {
            
            var timer,
                    handler = function(evt) {
                    
                    var _self = this,
                        _args = arguments;
                    
                    if (timer) {
                        clearTimeout(timer);
                    }
                    
                    timer = setTimeout( function(){
                        
                        timer = null;
                        evt.type = 'scrollstop';
                        jQuery.event.handle.apply(_self, _args);
                        
                    }, special.scrollstop.latency);
                    
                };
            
            jQuery(this).bind('scroll', handler).data(uid2, handler);
            
        },
        teardown: function() {
            jQuery(this).unbind( 'scroll', jQuery(this).data(uid2) );
        }
    };
    
})();
		
function jsHover($el){
		// the extension functions and options 	
					extensionPlugin 	= {		
						extPluginOpts	: {
							mouseLeaveFadeSpeed	: 500,
							hovertimeout_t		: 1000,
							useTimeout			: false,
							deviceWidth			: 980
						},
						hovertimeout	: null, // timeout to hide the scrollbar
						isScrollbarHover: false,// true if the mouse is over the scrollbar
						elementtimeout	: null,	// avoids showing the scrollbar when moving from inside the element to outside, passing over the scrollbar
						isScrolling		: false,// true if scrolling
						addHoverFunc	: function() {
							
							// run only if the window has a width bigger than deviceWidth
							if( $(window).width() <= this.extPluginOpts.deviceWidth ) return false;
							var instance		= this;
							$.fn.jspmouseenter 	= $.fn.show;
							$.fn.jspmouseleave 	= $.fn.fadeOut;
							var $vBar			= this.getContentPane().siblings('.jspVerticalBar').hide();
							$el.bind('mouseenter.jsp',function() {
								
								$vBar.stop( true, true ).jspmouseenter();
								if( !instance.extPluginOpts.useTimeout ) return false;
								clearTimeout( instance.hovertimeout );
								instance.hovertimeout 	= setTimeout(function() {
									if( !instance.isScrolling )
										$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 );
								}, instance.extPluginOpts.hovertimeout_t );
							}).bind('mouseleave.jsp',function() {
								
								if( !instance.extPluginOpts.useTimeout )
									$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 );
								else {
								clearTimeout( instance.elementtimeout );
								if( !instance.isScrolling )
										$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 );
								}
							});
							
							if( this.extPluginOpts.useTimeout ) {
								
								$el.bind('scrollstart.jsp', function() {
								
									// when scrolling show the scrollbar
									clearTimeout( instance.hovertimeout );
									instance.isScrolling	= true;
									$vBar.stop( true, true ).jspmouseenter();
									
								}).bind('scrollstop.jsp', function() {
									
									// when stop scrolling hide the scrollbar (if not hovering it at the moment)
									clearTimeout( instance.hovertimeout );
									instance.isScrolling	= false;
									instance.hovertimeout 	= setTimeout(function() {
										if( !instance.isScrollbarHover )
											$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 );
									}, instance.extPluginOpts.hovertimeout_t );
									
								});
								
								// wrap the scrollbar
								// we need this to be able to add the mouseenter / mouseleave events to the scrollbar
								var $vBarWrapper	= $('<div/>').css({
									position	: 'absolute',
									left		: $vBar.css('left'),
									top			: $vBar.css('top'),
									right		: $vBar.css('right'),
									bottom		: $vBar.css('bottom'),
									width		: $vBar.width(),
									height		: $vBar.height()
								}).bind('mouseenter.jsp',function() {									
									clearTimeout( instance.hovertimeout );
									clearTimeout( instance.elementtimeout );
									instance.isScrollbarHover	= true;				
									instance.elementtimeout	= setTimeout(function() {
										$vBar.stop( true, true ).jspmouseenter();
									}, 100 );	
								}).bind('mouseleave.jsp',function() {
									clearTimeout( instance.hovertimeout );
									instance.isScrollbarHover	= false;
									instance.hovertimeout = setTimeout(function() {
										if( !instance.isScrolling )
											$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 );
									}, instance.extPluginOpts.hovertimeout_t );
								});
								$vBar.wrap( $vBarWrapper );
							}
						}
					},
					// the jScrollPane instance
					jspapi 			= $el.data('jsp');
				// extend the jScollPane by merging	
				$.extend( true, jspapi, extensionPlugin );

				jspapi.addHoverFunc();
			
	
	
	}	
/*
* jScrollPane - v2.0.0beta1 - 2010-08-06
* http://jscrollpane.kelvinluck.com/
*
* Copyright (c) 2010 Kelvin Luck
* Dual licensed under the MIT and GPL licenses.
*/
(function (b, a, c) {
    b.fn.jScrollPane = function (f) {
        function d(ah, ae) {
            var an, h = this, ad, I, R, at, W, al, ac, E, q, aq, m, X, A, p, ap, D, P, af, V, o, S, C, aa, F, H, O, n, K, y, ao, z; I = {}; J(ae); function J(aw) { var aA, az, ay, av, au, ax; an = aw; if (ad == c) { ah.css("overflow", "hidden"); R = an.paneWidth || ah.innerWidth(); at = ah.innerHeight(); ad = b('<div class="jspPane" />').wrap(b('<div class="jspContainer" />').css({ width: R + "px", height: at + "px" })); ah.wrapInner(ad.parent()); W = ah.find(">.jspContainer"); ad = W.find(">.jspPane") } else { ax = ah.outerWidth() != R || ah.outerHeight() != at; if (ax) { R = ah.innerWidth(); at = ah.innerHeight(); W.css({ width: R + "px", height: at + "px" }) } ad.css("width", null); if (!ax && ad.outerWidth() == al && ad.outerHeight() == ac) { return } W.find(".jspVerticalBar,.jspHorizontalBar").remove().end() } aA = ad.clone().css("position", "absolute"); az = b('<div style="width:1px; position: relative;" />').append(aA); b("body").append(az); al = Math.max(ad.outerWidth(), aA.outerWidth()); az.remove(); ac = ad.outerHeight(); E = al / R; q = ac / at; aq = q > 1; m = E > 1; if (!(m || aq)) { ah.removeClass("jspScrollable"); ad.css("top", 0); Q(); r(); u() } else { ah.addClass("jspScrollable"); ay = an.maintainPosition && (p || P); if (ay) { av = j(); au = g() } x(); aj(); k(); if (ay) { w(av); v(au) } t(); ar(); if (an.hijackInternalLinks) { i() } } if (an.autoReinitialise && !z) { z = setInterval(function () { J(an) }, an.autoReinitialiseDelay) } else { if (!an.autoReinitialise && z) { clearInterval(z) } } } function x() { if (aq) { W.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'), b('<div class="jspDragBottom" />'))), b('<div class="jspCap jspCapBottom" />'))); af = W.find(">.jspVerticalBar"); V = af.find(">.jspTrack"); X = V.find(">.jspDrag"); if (an.showArrows) { aa = b('<a href="#" class="jspArrow jspArrowUp">Scroll up</a>').bind("mousedown.jsp", G(0, -1)).bind("click.jsp", ai); F = b('<a href="#" class="jspArrow jspArrowDown">Scroll down</a>').bind("mousedown.jsp", G(0, 1)).bind("click.jsp", ai); if (an.arrowScrollOnHover) { aa.bind("mouseover.jsp", G(0, -1, aa)); F.bind("mouseover.jsp", G(0, 1, F)) } U(V, an.verticalArrowPositions, aa, F) } S = at; W.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function () { S -= b(this).outerHeight() }); X.hover(function () { X.addClass("jspHover") }, function () { X.removeClass("jspHover") }).bind("mousedown.jsp", function (au) { b("html").bind("dragstart.jsp selectstart.jsp", function () { return false }); X.addClass("jspActive"); var s = au.pageY - X.position().top; b("html").bind("mousemove.jsp", function (av) { L(av.pageY - s, false) }).bind("mouseup.jsp mouseleave.jsp", B); return false }); N(); ag(); l() } else { Q() } } function N() { V.height(S + "px"); p = 0; o = an.verticalGutter + V.outerWidth(); ad.width(R - o); if (af.position().left == 0) { ad.css("margin-left", o + "px") } } function aj() {
                if (m) {
                    W.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'), b('<div class="jspDragRight" />'))), b('<div class="jspCap jspCapRight" />'))); H = W.find(">.jspHorizontalBar"); O = H.find(">.jspTrack"); ap = O.find(">.jspDrag"); if (an.showArrows) { y = b('<a href="#" class="jspArrow jspArrowLeft">Scroll left</a>').bind("mousedown.jsp", G(-1, 0)).bind("click.jsp", ai); ao = b('<a href="#" class="jspArrow jspArrowRight">Scroll right</a>').bind("mousedown.jsp", G(1, 0)).bind("click.jsp", ai); if (an.arrowScrollOnHover) { y.bind("mouseover.jsp", G(-1, 0, y)); ao.bind("mouseover.jsp", G(1, 0, ao)) } U(O, an.horizontalArrowPositions, y, ao) } ap.hover(function () { ap.addClass("jspHover") }, function () { ap.removeClass("jspHover") }).bind("mousedown.jsp", function (au) {
                        b("html").bind("dragstart.jsp selectstart.jsp", function () { return false }); ap.addClass("jspActive"); var s = au.pageX - ap.position().left; b("html").bind("mousemove.jsp", function (av) { M(av.pageX - s, false) }).bind("mouseup.jsp mouseleave.jsp", B);
                        return false
                    }); n = W.innerWidth(); ab(); am()
                } else { }
            } function ab() { W.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function () { n -= b(this).outerWidth() }); O.width(n + "px"); P = 0 } function k() { if (m && aq) { var au = O.outerHeight(), s = V.outerWidth(); S -= au; b(H).find(">.jspCap:visible,>.jspArrow").each(function () { n += b(this).outerWidth() }); n -= s; at -= s; R -= au; O.parent().append(b('<div class="jspCorner" />').css("width", au + "px")); N(); ab() } if (m) { ad.width(W.outerWidth() + "px") } ac = ad.outerHeight(); q = ac / at; if (m) { K = 1 / E * n; if (K > an.horizontalDragMaxWidth) { K = an.horizontalDragMaxWidth } else { if (K < an.horizontalDragMinWidth) { K = an.horizontalDragMinWidth } } ap.width(K + "px"); D = n - K } if (aq) { C = 1 / q * S; if (C > an.verticalDragMaxHeight) { C = an.verticalDragMaxHeight } else { if (C < an.verticalDragMinHeight) { C = an.verticalDragMinHeight } } X.height(C + "px"); A = S - C } } function U(av, ax, au, s) { var az = "before", aw = "after", ay; if (ax == "os") { ax = /Mac/.test(navigator.platform) ? "after" : "split" } if (ax == az) { aw = ax } else { if (ax == aw) { az = ax; ay = au; au = s; s = ay } } av[az](au)[aw](s) } function G(au, s, av) { return function () { ak(au, s, this, av); this.blur(); return false } } function ak(av, s, ay, ax) { ay = b(ay).addClass("jspActive"); var aw, au = setInterval(function () { if (av != 0) { M(P + av * an.arrowButtonSpeed, false) } if (s != 0) { L(p + s * an.arrowButtonSpeed, false) } }, an.arrowRepeatFreq); aw = ax == c ? "mouseup.jsp" : "mouseout.jsp"; ax = ax || b("html"); ax.bind(aw, function () { ay.removeClass("jspActive"); clearInterval(au); ax.unbind(aw) }) } function B() { b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"); X && X.removeClass("jspActive"); ap && ap.removeClass("jspActive") } function L(s, au) { if (!aq) { return } if (s < 0) { s = 0 } else { if (s > A) { s = A } } if (au == c) { au = an.animateScroll } if (au) { h.animate(X, "top", s, Y) } else { X.css("top", s); Y(s) } } function Y(au) { if (au == c) { au = X.position().top } W.scrollTop(0); p = au; ag(); var av = au / A, s = -av * (ac - at); ad.css("top", s) } function M(au, s) { if (!m) { return } if (au < 0) { au = 0 } else { if (au > D) { au = D } } if (s == c) { s = an.animateScroll } if (s) { h.animate(ap, "left", au, Z) } else { ap.css("left", au); Z(au) } } function Z(au) { if (au == c) { au = ap.position().left } W.scrollTop(0); P = au; am(); var av = au / D, s = -av * (al - R); ad.css("left", s) } function ag() { if (an.showArrows) { aa[p == 0 ? "addClass" : "removeClass"]("jspDisabled"); F[p == A ? "addClass" : "removeClass"]("jspDisabled") } } function am() { if (an.showArrows) { y[P == 0 ? "addClass" : "removeClass"]("jspDisabled"); ao[P == D ? "addClass" : "removeClass"]("jspDisabled") } } function v(s, au) { var av = s / (ac - at); L(av * A, au) } function w(au, s) { var av = au / (al - R); M(av * D, s) } function T(aC, aA, au) { var ay, aw, s = 0, av, az, aB; try { ay = b(aC) } catch (ax) { return } aw = ay.outerHeight(); W.scrollTop(0); while (!ay.is(".jspPane")) { s += ay.position().top; ay = ay.offsetParent(); if (/^body|html$/i.test(ay[0].nodeName)) { return } } av = g(); az = av + at; if (s < av || aA) { aB = s - an.verticalGutter } else { if (s + aw > az) { aB = s - at + aw + an.verticalGutter } } if (aB) { v(aB, au) } } function j() { return -ad.position().left } function g() { return -ad.position().top } function l() { W.unbind("mousewheel.jsp").bind("mousewheel.jsp", function (s, av) { var au = p; L(p - av * an.mouseWheelSpeed, false); return au == p }) } function Q() { W.unbind("mousewheel.jsp") } function ai() { return false } function t() { ad.find(":input,a").bind("focus.jsp", function () { T(this, false) }) } function r() { ad.find(":input,a").unbind("focus.jsp") } function ar() { if (location.hash && location.hash.length > 1) { var av, au; try { av = b(location.hash) } catch (s) { return } if (av.length && ad.find(av)) { if (W.scrollTop() == 0) { au = setInterval(function () { if (W.scrollTop() > 0) { T(location.hash, true); b(document).scrollTop(W.position().top); clearInterval(au) } }, 50) } else { T(location.hash, true); b(document).scrollTop(W.position().top) } } } } function u() { b("a.jspHijack").unbind("click.jsp-hijack").removeClass("jspHijack") } function i() { u(); b("a[href^=#]").addClass("jspHijack").bind("click.jsp-hijack", function () { var s = this.href.split("#"), au; if (s.length > 1) { au = s[1]; if (au.length > 0 && ad.find("#" + au).length > 0) { T("#" + au, true); return false } } }) } b.extend(h, { reinitialise: function (au) { au = b.extend({}, au, an); J(au) }, scrollToElement: function (av, au, s) {
                T(av, au, s)
            }, scrollTo: function (av, s, au) { w(av, au); v(s, au) }, scrollToX: function (au, s) { w(au, s) }, scrollToY: function (s, au) { v(s, au) }, scrollBy: function (au, s, av) { h.scrollByX(au, av); h.scrollByY(s, av) }, scrollByX: function (s, av) { var au = j() + s, aw = au / (al - R); M(aw * D, av) }, scrollByY: function (s, av) { var au = g() + s, aw = au / (ac - at); L(aw * A, av) }, animate: function (au, ax, s, aw) { var av = {}; av[ax] = s; au.animate(av, { duration: an.animateDuration, ease: an.animateEase, queue: false, step: aw }) }, getContentPositionX: function () { return j() }, getContentPositionY: function () { return g() }, getContentPane: function () { return ad }, scrollToBottom: function (s) { L(A, s) }, hijackInternalLinks: function () { i() }
            })
        } f = b.extend({}, b.fn.jScrollPane.defaults, f); var e; this.each(function () { var g = b(this), h = g.data("jsp"); if (h) { h.reinitialise(f) } else { h = new d(g, f); g.data("jsp", h) } e = e ? e.add(g) : g }); return e
    }; b.fn.jScrollPane.defaults = { showArrows: false, maintainPosition: true, autoReinitialise: false, autoReinitialiseDelay: 500, verticalDragMinHeight: 0, verticalDragMaxHeight: 99999, horizontalDragMinWidth: 0, horizontalDragMaxWidth: 99999, animateScroll: false, animateDuration: 300, animateEase: "linear", hijackInternalLinks: false, verticalGutter: 4, horizontalGutter: 4, mouseWheelSpeed: 10, arrowButtonSpeed: 10, arrowRepeatFreq: 100, arrowScrollOnHover: false, verticalArrowPositions: "split", horizontalArrowPositions: "split", paneWidth: c }
})(jQuery, this);