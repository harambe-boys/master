<?php
	require_once 'data/dataBase.php';
	require_once 'classes/cUsuario.php';

	
	$usuario = new Usuario();
	
	if ( $usuario->verSession() == true ) {
		header("Location: index.php");
		exit();
	}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
		<title>Lomas de Altamira</title>
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		<link rel="shortcut icon" type="image/ico" href="" />
		
		
		<link href="css/zice.style.css" rel="stylesheet" type="text/css" />
		<link href="css/icon.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<style type="text/css">
		html {
			background-image: none;
		}
		label{
			color: ##0B53AA;
		}
		.formLogin span.fa{
			color: #e7a125 !important;
			min-width: 25px;
		}
		#versionBar {
			
			position:fixed;
			width:100%;
			height:35px;
			bottom:0;
			left:0;
			text-align:center;
			line-height:35px;
		}
		.copyright{
			text-align:center; font-size:11px; color:white; border-top: solid 4px #378bdd;font-weight: bold;
		}
		.copyright a{
			color:white;font-weight: bold; text-decoration:none;
		}
		</style>
	</head>
<body >
<div id="alertMessage" class="error"></div>
<div id="successLogin"></div>
<div class="text_success"><img src="images/mred/Preloader_3.gif" /><span>Espere un momento por favor!</span></div>

<div id="login" >
  <div class="inner">
  <div  class="logo" ><img src="images/Altamira 1024x1024.png"  style="position: relative; left: -90px; top: -8px; width: 44%;"/></div>
  <div class="userbox"></div>
  <div class="formLogin">
   	<form name="formLogin"  id="formLogin" action="actions/actionL.php" method="post">
		<input type="hidden" id="opt" name="opt"  value="nLogin"   />	
		<div class="tip">
			<p>
				<label>Usuario:</label> <br />
					<!--<img style="display:inline-block; vertical-align: middle;"  src="images/icon/user_.png">-->
					<span class="fa fa-user fa-2x"></span>
					<input type="text" name="usuario"  value="" id="username_id" title="Usuario" />
			</p>	
		</div>
		<div class="tip">					
			<p>
				<label>Password:</label> <br />
				<span class="fa fa-lock fa-2x"></span>
				<input type="password"  name="password" id="password" title="Password"  />
			</p>
		</div>	
		<div style="float:left;padding:2px 0px ;">
			<div> 
				<ul class="uibutton-group" style="position: relative; bottom: 9px; padding: 10px; left: 110px;">
					<input type="submit" class="uibutton  normal" value="INGRESAR" style="border-radius: 5px; padding: 10px;"/>
				</ul>
			</div>
		</div>
							
	</form>	
				
  </div>
</div>
  <div class="clear"></div>
  <div class="shadow"></div>
</div>

<!--Login div-->
<div class="clear"></div>
<div id="versionBar" >
  <div class="copyright" > &copy; Copyright <?php echo date('Y');?>  Todos los derechos reservados <span class="tip"><a  href="#" title="Desarrollador" >Lomas de Altamira</a> </span> </div>
  <!-- // copyright-->
</div>
<!-- Link JScript-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
<script type="text/javascript" src="components/ui/jquery.ui.min.js"></script>     
<script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
<script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
<script type="text/javascript" src="js/login.js"></script>
</body>
</html>