<?php
	//Data
	include_once "data/dataBase.php";
	//Clases
	include_once "classes/cUsuario.php";
	
	$oUsuario 		= new Usuario();
	
	
	if ( !$oUsuario->verSession() ) {
		header("Location: login.php");
		exit();
	}
	$nombre = $_SESSION['Altamira']['user'];
	$url = dirname($_SERVER["PHP_SELF"]); 
	$idUsuario_web	= "";
	$nombre				= "";
	$contrasenia		= "";
	$estado 			= "";
	$idRol 				= "";
	$option    			= "";
	
	
		
	if (isset($_GET['opt'])) {
		$option    			= $_GET['opt'];
		$idUsuario_web 	= $_GET['idUsuario_web'];
	}
	
	if ($option == "mUsuario") {
		$vUsuarioweb = $oUsuario->getUsuariowebOne($idUsuario_web);
		foreach ($vUsuarioweb AS $id => $info){  
			$idUsuario_web	= $id;
			$nombre				= $info["nombre"];
			$contrasenia		= $info["contrasenia"];
			$estado 			= $info["estado"];
			$idRol 				= $info["idrol"];
		}
	}
	
	$vRol			= $oUsuario->getRolAll();
	$vUsuariosWeb = $oUsuario->getUsuariowebAll();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Usuarios Web</title>
       <?php
			include_once "cssyjscomun.php";
		?>
		<script type="text/javascript">	
			$(document).ready(function() {	
				$("#Usuario").addClass("select");
				var options = {
					target:       '#alertMessage',
					beforeSubmit: validate,
					success:      successful,
					clearForm:    false,
					resetForm:    false
				};
				<?php
					if($option == "mUsuario"){
				?>
						$("#hi1").removeClass();
						$("#hi2").addClass("active");
						$("#tab1").hide();
						$("#tab2").show();
				<?php
					}
				?>
				$('#form').submit(function() {
					$(this).ajaxSubmit(options);
					return false;
				});
				$('#data_table').dataTable({
					"sPaginationType":"full_numbers",
					"aaSorting": []
				});
			});
			
			
			function validate(){
				var form     = document.form;
				var is_error = false;
				var msg      = '';
				
				if (!form.nombre.value) {
					msg = 'Ingrese nombre del Usuario';
					is_error = true;
				}else if(!form.password.value){
					msg = 'Ingresar el Password' ;
					is_error = true;
				}
				<?php
				if ($option == "mUsuario") {
				?>
				if(form.password.value != form.cpassword.value){
					msg = 'Las Contrase\u00F1as no coinciden';
					is_error = true;
				}
				<?php
				}
				?>
				
				if (is_error == true) {
					showWarning(msg,7000);
					return false;
				} else {
					loading('Loading',1);
				}	
			}
		  
			function successful(responseText, statusText){
				responseText = responseText.replace(/^\s*|\s*$/g,"");
				if (responseText == 'done'){
					<?php
						if ($option == "mUsuario") {
							echo 'msg = "El Usuario Web ha sido Modificado.";';
						}else{
							echo 'msg = "El nuevo Usuario Web ha sido Guardado.";';
						}
					?>
					setTimeout( "showSuccess(msg,5000);", 2000 ); 
					setTimeout( "unloading()", 3000 );
					window.setTimeout("document.location.href='usuario.php';",2500);
				} else if(responseText == 'exist') {
					msg = "Nombre no disponible, intente con otro nombre.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if(responseText == 'pass') {
					msg = "La verificaci\u00F3n de la contrase\u00F1a no coinciden.";
					showWarning(msg,7000);
					window.setTimeout("unloading();",3000);
				}else if (responseText == 'error1'){
					msg = "Necesita ingresar la contrase\u00F1a antigua.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 );
					document.getElementById("get").disabled = false;
				}else if (responseText == 'error2'){
					msg = "Contrase\u00F1as antigua incorrecta.";
					setTimeout( "showWarning('<font color=black>'+msg+'</font>',5000);", 2000 );
					setTimeout( "unloading()", 2000 ); 
					document.getElementById("get").disabled = false;
				}else  {
					msg = "ERROR. INTENTELO DE NUEVO.";
					setTimeout( "showError(msg,7000);", 2000 );
					setTimeout( "unloading()", 3000 );
					window.setTimeout("location.reload(true);",2500);
				}
			}
			
			
		</script>		
      
	</head>        
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                       
        <?php
			include_once "menu.php";
		?>

            
		<div id="content">
			<div class="inner">
				<div class="topcolumn">
					<!--<div class="logo"></div>-->
				</div>
				<div class="clear"></div>
					
				<div class="onecolumn" >
					<div class="header"><span ><span class="ico  fa fa-user-plus fa-2x"></span>Usuarios Web  </span> </div>
					<!-- End header -->	
					<div class="clear"></div>
					<div class="content" >
						<div id="uploadTab">
							<ul class="tabs" >
								<li id="hi1"><a href="#tab1"  id="3"  >  Lista de Usuarios Web</a></li>  
								<li id="hi2"><a href="#tab2"  id="2"  ><?php if ($option == "mUsuario") { echo "Modificar"; }else{ echo "Crear"; }?> Usuarios Web</a></li>   
							</ul>
							<div class="tab_container" >
								<div id="tab1" class="tab_content" > 
									<div class="load_page">
										<form class="tableName toolbar">
											<table class="display data_table2" id="data_table">
												<thead>
													<tr>
														<th>Nombre</th>
														<th>Rol</th>
														<th>Estado</th>
														<th>Opciones</th>
														 
													</tr>
												</thead>
												<tbody>
													<?php
													if($vUsuariosWeb){
														foreach ($vUsuariosWeb AS $id => $arrUsuarioWeb) {
													?>
														<tr>
															<td><?=$arrUsuarioWeb['nombre'];?></td>		
															<td ><?=$arrUsuarioWeb['nombreRol'];?></td>
															<td ><?php if($arrUsuarioWeb['estado']==1){echo "activo";}else{echo "Inactivo";}?></td>
															<td><span class="tip" >
																	<a href="usuario.php?opt=mUsuario&idUsuario_web=<?=$id;?>" title="Modificar" >
																		<img src="images/icon/icon_edit.png" > 
																	</a>
																	<?php 
																		if($arrUsuarioWeb['estado']==1){
																	?>
																			<a id="opt=bUsuarioweb&idUsuario_web=<?=$id?>" class="actiones2"  data-action="actionWebUser.php" data-location="usuario.php" data-tittle="Bloquear" data-msg="&iquest;Quiere bloquear al usuario?" name="<?=$arrUsuarioWeb['nombre']?>" title="Bloquear">
																				<img src="images/icon/color_18/delete.png" >
																			</a>
																	<?php
																		}elseif($arrUsuarioWeb['estado']==0){
																	?>
																			<a id="opt=hUsuarioweb&idUsuario_web=<?=$id?>" class="actiones2"  data-action="actionWebUser.php" data-location="usuario.php" data-tittle="Habilitar" data-msg="&iquest;Quiere habilitar al usuario?" name="<?=$arrUsuarioWeb['nombre']?>" title="Habilitar">
																				<img src="images/icon/color_18/checkmark2.png" >
																			</a>
																	<?php
																	}
																	?>
																</span> 
															</td>
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											</table>
										</form>
									</div>	
								</div>
								<!--tab1-->
								<div id="tab2" class="tab_content"> 
									<div class="load_page">
										<div class="formEl_b" id="msg">	
											<form id="form" action="actions/actionWebUser.php" method="post" name="form"> 
												<?php
													if ($option == "mUsuario") {
														echo '<input type="hidden" name="idUsuario_web" value="'.$idUsuario_web.' "/>';
														echo '<input type="hidden" name="opt" value="mUsuario" />';
													}else{
														echo '<input type="hidden" name="opt" value="nUsuario" />';
													}
												?>
												<fieldset >
													<legend>Por favor introducir toda la informaci&oacute;n.</span></legend>
													
														
													<div class="section ">
														<label> Nombre de Usuario</label>   
														<div> 
															<input type="text" class="validate[required] large" name="nombre" id="nombre" value="<?=$nombre;?>" />
														</div>
													</div>
													
													
													<?php
													if ($option == "mUsuario") {
													?>
													<div class="section ">
														<label> Nuevo Contrase&ntilde;a</label>   
														<div> 
															<input type="password"  class="validate[required,minSize[8]] medium"  name="password" id="password" value="" >
														</div>
													</div>
								  
													<div class="section ">
														  <label>Confirmar Contrase&ntilde;a</label>   
														  <div> 
														  <input type="password"  class="validate[required,minSize[8]] medium"  name="cpassword" id="cpassword" value="" >
														  </div>
													</div>
													<?php  
													}else{
													?>
													<div class="section ">
														<label> Nuevo Contrase&ntilde;a</label>   
														<div> 
															<input type="password"  class="validate[required,minSize[8]] medium"  name="password" id="password" value="" >
														</div>
													</div>
								  
													<div class="section ">
														  <label>Confirmar Contrase&ntilde;a</label>   
														  <div> 
														  <input type="password"  class="validate[required,minSize[8]] medium"  name="cpassword" id="cpassword" value=""  >
														  </div>
													</div>
													<?php  
													}
													?>
													
													<div class="section">
														<label> Roles</label>   
														<div> 
															<select name="idRol" id="idRol" class="chzn-select">
																<?php
																	if($vRol){
																		foreach ($vRol AS $id => $arrRol) {
																			if($idRol == $id){
																				$selected = "selected = 'selected'";
																?>
																<option value="<?=$id;?>"  <?=$selected?>><?=$arrRol['nombre'];?></option>
																<?php
																			} else {
																?>
																<option value="<?=$id;?>"><?=$arrRol['nombre'];?></option>
																<?php
																			}
																		}
																	}
																?>
															</select>
														</div>
													</div>												
													<div class="section">
														<label> Estado</label>   
														<div> 
															<select name="estado" id="estado" class="chzn-select">
																<option value="0" <?php if($estado==0){echo"selected='selected'";}?>>Inactivo</option>
																<option value="1" <?php if($estado==1){echo"selected='selected'";}?>>Activo</option>
															</select>
														</div>
													</div>											
													<div class="section last">
														<div>
														<?php
															if ($option == "mUsuario") {
																echo '<input type="submit" value="MODIFICAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}else{
																echo '<input type="submit" value="GUARDAR" class="uibutton submit_form" name="guardar" id="get"/>';
															}
														?>    
														<a class="btn btn-danger" style="padding: 2px 12px 2px 12px; border-radius: 0px;border: 1px solid #000000;" href="http://52.23.92.102/Altamira/usuario.php" role="button">Cancelar</a>
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>	
								</div><!--tab2-->
							</div>
						</div><!--/END TAB/-->
						<div class="clear"/></div>                  
				</div>
			</div>
			<?php
				include_once "footer.php";
			?>
		</div> <!--// End inner -->
	</body>
</html>